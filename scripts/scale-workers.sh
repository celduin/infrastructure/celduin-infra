#!/bin/bash

set -e

if [ -z "$ENVIRONMENT" ]; then
	echo "ENVIRONMENT is not set"
	false
fi

if [ -z "$1" ]; then
	echo "No desired capacity is specified"
	false
fi

source_dir="$(dirname $0)"
name_script="$source_dir/worker-group-name.py"

group_name="$(aws autoscaling describe-auto-scaling-groups | $name_script "-" "$ENVIRONMENT")"

aws autoscaling set-desired-capacity --auto-scaling-group-name "$group_name" --desired-capacity $1
