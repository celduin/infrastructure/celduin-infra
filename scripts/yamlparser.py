#!/usr/bin/env python3

import sys
import yaml
import json
import subprocess


# Load a python object and convert it into jsonnet
def dump_jsonnet(element, name):
    with open(name, 'w') as out:
        # Convert a pyhton object into json and write it on out
        json.dump(element, out, indent=2)
    # Format json content into jsonnet
    cmd = ["jsonnetfmt",  "-i", name]
    subprocess.run(cmd)


def check_input():
    # Check if at list one file has been provided
    if len(sys.argv) < 2:
        print("ERROR: You need to provide at least one yml file.")
        return False
    for file in sys.argv[1:]:
        try:
            key = "metadata"
            with open(file, 'r') as f:
                # Check if the file has yml extension
                if file[-3:] != "yml" and file[-4:] != "yaml":
                    raise ValueError
                for stream in yaml.load_all(f, Loader=yaml.FullLoader):
                    # Check if the file has valid yaml k8s content
                    if not key in stream.keys():
                        raise ValueError
            return True
        # Check if the file exists
        except IOError:
            print("ERROR: {} has not been found".format(file))
            return False
        except ValueError:
            print("ERROR: You can only provide yaml files with .yml or .yaml file extension.")
            return False


def main():
    """In order to use this tool you need the jsonnetfmt binary in your PATH
    You can build it from source from https://github.com/google/jsonnet or
    install it from you package manager if it has it.
    Usage: ./yamlparser.py <filename>"""

    if not check_input():
        return False
    # Loop over the provided input files.
    for file in sys.argv[1:]:
        with open(file, 'r') as element:
            for stream in yaml.load_all(element, Loader=yaml.FullLoader):
                # Search for the name of the element
                name = stream['metadata']['name'] + "-" + stream['kind'] + ".jsonnet"
                dump_jsonnet(stream, name)


if __name__ == "__main__":
    main()
