#!/usr/bin/python3

# Finds the full name of the autoscaling group that starts with k8-cluster-$ENVIRONMENT-1
# since at the time of writing, that's the nodegroup that contains the workers.
# In future, it would be useful to find the autoscaling group with the right tag, instead
# of relying on the index.

import sys
import json

# arg 1 is the json file that contains autoscaling groups, output from `aws autoscaling describe-auto-scaling-groups`
# an infile of "-" means to read from stdin
infile = sys.argv[1]

# Arg 2 is the environment
environment = sys.argv[2]

if infile == "-":
    data = json.load(sys.stdin)
else:
    with open(infile, "r") as f:
        data = json.load(f)

for group in data["AutoScalingGroups"]:
    if group["AutoScalingGroupName"].startswith("k8-cluster-{}-1".format(environment)):
        print(group["AutoScalingGroupName"])
        break
