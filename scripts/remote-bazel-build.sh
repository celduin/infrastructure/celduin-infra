#!/bin/bash

set -e
# The first argument is the name of the target to build using remote execution
# It also *requires* ENVIRONMENT and CI_PROJECT_DIR
# It requires the executables 'ansible-vault' and 'bazel' to exist
# It *uses*, but doesn't require PUSH_URL, ENV_SEED, BAZELRC_PATH, CLUSTER_PORT and CERTS_DIR
# if ANSIBLE_PASSWD_FILE is not provided, then an interactive prompt will request the password

if [ -z "$1" ]; then
    echo "Bazel build target not defined"
    FAIL=yes
else
    BAZEL_BUILD_TARGET=$1
fi

if [ -z "$ENVIRONMENT" ]; then
    echo "variable ENVIRONMENT is not set"
    FAIL=yes
fi

if [ -z "$CI_PROJECT_DIR" ]; then
    echo "variable CI_PROJECT_DIR is not set"
    FAIL=yes
fi

if [ -z "$PUSH_URL" ] && [ ! -d "${CI_PROJECT_DIR}/${ENVIRONMENT}-cluster" ]; then
    echo "Directory ${CI_PROJECT_DIR}/${ENVIRONMENT}-cluster not found. Is get-cluster-info being run?"
    FAIL=yes
fi

if ! command -v "ansible-vault"; then
    echo "Executable 'ansible-vault' not found. Is ansible installed?"
    FAIL=yes
fi

if ! command -v "bazel"; then
    echo "Executable 'bazel' not found. Is it installed?"
    FAIL=yes
fi

if [ -n "$FAIL" ]; then
    false
fi

if [ -z "$PUSH_URL" ]; then
    export PUSH_URL="$(cat ${CI_PROJECT_DIR}/${ENVIRONMENT}-cluster/read_write_domain)"
fi

if [ -z "$ENV_SEED" ]; then
    export ENV_SEED="$RANDOM"
fi

if [ -z "$CLUSTER_PORT" ]; then
    export CLUSTER_PORT="443"
fi

if [ -n "$BAZELRC_PATH" ]; then
  export BAZELRC_EXPR="--bazelrc=$BAZELRC_PATH"
else
  export BAZELRC_EXPR=""
fi

if [ -z "$CERTS_DIR" ]; then
  export CERTS_DIR="${CI_PROJECT_DIR}/test/buildstream/${ENVIRONMENT}-certs"
fi

# Ensure the certificates in CERTS_DIR are decrypted
for f in $(find "$CERTS_DIR" -type f); do
  # Only decrypt files if they're encrypted, i.e. starts with "$ANSIBLE_VAULT"
  if head -n1 "$f" | grep -q '$ANSIBLE_VAULT'; then
    find "$CERTS_DIR" -type f | xargs ansible-vault decrypt --vault-password-file="${ANSIBLE_PASSWD_FILE}"
  fi
done

bazel --output_user_root="${XDG_CACHE_HOME}" $BAZELRC_EXPR build \
  --verbose_failures \
  --action_env="FOO=${ENV_SEED}" \
  --remote_executor=${PUSH_URL}:${CLUSTER_PORT} \
  --remote_timeout=600 \
  --tls_client_certificate=$CERTS_DIR/client-push.crt \
  --tls_client_key=$CERTS_DIR/client-push.key \
  "$BAZEL_BUILD_TARGET"
