This repository holds the ansible for various pieces of infrastructure used for
projects under the celduin umbrella.

We ansible-vault our secrets, when running pass `ansible-playbook` the `--ask-vault-pass`
option, and you will be asked the password before the playbook gets run, and
ansible will decrypt only for the run.

Passwords for secrets are stored in the brains of Thomas Coldrick (coldtom) and
Phil Dawson (phildawson). If you update the keys for cache auth, then these
will need to be updated in the CI variables of the relevant projects.

A list of infra maintained here can be found below.

# Generating client certificates

As per the [BuildStream artifact server documentation](https://buildstream.gitlab.io/buildstream/using_configuring_cache_server.html?highlight=authenticate)

To generate a new client certificate do

openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=client" -out roles/bst_cas/files/client.crt -keyout roles/bst_cas/files/client.key

In order to use this certificate to push to the caches, the resulting client.crt
must be included in the server's authorised.crt file. Take a look in
roles/ssl/tasks/main.yml for how to do this.

# Provisioning on AWS

Our cache servers use nitro EBS volumes, which don't preserve the device name
given in AWS. As a result to deterministically sort out our mountpoints, we
need to use some information from AWS, and to run the ansible to provision the
mountpoints you will need to have boto3 installed and AWS credentials.

For technical details, we poll AWS for the volume ID corresponding to each
volume we want, and then use ansible to inspect the device names present.
Thankfully the volume ID is included in the device name so we can match up
the two. We can then mount the volumes in the correct places.
