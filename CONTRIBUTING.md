# Contributing

If you wish to assist in our development efforts with celduin-infra maintained clusters, this is the information you need.

## Pre commit hook

We provide a pre-commit hook in `scripts/pre-commit`, it's recommended to use
this to avoid committing secrets and to automatically run the jsonnet formatter.
I use

```bash
ln -s scripts/pre-commit .git/hooks/pre-commit
```

to activate the hook.

## Deploying Infrastructure

### Prerequisites

You will need:

- An AWS account with enough access to create resources
- The AWS CLI
  - This can be installed from PyPI - `pip3 install --user awscli`
- Terraform
  - Your distribution _may_ supply this, if not then you may simply download
    the binary from [hashicorp](https://www.terraform.io/downloads.html) and
    add it to your `$PATH`
- aws-iam-authenticator
  - This can be installed by following the
    [instructions](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)
- Jsonnet
  - This may be available from your distribution, but if not you can install
    the binary release from the [jsonnet github](https://github.com/google/jsonnet/releases)

### Environments

We currently have a few terraform modules, first we have the `global` terraform, which sets
up resources shared by many environements, for example DNS hosted zones and storage
for terraform state.

We then have production and staging environments. These should all be deployed
using CI and not manually. There is a `public` cluster, a public cache intended for use by
projects who want it.

`staging` is used to verify that changes work correctly, and are not broken.

`dev` is to be used by developers to quickly spin up a small test cluster, this
doesn't have a terraform backend, so as to avoid overwriting existing
infrastructure. This means you _must_ destroy the infra when done, otherwise
someone will need to hunt through AWS and delete things manually.

To create a new environment, _copy_ either `public` or `staging`, and change the
`key` field in the `backend` of the `terraform` block. Not changing this will
overwrite currently deployed infra.

### Deploying Terraform

First you need to log in to AWS on the command line, using the awscli:

```bash
aws configure
```

and add in your access key ID, secret access key, and region. This will allow
terraform to provision infrastructure in AWS.

Your Access Key ID and Secret access key can be found from the AWS console in
your browser, under "My Security Credentials".
The region code can be found from the dropdown in the top-right corner of the AWS
console.

Deploying the infrastructure for some environment can be achieved by running:

```bash
./scripts/terraform-jsonnet -e [environment name] -c apply
```

## Deploying Services

To deploy the services you need `kubectl` installed. This _may_ be packaged by
your distribution, so it's worth having a look, if not then you can follow the
instructions at https://kubernetes.io/docs/tasks/tools/install-kubectl/.

### Deploying from CI using ArgoCD

Celduin-infra uses ArgoCD to deploy kubernetes infrastructure. Upon merges to
master, rendered jsonnet will be pushed to a [separate repository](https://gitlab.com/celduin/infrastructure/infrastructure-declarations)

Celduin-infra does not describe the ArgoCD CRDs and other kubernetes objects required to deploy ArgoCD. To do so:

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v1.4.2/manifests/install.yaml
```

If you need to remove ArgoCD resources, you can delete the `argocd` namespace. However due to finalizers
that can be deadlocked, the removal of the argocd namespace can hang. Following [this](https://github.com/kubernetes/kubernetes/issues/60538#issuecomment-369099998)
to patch the custom finalizer can faciliate the removal of all resources and deletion of the namespace.

```bash
kubectl delete crd applications.argoproj.io
# This will hang, Crtl-C to kill the current process whilst the deletion is stuck server side

kubectl patch crd applications.argoproj.io -p '{"metadata":{"finalizers":[]}}' --type=merge
# This will remove the offending finaliser

kubectl delete crd applications.argoproj.io

kubectl delete ns argocd
```

### Certificates

We need a few TLS certificates in order to 1) encrypt traffic and 2)
authenticate client access to the server. For production environments these are
encrypted using ansible-vault. For an example of the layout, see [the one in dev](./kubernetes/clusters/dev/config/certs.jsonnet).

To generate a new "client" certificate/key pair for a cluster, use:

```bash
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=client" -out client.crt -keyout client.key
```

Server TLS certificates should be handled by [cert-manager](https://cert-manager.io).

Different deployments may have different numbers of certs, due to policy of who
can access the cache.

Deploying the certificates is an out of band operation, and should be done
manually for the time being. This can be done by generating the certificate json

```bash
jsonnet -m target kubernetes/clusters/$environment/config/certs.jsonnet
```

Note you will first need to decrypt the certificates (if this is public or
staging) by using

```bash
ansible-vault decrypt kubernetes/clusters/$environment/config/certs.jsonnet
```

and entering the password when prompted. This password may be obtained from
@coldtom.

More information about certificate management can be found [here](./docs/certificate-management.md)

### Deploying Kubernetes

You will need to configure kubectl to point at your cluster, to do this run:

```bash
aws eks update-kubeconfig --name k8-cluster-$cluster_id
```

To deploy the kubernetes, simply run

```bash
mkdir -p target
jsonnet -m target kubernetes/clusters/$environment/config/certs.jsonnet
jsonnet -m target kubernetes/clusters/$environment/deployment.jsonnet
kubectl apply -f target/*-Namespace.json
kubectl apply -f target
```

In the event you need to deploy to staging, or public, you will need to
decrypt the certs first using

```bash
ansible-vault decrypt kubernetes/clusters/$environment/config/certs.jsonnet
```

## View Development Grafana Dashboards

In order to access the grafana dashboards of your local cluster, you need to:

1. Add in `/etc/hosts` `stats.cache.com` pointing to 127.0.0.1 (localhost) or instead
    add make `stats.cache.com` point to you minikube ip address.
2. If you pointed `stats.cache.com` pointed to 127.0.0.1, then execute
    `kubectl port-forward -n rex service/traefik-ingress 30443:443`. If you used minikube
    ip address you can skip this step.
3. Go to your browser and introduce `stats.cache.com:30443`.

## Destroying Deployed Infra

To destroy, first make sure you delete all the services as Kubernetes in EKS can
provision volumes and load balancers, which can be left behind if you don't
clean up first. For this simply

```bash
mkdir -p target
jsonnet -m target kubernetes/clusters/$environment/deployment.jsonnet
kubectl delete -f target
```

Then navigate to the terraform deployed and

```bash
./scripts/terraform-jsonnet -e $environment -c destroy
```

## Formatting jsonnet

Our CI checks the format of our jsonnet files using `jsonnetfmt`. To ease
running this for the contributor, we provide a handy script to do this. Simply
run

```bash
./scripts/format-jsonnet
```

to format all jsonnet files in the repository.

## Repository layout

You may have noticed there are several folders not directly involved with the
bulk of this documentation. A brief description of all folders is:

- `ansible` - Ansible definitions
- `bb-components` - A bazel project used to vendor docker images of Buildbarn
  components
- `common` - Libsonnets used by both terraform and kubernetes
- `docs` - In depth documentation
- `kubernetes` - Definitions for kubernetes services
- `licenses` - Text for licenses of code under other licenses
- `scripts` - Scripts used for tasks, e.g. generating docs
- `terraform` - Terraform definitions for infrastructure
- `test` - Toy projects used to minimally test deployments
- `zuul-ansible-ec2` - Ansible for a zuul deployment
- `zuul-deployment` - Kubernetes for a zuul deployment, should be put in the
  `kubernetes/` directory in future
