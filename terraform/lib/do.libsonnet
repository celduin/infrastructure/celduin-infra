/**
 * DigitalOcean Libsonnet
 *
 * This libsonnet contains abstractions for generating DigitalOcean specific terraform
 * snippets.
 */

/**
 * Create a new DigitalOcean `provider` block
 *
 * @param token The DO API token
 *
 * @return a DigitalOcean provider
 */
local newDigitalOceanProvider(token) =
  [
    {
      nested_blocks:: ['provider', 'digitalocean'],

      token: token,
    },
  ];

/**
 * Creates DigitalOcean volumes, to be used in conjunction with newDroplet()
 *
 * @param name The name of the DO volume
 * @param size The size, in GiB of the DO volume to be provisioned
 *
 * @return a DigitalOcean volume
 */
local newVolume(name, size) = {
  name: name,
  size: size,
};

/**
 * Creates DigitalOcean droplets, to be used in conjunction with newDroplet()
 *
 * @param name The name of the DO droplet
 * @param image The initial image to provision the DO droplet with
 * @param instance The instance slug, that determines the size of the DO droplet to be provisioned
 * @param volumes The volumes to attach to the droplet. Defaults to an empty array. If used, use the newVolume()
 *                helper function to provision volumes
 * @param ssh_keys The name of ssh keys that are registered to the DO account, that are then used to access the droplet.
 *                 Defaults to an empty array.
 * @param region The region of the droplet, defaults to "lon1"
 * @param private_networking Whether private networking is to be used. Defaults to true.
 *
 * @return a provisioned DigitalOcean droplet, which will output the floating IP that the droplet is assigned to.
 */
local newDroplet(name, image, instance, volumes=[], ssh_keys=[], region='lon1', private_networking=true) =
  [
    {
      nested_blocks:: ['resource', 'digitalocean_droplet', '%s_droplet' % name],

      name: name,
      image: image,
      region: region,
      size: instance,
      private_networking: private_networking,
      [if volumes != [] then 'volume_ids']: ['${digitalocean_volume.%s_volume.id}' % vol.name for vol in volumes],
      [if ssh_keys != [] then 'ssh_keys']: ['${data.digitalocean_ssh_key.%s_ssh_key.id}' % key for key in ssh_keys],
    },

    {
      nested_blocks:: ['resource', 'digitalocean_floating_ip', '%s_floating_ip' % name],

      droplet_id: '${digitalocean_droplet.%s_droplet.id}' % name,
      region: '${digitalocean_droplet.%s_droplet.region}' % name,
    },

    {
      nested_blocks:: ['output', '%s_floating_ip' % name],

      value: '${digitalocean_floating_ip.%s_floating_ip.ip_address}' % name,
    },
  ] + [
    {
      nested_blocks:: ['resource', 'digitalocean_volume', '%s_volume' % vol.name],

      name: vol.name,
      size: vol.size,
      region: region,
    }
    for vol in volumes
  ] + [
    {
      nested_blocks:: ['data', 'digitalocean_ssh_key', '%s_ssh_key' % key],

      name: key,
    }
    for key in ssh_keys
  ];

{
  newDigitalOceanProvider:: newDigitalOceanProvider,
  newDroplet:: newDroplet,
  newVolume:: newVolume,
}
