/**
 * AWS Libsonnet
 *
 * This libsonnet contains abstractions for generating AWS specific terraform
 * snippets.
 */

local Utils = import 'util.libsonnet';

/**
 * Create a new AWS `provider` block
 *
 * @param region AWS region to use.
 *
 * @return an AWS provider
 */
local newAwsProvider(region='eu-west-1') =
  [
    {
      nested_blocks:: ['provider', 'aws'],

      region: region,
      version: '>= 2.11',
    },
  ];

/**
 * Get default AWS VPC and subnet data
 *
 * @return the default VPC ID and subnets
 */
local _getAwsVpcData() =
  [
    {
      nested_blocks:: ['data', 'aws_subnet_ids', 'all_subnets'],

      vpc_id: '${data.aws_vpc.default_vpc.id}',
    },

    {
      nested_blocks:: ['data', 'aws_vpc', 'default_vpc'],

      default: true,
    },
  ];

/**
 * Generate a config for a nodegroup to be attached to the EKS cluster
 *
 * "Userdata" is the rather... obtuse name given by AWS to custom scripts added
 * to a LaunchConfiguration to be run at startup. Use this to add integration
 * steps you want run at node startup.
 *
 * `nodeLabels` allows us to add kubernetes labels to the node, it accepts
 * objects of the form of Utils.nodeLabel(), before formatting them into args
 * passed to kubelet.
 *
 * @param instanceType AWS instance type for the nodes
 * @param minNodes Minimum number of nodes in EC2 ASG
 * @param desiredNodes Ideal number of nodes in EC2 ASG
 * @param ebsOptimized Whether instances should be EBS optimised
 * @param autoscale Whether to enable Autoscaling for the ASG
 * @param preUserdata Data to be prepended to Userdata
 * @param postUserdata Data to be appended to Userdata
 * @param nodeLabels Kubernetes Labels to be applied to the node.
 *
 * @return an object representing configuration of a node group
 */
local nodeGroupConfig(
  instanceType,
  minNodes=1,
  desiredNodes=1,
  maxNodes=4,
  ebsOptimized='true',
  autoscale='true',
  preUserdata='',
  postUserdata='',
  nodeLabels=[]
      ) =
  {
    asg_desired_capacity: desiredNodes,
    asg_max_size: maxNodes,
    asg_min_size: minNodes,
    ebs_optimized: ebsOptimized,
    enable_autoscaling: autoscale,
    instance_type: instanceType,
    pre_userdata: preUserdata,
    additional_userdata: postUserdata,
    kubelet_extra_args: Utils.nodeLabelsToKubeletArgs(nodeLabels),
  };

/**
 * Create a new EKS cluster with ingress NLB
 *
 * @param clusterId Unique identifier for this cluster
 * @param nodeGroups List of Nodegroup configurations, as above
 * @param workerPolicies Optional list of ARNs of IAM policies to be added to
 *                       the worker's IAM role.
 *
 * @return a new EKS cluster, together with an ingress NLB forwarding 80, 443 ->
 *         30080, 30443, and all necessary security groups
 */
local newEksCluster(
  clusterId,
  nodeGroups,
  workerPolicies=[]
      ) =
  [
    {
      nested_blocks:: ['module', '%s_k8s_cluster' % clusterId],

      cluster_name: 'k8-cluster-' + clusterId,
      cluster_version: '1.14',
      source: 'terraform-aws-modules/eks/aws',
      subnets: '${data.aws_subnet_ids.all_subnets.ids}',
      version: '5.0.0',
      vpc_id: '${data.aws_vpc.default_vpc.id}',
      worker_additional_security_group_ids: [
        '${module.%s_worker_security_group.this_security_group_id}' % clusterId,
      ],
      worker_groups: [
        nodegroup {
          target_group_arns: [
            '${aws_lb_target_group.%s_lb_tg_https.arn}' % clusterId,
            '${aws_lb_target_group.%s_lb_tg_http.arn}' % clusterId,
          ],
        }
        for nodegroup in nodeGroups
      ],
      [if workerPolicies != [] then 'workers_additional_policies']: workerPolicies,
    },

    {
      nested_blocks:: ['module', '%s_worker_security_group' % clusterId],

      description: 'Security group for network traffic into k8-cluster-%s' % clusterId,
      egress_rules: [
        'all-all',
      ],
      ingress_cidr_blocks: [
        '0.0.0.0/0',
      ],
      ingress_rules: [
        'all-tcp',
      ],
      name: '%s-worker-security-group' % clusterId,
      source: 'terraform-aws-modules/security-group/aws',
      version: '3.4.0',
      vpc_id: '${data.aws_vpc.default_vpc.id}',
    },

    {
      nested_blocks:: ['resource', 'aws_lb', '%s_nlb' % clusterId],

      internal: false,
      load_balancer_type: 'network',
      name: '%s-ingress-lb' % clusterId,
      subnets: '${data.aws_subnet_ids.all_subnets.ids}',
    },

    {
      nested_blocks:: ['resource', 'aws_lb_listener', '%s_lb_listener_http' %
                                                      clusterId],
      default_action: {
        target_group_arn: '${aws_lb_target_group.%s_lb_tg_http.arn}' % clusterId,
        type: 'forward',
      },
      load_balancer_arn: '${aws_lb.%s_nlb.arn}' % clusterId,
      port: 80,
      protocol: 'TCP',
    },

    {
      nested_blocks:: ['resource', 'aws_lb_listener', '%s_lb_listener_https' %
                                                      clusterId],

      default_action: {
        target_group_arn: '${aws_lb_target_group.%s_lb_tg_https.arn}' % clusterId,
        type: 'forward',
      },
      load_balancer_arn: '${aws_lb.%s_nlb.arn}' % clusterId,
      port: 443,
      protocol: 'TCP',
    },

    {
      nested_blocks:: ['resource', 'aws_lb_target_group', '%s_lb_tg_http' % clusterId],

      name: '%s-target-group-http' % clusterId,
      port: 30000 + 80,
      protocol: 'TCP',
      vpc_id: '${data.aws_vpc.default_vpc.id}',
    },

    {
      nested_blocks:: ['resource', 'aws_lb_target_group', '%s_lb_tg_https' %
                                                          clusterId],

      name: '%s-target-group-https' % clusterId,
      port: 30000 + 443,
      protocol: 'TCP',
      vpc_id: '${data.aws_vpc.default_vpc.id}',
    },
  ] + _getAwsVpcData();


/**
 * Creates a new elasticache cluster configured for Redis, and necessary
 * dependencies
 *
 * @param clusterId Unique identifier for this cluster
 * @param instanceType AWS instance type cluster
 * @param port The port to use as an endpoint for Redis
 * @param outputName The name of the key to use in terraform output
 *
 * @return a new elasticache cluster configured for Redis
 */
local newRedisCluster(clusterId, instanceType, port=6379, outputName=null) =

  local output =
    if outputName == null then
      '%s_redis' % clusterId
    else
      outputName;

  [
    {
      nested_blocks:: ['module', '%s_redis_security_group' % clusterId],

      description: 'Security group for Redis',
      egress_rules: [
        'redis-tcp',
      ],
      ingress_cidr_blocks: [
        '0.0.0.0/0',
      ],
      ingress_rules: [
        'redis-tcp',
      ],
      name: '%s-redis-security-group' % clusterId,
      source: 'terraform-aws-modules/security-group/aws',
      version: '3.4.0',
      vpc_id: '${data.aws_vpc.default_vpc.id}',
    },

    {
      nested_blocks:: ['resource', 'aws_elasticache_cluster', '%s_redis_cluster' %
                                                              clusterId],

      clusterId: '%s-memory-store' % clusterId,
      engine: 'redis',
      engine_version: '5.0.6',
      node_type: instanceType,
      num_cache_nodes: 1,
      parameter_group_name: 'default.redis5.0',
      port: port,
      security_group_ids: [
        '${module.%s_redis_security_group.this_security_group_id}' % clusterId,
      ],
      subnet_group_name: '${aws_elasticache_subnet_group.%s_subnet_group.name}' %
                         clusterId,
    },

    {
      nested_blocks:: ['resource', 'aws_elasticache_subnet_group', '%s_subnet_group' %
                                                                   clusterId],

      name: '%s-redis-cluster-subnet' % clusterId,
      subnet_ids: '${data.aws_subnet_ids.all_subnets.ids}',
    },

    {
      nested_blocks:: ['output', output],

      value: '${aws_elasticache_cluster.%s_redis_cluster.cache_nodes.0.address}' %
             clusterId,
    },
  ] + _getAwsVpcData();

/**
 * Creates a new S3 bucket
 *
 * @param bucketName The name of the bucket
 * @param outputName The name of the key to use in terraform output
 * @param forceDestroy Whether to force destruction of this bucket. Needed for
 *                     terraform to tear down the bucket.
 *
 * @return an S3 bucket
 */
local newS3Bucket(bucketName, outputName=null, forceDestroy=true) =

  local output =
    if outputName == null then
      's3_%s' % bucketName
    else
      outputName;

  [
    {
      nested_blocks:: ['resource', 'aws_s3_bucket', 's3_bucket_%s' % bucketName],

      bucket: bucketName,
      force_destroy: forceDestroy,
    },

    {
      nested_blocks:: ['output', output],

      value: '${aws_s3_bucket.s3_bucket_%s.region}/${aws_s3_bucket.s3_bucket_%s.bucket}' % [bucketName, bucketName],
    },
  ];

/**
 * Creates a new Route53 hosted zone with a given name
 *
 * @param zoneName The name of the hosted zone
 *
 * @return a new Route53 DNS hosted zone
 */
local newRoute53HostedZone(zoneName) =
  [
    {
      nested_blocks:: ['resource', 'aws_route53_zone', 'hosted_zone_%s' %
                                                       Utils.sanitiseHostname(zoneName)],

      name: zoneName,
    },
  ];

/**
 * Gets a Route53 hosted zone with a given name
 *
 * @param zoneName The name of the hosted zone
 *
 * @return data for the specified hosted zone
 */
local _getAwsHostedZone(zoneName) =
  [
    {
      nested_blocks:: ['data', 'aws_route53_zone', 'hosted_zone_%s' %
                                                   Utils.sanitiseHostname(zoneName)],

      name: zoneName,
    },
  ];

/**
 * Creates a new Route53 DNS Record
 *
 * @param name The hostname for the record
 * @param type The type of DNS record, e.g. CNAME
 * @param records List of record values
 * @param zoneName The Route53 hosted zone to use
 * @param ttl The TTL of the record
 * @param outputName The name of the key to use in terraform output
 *
 * @return a new DNS record in the specified hosted zone
 */
local newRoute53Record(name,
                       zoneName,
                       type,
                       records,
                       ttl='300',
                       outputName=null) =

  local output =
    if outputName == null then
      Utils.sanitiseHostname(name)
    else
      outputName;

  [
    {
      nested_blocks:: ['resource', 'aws_route53_record', 'record_%s' %
                                                         Utils.sanitiseHostname(name)],

      name: name,
      type: type,
      records: records,
      ttl: ttl,
      zone_id: '${data.aws_route53_zone.hosted_zone_%s.zone_id}' %
               Utils.sanitiseHostname(zoneName),
    },

    {
      nested_blocks:: ['output', output],

      value: '${aws_route53_record.record_%s.name}' %
             Utils.sanitiseHostname(name),
    },
  ] + _getAwsHostedZone(zoneName);

/**
 * Creates a new IAM policy
 *
 * @param name The name for the new IAM policy
 * @param policy JSON for the policy definition
 *
 * @return a new IAM policy
 */
local newIamPolicy(name, policy) =
  [
    {
      nested_blocks:: ['resource', 'aws_iam_policy', name],

      name: name,
      policy: std.manifestJsonEx(policy, '  '),
    },
  ];


{
  nodeGroupConfig:: nodeGroupConfig,
  newAwsProvider:: newAwsProvider,
  newK8sCluster:: newEksCluster,
  newMemoryStore:: newRedisCluster,
  newObjectStore:: newS3Bucket,
  newDnsRecord:: newRoute53Record,
  newDnsZone:: newRoute53HostedZone,
  newIamPolicy:: newIamPolicy,
}
