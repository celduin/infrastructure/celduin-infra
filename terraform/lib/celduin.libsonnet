/**
 * Celduin Libsonnet
 *
 * Leverages our terraform libsonnets to template out entire standalone
 * deployments. For example the newCacheCluster() function templates an entire
 * remote cache deployment.
 */

local Aws = import 'aws.libsonnet';
local Tf = import 'terraform.libsonnet';

/**
 * Templates an entire remote cache deployment, including DNS
 *
 * @param clusterId A unique ID for the cluster, used to namespace generated tf
 * @param dnsZone The DNS hosted zone for the DNS names
 * @param nodeGroups List of nodegroup configs to be passed to the EKS cluster
 * @param backendBucketName Name of an S3 bucket to store Terraform state in
 * @param awsRegion AWS region for these resources
 *
 * @return a full set of configs for a remote cache deployment
 */
local newCacheCluster(clusterId,
                      dnsZone,
                      nodeGroups=[Aws.nodeGroupConfig('m5.xlarge')],
                      backendBucketName='celduin-tf-state',
                      awsRegion='eu-west-1') =

  Aws.newK8sCluster(clusterId,
                    nodeGroups,
                    workerPolicies=[
                      '${aws_iam_policy.dns_manager_%s.arn}' % clusterId,
                      '${aws_iam_policy.loki_s3_access_%s.arn}' % clusterId,
                    ],) +

  Aws.newObjectStore('celduin-loki-%s' % clusterId, outputName='loki_bucket') +

  Aws.newDnsRecord('%s.%s' % [clusterId, dnsZone],
                   dnsZone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % clusterId],
                   ttl=300,
                   outputName='read_only_domain',) +

  Aws.newDnsRecord('push.%s.%s' % [clusterId, dnsZone],
                   dnsZone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % clusterId],
                   ttl=300,
                   outputName='read_write_domain',) +

  Aws.newDnsRecord('stats.%s.%s' % [clusterId, dnsZone],
                   dnsZone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % clusterId],
                   ttl=300,
                   outputName='stats_domain',) +

  Aws.newDnsRecord('browser.%s.%s' % [clusterId, dnsZone],
                   dnsZone,
                   'CNAME',
                   ['${aws_lb.%s_nlb.dns_name}' % clusterId],
                   ttl=300,
                   outputName='browser_domain') +

  // IAM policy required for Cert-manager to handle DNS-01 ACME challenges
  Aws.newIamPolicy(
    'dns_manager_%s' % clusterId,
    {
      Version: '2012-10-17',
      Statement: [
        {
          Effect: 'Allow',
          Action: 'route53:GetChange',
          Resource: 'arn:aws:route53:::change/*',
        },
        {
          Effect: 'Allow',
          Action: [
            'route53:ChangeResourceRecordSets',
            'route53:ListResourceRecordSets',
          ],
          Resource: 'arn:aws:route53:::hostedzone/*',
        },
        {
          Effect: 'Allow',
          Action: 'route53:ListHostedZonesByName',
          Resource: '*',
        },
      ],
    },
  ) +

  Aws.newIamPolicy(
    'loki_s3_access_%s' % clusterId,
    {
      Version: '2012-10-17',
      Statement: [
        {
          Effect: 'Allow',
          Action: [
            's3:ListBucket',
            's3:GetObject',
            's3:PutObject',
          ],
          Resource: [
            'arn:aws:s3:::celduin-loki-%s' % clusterId,
            'arn:aws:s3:::celduin-loki-%s/*' % clusterId,
          ],
        },
      ],
    },
  ) +

  Aws.newAwsProvider(region=awsRegion) +

  Tf.newTfBlock() +

  Tf.newS3Backend(
    backendBucketName,
    'remotecache/%s/terraform.tfstate' % clusterId,
    's3.%s.amazonaws.com' % awsRegion,
    awsRegion,
  );

{
  newCacheCluster: newCacheCluster,
  newTfFile: Tf.newTfFile,
}
