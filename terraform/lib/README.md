# Templating terraform

Due to terraform's JSON object structure specification being nested, we define
our terraform in a slightly counterintuitive way. Thanks to the nesting, we
cannot simply create abstractions at the top level in case of collisions.
Instead we flatten out the outer layers of config, up to and including the name
of the resource in terraform, specifying the nested structure as an array under
`nested_blocks` in the config for each object. The `nested_blocks` should be a
hidden field, so it doesn't appear in the generated JSON.

That is to say, to template something like:

```json
{
  "resource": {
    "aws_instance": {
      "unique_instance_name": {
        "foo": "bar",
        "baz": "foo",
        "pony": {
          "a": 1,
          "b": 2,
        }
      }
    }
  }
}
```

we move the outer three blocks into the `nested_blocks` field, like so:

```jsonnet
{
  nested_blocks:: ['resource', 'aws_instance', 'unique_instance_name'],

  foo: 'bar',
  baz: 'foo',
  pony: {
    'a': 1,
    'b': 2,
  },
}
```

This looks a bit closer to the configuration in HCL, which would be

```hcl
resource "aws_instance" "unique_instance_name" {
  foo = "bar"
  baz = "foo"
  pony = {
    a = 1
    b = 2
  }
}
```

Functions to create a template should return a list of these "Config"s, e.g.

```jsonnet
local NewFoo(foo) =
  [
    {
      nested_blocks:: ['resource', 'aws_instance', 'unique_instance_name'],

      foo: 'bar',
      baz: 'foo',
      pony: {
        'a': 1,
        'b': 2,
      },
    },
];
```

To compose templated snippets, one can then safely use the `+` operator to
compose the lists. The `newTfFile` function in `terraform.libsonnet` will
convert a list of these configs into a single terraform configuration file.

For example:

```jsonnet
local Config = {
  nested_blocks:: ['resource', 'aws_instance', 'unique_instance_name'],

  foo: 'bar',
  bar: 'foo',
};

newTfFile(Config)
```

will produce

```json
{
  "resource": {
    "aws_instance": {
      "unique_instance_name": {
        "foo": "bar",
        "bar": "foo",
      },
    },
  },
}
```

as expected.
