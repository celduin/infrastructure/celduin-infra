/**
 * Terraform Libsonnet
 *
 * This libsonnet contains templating for general Terraform configuration as
 * well as generating the top final .tf.json file.
 */

/**
 * Returns a sorted list of configs with name at depth
 *
 * Performs a lexicographical sort on the list of `nested_blocks` joined by '.'.
 * This should work and be safe, as "foo" < "foo.bar", and "." is not allowed
 * in the terraform keys.
 *
 * @param configs The configs to reduce
 * @param name The name of the key we want
 * @param depth The depth of the key we want
 *
 * @return a sorted list of configs with `name` at `depth`, consider as the list
 *         of subconfigs of the block `name` at this depth
 */
local _getSubLevel(configs, name, depth) =
  std.sort([
             config
             for config in configs
             if depth < std.length(config.nested_blocks)
             if config.nested_blocks[depth] == name
           ],
           keyF=function(x) std.join('.', x.nested_blocks));


/**
 * Recursive function for parsing configs into a terraform file
 * Used to nest generated lists of configs into the proper terraform structure
 *
 * @param configs List of config objects
 * @param name Name of the current block being constructed
 * @param depth Current depth into flattened nesting structure
 *
 * @return the nested configuration of `name` when found at depth `depth`,
 *         produced from configs in `configs`
 */
local _createLevel(configs, name, depth) =

  // Get configs at this level
  local subconfigs = _getSubLevel(configs, name, depth);

  // Get keys for next level
  local subnames = std.set([
    config.nested_blocks[depth + 1]
    for config in subconfigs
    if depth < std.length(config.nested_blocks) - 1
  ]);

  local base =
    // If we have config at this level of nesting, include it
    // Reliant on the sort performed in _getSubLevel
    if std.length(subconfigs[0].nested_blocks) == depth + 1 then
      // TODO: If we have multiple objects at this level, then we end up
      // silently ignoring any other than the first
      subconfigs[0]
    else
      {};

  // If we're at the top level, then we're done
  if std.length(subnames) == 0 then
    base

  // Otherwise generate the next level
  else base + {
    [subname]: _createLevel(configs=subconfigs, name=subname, depth=depth + 1)
    for subname in subnames
  };

/**
 * Function to create a full terraform config file from a list of config objects
 *
 * @param configs List of config objects
 *
 * @return a nested terraform-compatible file produced from `configs`
 */
local newTfFile(configs) =
  local topLevels = std.set([config.nested_blocks[0] for config in configs]);

  {
    [top_level]: _createLevel(configs, top_level, depth=0)
    for top_level in topLevels
  };

/**
 * Create a `terraform` block of config
 *
 * @param version The minimum terraform version
 *
 * @return a new `terraform` block with the specified version
 */
local newTfBlock(version='0.12.20') = [
  {
    nested_blocks:: ['terraform'],

    required_version: '>= %s' % version,
  },
];

/**
 * Create a new S3 backend for terraform, to store the state in
 *
 * @param bucketName The name of the S3 bucket to store state in
 * @param key The key name for the stored state
 * @param endpoint The S3 endpoint to use
 * @param region The S3 region to use
 * @param skipCreds Skip credential validation by STS API
 *
 * @return an S3 backend for terraform
 */
local newS3Backend(bucketName, key, endpoint, region, skipCreds=true) =
  [
    {
      nested_blocks:: ['terraform', 'backend', 's3'],

      bucket: bucketName,
      endpoint: endpoint,
      key: key,
      region: region,
      skip_credentials_validation: skipCreds,
    },
  ];


{
  newTfFile:: newTfFile,
  newTfBlock:: newTfBlock,
  newS3Backend:: newS3Backend,
}
