/**
 * Dev Terraform
 *
 * Terraform to create a dev cluster
 */

local NodeTypes = import '../common/node-types.libsonnet';
local Aws = import 'lib/aws.libsonnet';
local Tf = import 'lib/terraform.libsonnet';
local Utils = import 'lib/util.libsonnet';

local DEFAULT_LABEL = Utils.createNodetype(nodeType=NodeTypes.default);
local WORKER_LABEL = Utils.createNodetype(nodeType=NodeTypes.worker);

local M5_XLARGE_NODEGROUP = Aws.nodeGroupConfig('m5.xlarge', nodeLabels=[DEFAULT_LABEL]);
local DEFAULT_WORKER_NODEGROUP = Aws.nodeGroupConfig('m5.large', nodeLabels=[WORKER_LABEL]);

local DevConfig = Aws.newK8sCluster(
                    'dev',
                    nodeGroups=[
                      M5_XLARGE_NODEGROUP,
                      DEFAULT_WORKER_NODEGROUP, /* Position is important! If the worker nodegroup changes place, update scripts/worker-group-name.py */
                    ],
                  ) +
                  Aws.newAwsProvider() +
                  Tf.newTfBlock();

Tf.newTfFile(DevConfig)
