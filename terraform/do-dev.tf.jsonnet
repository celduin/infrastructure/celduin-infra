/**
 * Dev DigitalOcean Terraform
 *
 * Terraform to create a droplet, using DigitalOcean
 *
 * To use:
 * jsonnet --tla-str token=[TOKEN]--tla-str ssh_key=[DO_SSH_KEY] do-dev.tf.jsonnet > main.tf.json && terraform init && terraform apply
 */

function(token, ssh_key)
  local Do = import 'lib/do.libsonnet';
  local Tf = import 'lib/terraform.libsonnet';

  local DevConfig = Do.newDroplet(name='dev', image='ubuntu-20-04-x64', instance='s-2vcpu-4gb', volumes=[Do.newVolume(name='vol', size='100')], ssh_keys=[ssh_key]) +
                    Do.newDigitalOceanProvider(token) +
                    Tf.newTfBlock();

  Tf.newTfFile(DevConfig)
