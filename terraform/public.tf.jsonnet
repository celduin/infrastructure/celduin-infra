/**
 * Public Terraform
 *
 * Terraform to create the public cluster
 */

/**
 * Public Terraform
 *
 * @param dnsZone DNS Hosted zone to base domain names on
 * @param backendBucketName Name of an S3 bucket used to store Terraform state
 * @param awsRegion The AWS region to use for these resources
 *
 * @return Terraform defining a production-ready cluster
 */
function(dnsZone='aws.celduin.co.uk',
         backendBucketName='celduin-tf-state',
         awsRegion='eu-west-1')

  local Celduin = import 'lib/celduin.libsonnet';
  local Aws = import 'lib/aws.libsonnet';
  local Utils = import 'lib/util.libsonnet';
  local NodeTypes = import '../common/node-types.libsonnet';

  local DEFAULT_LABEL = Utils.createNodetype(nodeType=NodeTypes.default);
  local WORKER_LABEL = Utils.createNodetype(nodeType=NodeTypes.worker);

  local M5_XLARGE_NODEGROUP = Aws.nodeGroupConfig('m5.xlarge', nodeLabels=[DEFAULT_LABEL]);
  local DEFAULT_WORKER_NODEGROUP = Aws.nodeGroupConfig('m5.large', nodeLabels=[WORKER_LABEL]);

  local PublicConfig = Celduin.newCacheCluster(
    clusterId='public',
    dnsZone=dnsZone,
    nodeGroups=[
      M5_XLARGE_NODEGROUP,
      DEFAULT_WORKER_NODEGROUP, /* Position is important! If the worker nodegroup changes place, update scripts/worker-group-name.py */
    ],
    backendBucketName=backendBucketName,
  );


  Celduin.newTfFile(PublicConfig)
