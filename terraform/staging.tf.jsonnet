/**
 * Staging Terraform
 *
 * Terraform to create the staging cluster
 */

/**
 * Staging Terraform
 *
 * @param dnsZone DNS Hosted zone to base domain names on
 * @param backendBucketName Name of an S3 bucket used to store Terraform state
 * @param awsRegion The AWS region to use for these resources
 *
 * @return Terraform defining a staging cluster
 */
function(dnsZone='aws.celduin.co.uk',
         backendBucketName='celduin-tf-state',
         awsRegion='eu-west-1')

  local Celduin = import 'lib/celduin.libsonnet';
  local Aws = import 'lib/aws.libsonnet';
  local Utils = import 'lib/util.libsonnet';
  local NodeTypes = import '../common/node-types.libsonnet';

  local DEFAULT_LABEL = Utils.createNodetype(nodeType=NodeTypes.default);
  local WORKER_LABEL = Utils.createNodetype(nodeType=NodeTypes.worker);

  local M5_XLARGE_NODEGROUP = Aws.nodeGroupConfig('t5.large', nodeLabels=[DEFAULT_LABEL]);
  local DEFAULT_WORKER_NODEGROUP = Aws.nodeGroupConfig('t3.large', nodeLabels=[WORKER_LABEL]);

  local StagingConfig = Celduin.newCacheCluster(
    clusterId='staging',
    nodeGroups=[
      M5_XLARGE_NODEGROUP,
      DEFAULT_WORKER_NODEGROUP, /* Position is important! If the worker nodegroup changes place, update scripts/worker-group-name.py */
    ],
    dnsZone=dnsZone,
    backendBucketName=backendBucketName,
  );


  Celduin.newTfFile(StagingConfig)
