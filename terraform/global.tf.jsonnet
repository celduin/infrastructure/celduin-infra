/**
 * Terraform for infrastructure that must be deployed before anything else.
 *
 * Deploying this is an out of band operation by necessity, as our deployments
 * rely on both the AWS hosted zone and the S3 bucket created here. This library
 * also depends on the S3 bucket, so some bootstrapping is required.
 *
 * To bootstrap these resources, first deploy using
 *
 * ```bash
 * mkdir -p tf
 * jsonnet terraform/global.tf.jsonnet \
 *   --tla-code bootstrap=true \
 *   --tla-str dnsZone=[your.domain.here] \
 *   --tla-str backendBucketName=[your_bucket_name_here] \
 *   --tla-str awsRegion=[desired-region-here] \
 * > main.tf.json
 *
 * cd tf
 * terraform init
 * terraform apply
 * ```
 *
 * Then deploy using the same command, omitting the `bootstrap=true` line
 */

local Aws = import 'lib/aws.libsonnet';
local Tf = import 'lib/terraform.libsonnet';

/**
 * Global Terraform
 *
 * @param bootstrap Optionally disable the S3 backend, in order to bootstrap resources
 * @param dnsZone DNS Hosted Zone to use
 * @param backendBucketName Name of an S3 bucket to use to store terraform state
 * @param awsRegion AWS Region for these resources
 *
 * @return Terraform to create globally-required resources
 */
function(bootstrap=false,
         dnsZone='aws.celduin.co.uk',
         backendBucketName='celduin-tf-state',
         awsRegion='eu-west-1')

  local GlobalConfig =
    Aws.newDnsZone(zoneName=dnsZone) +

    Aws.newObjectStore(bucketName=backendBucketName, forceDestroy=false) +

    Tf.newTfBlock() +

    if !bootstrap then
      Tf.newS3Backend(
        backendBucketName,
        'global/terraform.tfstate',
        's3.%s.amazonaws.com' % awsRegion,
        awsRegion,
      )
    else [];

  Tf.newTfFile(GlobalConfig)
