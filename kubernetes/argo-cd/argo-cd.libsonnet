/**
 * argo-cd.libsonnet
 *
 * Library gathering all the different common elements used in the various Argo-CD definitions
 */

/**
 * Create a new Source
 *
 * @param repo - str - Url of the repository to monitor
 * @param targetRevision - str - Reference to track in that repository e.g: HEAD, master, commit SHA, etc.
 * @param path - str - Path to monitor inside the repository
 *
 * @return a configured Source
 */
local _source(repo, targetRevision, path) = {
  repoURL: repo,
  targetRevision: targetRevision,
  path: path,
};

/**
 * Create a new Destination
 *
 * @param server - str - Name of the server this application should be deployed to
 * @param namespace - str - The namespace to whic this application should be deployed to
 *
 * @return a configured Destination
 */
local _destination(server, namespace) = {
  server: server,
  namespace: namespace,
};

/**
 * Converts an array of coded destinations to an array of Destination objects
 *
 * @param destinations - array - Array of Argo CD Destinations, structured as follow 'namespace|server'
 *
 * @return An array of Destination objects
 */
local _destinations(destinations) = [
  {
    local dest = std.split(destination, '|'),

    namespace: dest[0],
    server: dest[1],
  }
  for destination in destinations
];

/**
 * Create a new SyncPolicy object
 *
 * @param prune - Optional - bool - set to true Argo CD will delete resources when they are not configured in Git
 * @param selfheal - Optional - bool - set to true Argo CD will trigger a sync when the cluster's state changes from the one defined in Git
 *
 * @return a fully configured SyncPolicy
 */
local _syncPolicy(prune=false, selfheal=false) = {
  automated: {
    prune: prune,
    selfheal: selfheal,
  },
};

/**
 * Create a new Argo-CD Application definition
 *
 * @param name - str - Unique name of the application you wish to create
 * @param project - str - Name of the project to which the application should be assigned
 * @param repo - str - URL of the repository to monitor
 * @param targetRevision - str - Target to monitor in the repository can be one of the following types: commit SHA, Tag, Branch
 * @param path - str - Path to files to keep monitor
 * @param server - str - URL of the Cluster where to deploy the Application
 * @param namespace - str - Namespace in which the Application should be deployed
 * @param finalizer - Optional - bool - set to true Argo CD will delete the Kubernetes elements linked if the Application is deleted
 * @param autoSync - Optional - bool - set to true enables Argo CD automatic sync
 * @param prune - Optional - bool - set to true Argo CD will delete resources when they are not configured in Git
 * @param selfheal - Optional - bool - set to true Argo CD will trigger a sync when the cluster's state changes from the one defined in Git
 *
 * @return A fully configured Argo CD Application resource
 */
local application(
  name,
  project='default',
  repo='https://gitlab.com/celduin/infrastructure/infrastructure-declarations.git',
  targetRevision='master',
  path='',
  server='https://kubernetes.default.svc',
  namespace='default',
  finalizer=true,
  autoSync=false,
  prune=false,
  selfheal=false
      ) = {
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'Application',
  metadata: {
    name: '%s-%s' % [name, project],
    namespace: 'argocd',
  } + (if finalizer then { finalizers: ['resources-finalizer.argocd.argoproj.io'] } else {}),
  spec: {
    project: project,
    source: _source(repo, targetRevision, path),
    destination: _destination(server, namespace),
  } + (if autoSync then { syncPolicy: _syncPolicy(prune, selfheal) } else {}),
};

/**
 * Create a new Argo-CD Project definition
 *
 * @param name - str - Unique name of the application you wish to create
 * @param argoNamespace - str - Name of the project to which the application should be assigned
 * @param description - str - Description of the Project
 * @param sourceRepos - array - Comma separated List of URL of repositories that can e monitored by this Project
 * @param destinations - array - Comma separated list of Argo CD Destinations, following this structure 'namespace|server' e.g: 'default|https: *kubernetes.default.svc'
 *
 * @return A fully configured Argo CD Project resource
 */
local project(
  name,
  argoNamespace='argocd',
  description,
  sourceRepos='https://gitlab.com/celduin/infrastructure/infrastructure-declarations.git',
  destinations='default|https://kubernetes.default.svc',
      ) = {
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'AppProject',
  metadata: {
    name: name,
    namespace: argoNamespace,
  },
  spec: {
    description: description,
    sourceRepos: std.split(sourceRepos, ','),
    destinations: _destinations(std.split(destinations, ',')),
    clusterResourceWhitelist: [
      {
        group: '*',
        kind: '*',
      },
    ],
  },
};


{
  application:: application,
  project:: project,
}
