/**
 * Traefik Libsonnet
 *
 * Template for Traefik ingress controller.
 */
local kube = import '../kube.libsonnet';
local routing = import 'routing.libsonnet';

/**
 * Creates an array of objects of corresponding to an  of traefik controller
 * deployment.
 *
 * @param name The name of the deployment.
 * @param namespace The namespace traefik resides in.
 * @param containerImage Desired traefik container image
 * @param replicas Desired number of traefik controlles replicas.
 * @param httpNodeport The nodeport that traefik should listen at for http services
 * @param httpsNodeport The nodeport that traefik should listen at for https services
 * @param serviceType Let's you specify what kind of service you want.
 * @param adminNodeport Nodeport to expose the Admin interface at
 * @param metricsNodeport Port on which to expose prometheus metrics
 *
 * @return a fully configured Traefik IngressController definition
 */
local deployment(name,
                 namespace,
                 containerImage,
                 replicas=1,
                 httpNodeport=30080,
                 httpsNodeport=30443,
                 serviceType='NodePort',
                 adminNodeport=30808,
                 metricsNodeport=8082,) = [
  kube.customResourceDefinition('%s.traefik.containo.us' % crd.name) {
    spec: {
      group: 'traefik.containo.us',
      version: 'v1alpha1',
      names: {
        kind: crd.kind,
        plural: crd.plural,
        singular: crd.singular,
      },
      scope: 'Namespaced',
    },
  }
  for crd in [
    { name: 'ingressroutes', kind: 'IngressRoute', plural: 'ingressroutes', singular: 'ingressroute' },
    { name: 'middlewares', kind: 'Middleware', plural: 'middlewares', singular: 'middleware' },
    { name: 'tlsoptions', kind: 'TLSOption', plural: 'tlsoptions', singular: 'tlsoption' },
    { name: 'ingressroutetcps', kind: 'IngressRouteTCP', plural: 'ingressroutetcps', singular: 'ingressroutetcp' },
    { name: 'ingressrouteudps', kind: 'IngressRouteUDP', plural: 'ingressrouteudps', singular: 'ingressrouteudp' },
    { name: 'tlsstores', kind: 'TLSStore', plural: 'tlsstores', singular: 'tlsstore' },
    { name: 'traefikservices', kind: 'TraefikService', plural: 'traefikservices', singular: 'traefikservice' },
  ]
] + [
  kube.deployment(name + '-controller', namespace) {
    spec: {
      replicas: replicas,
      selector: {
        matchLabels: {
          app_name: name,
        },
      },
      template: {
        metadata: {
          labels: {
            app_name: name,
          },
          annotations: {
            prometheus_io_scrape: 'true',
            prometheus_io_port: std.toString(metricsNodeport),
          },
        },
        spec: {
          serviceAccount: name,
          containers: [
            kube.container(name, containerImage) {
              args: [
                '--log.level=DEBUG',
                // Activate access logging
                '--accesslog=true',
                // Activates dashboard
                '--api.insecure=true',
                // Create desired entrypoints to the cluster
                '--entryPoints.web.address=:' + routing.entrypoints.web.port,
                '--entryPoints.secure.address=:' + routing.entrypoints.secure.port,
                // Enable kubernetes ingress provider to watch incoming ingress events.
                '--providers.kubernetescrd',
                // Enable Prometheus
                '--metrics.prometheus=true',
                // Enable metrics in entryPoints
                '--metrics.prometheus.addEntryPointsLabels=true',
                // Enable metrics on services
                '--metrics.prometheus.addServicesLabels=true',
                // Create entrypoint to expose prometheus metrics
                '--entryPoints.metrics.address=:' + metricsNodeport,
                '--metrics.prometheus.entryPoint=metrics',
                // Add a http -> https redirect
                '--entrypoints.web.http.redirections.entryPoint.to=websecure',
                '--entrypoints.web.http.redirections.entryPoint.scheme=https',
              ],
              ports: [
                {
                  containerPort: routing.entrypoints.web.port,
                  protocol: routing.entrypoints.web.protocol,
                },
                {
                  containerPort: routing.entrypoints.admin.port,
                  protocol: routing.entrypoints.admin.protocol,
                },
                {
                  containerPort: routing.entrypoints.secure.port,
                  protocol: routing.entrypoints.secure.protocol,
                },
                {
                  containerPort: routing.entrypoints.metrics.port,
                  protocol: routing.entrypoints.metrics.protocol,
                },
              ],
            },
          ],
        },
      },
    },
  },
  kube.clusterRole(name) {
    rules: [
      {
        apiGroups: [
          '',
        ],
        resources: [
          'services',
          'endpoints',
          'secrets',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
      {
        apiGroups: [
          'extensions',
        ],
        resources: [
          'ingresses',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
      {
        apiGroups: [
          'extensions',
        ],
        resources: [
          'ingresses/status',
        ],
        verbs: [
          'update',
        ],
      },
      {
        apiGroups: [
          'traefik.containo.us',
        ],
        resources: [
          'middlewares',
          'ingressroutes',
          'tlsoptions',
          'traefikservices',
          'ingressroutetcps',
          'ingressrouteudps',
          'tlsstores',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
    ],
  },
  kube.clusterRoleBinding(name) {
    roleRef: {
      apiGroup: 'rbac.authorization.k8s.io',
      kind: 'ClusterRole',
      name: name,
    },
    subjects: [
      {
        kind: 'ServiceAccount',
        name: name,
        namespace: namespace,
      },
    ],
  },
  kube.serviceAccount(name, namespace) {
    automountServiceAccountToken: true,
  },
  kube.service(name, namespace) {
    spec: {
      selector: {
        app_name: name,
      },
      ports: [
        {
          protocol: routing.entrypoints.web.protocol,
          port: routing.entrypoints.web.port,
          targetPort: routing.entrypoints.web.port,
          nodePort: httpNodeport,
          name: routing.entrypoints.web.name,
        },
        {
          protocol: routing.entrypoints.secure.protocol,
          port: routing.entrypoints.secure.port,
          targetPort: routing.entrypoints.secure.port,
          nodePort: httpsNodeport,
          name: routing.entrypoints.secure.name,
        },
        {
          protocol: routing.entrypoints.admin.protocol,
          port: routing.entrypoints.admin.port,
          targetPort: routing.entrypoints.admin.port,
          nodePort: adminNodeport,
          name: routing.entrypoints.admin.name,
        },
      ],
      type: serviceType,
    },
  },
];

{
  deployment:: deployment,
}
