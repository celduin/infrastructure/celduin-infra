/**
 * certmanager.libsonnet
 *
 * Utilities, templates and abstractions for creating cert-manager resources.
 */

local kube = import '../kube.libsonnet';

/**
 * Creates a boilerplate Issuer resource
 *
 * @param name The name of the Issuer resource
 * @paran namespace The namespace for the Issuer
 * @param labels Optionally add labels to the Issuer
 * @param annotations Optionally add annotations to the Issuer
 *
 * @return a boilerplate Issuer resource
 */
local _issuer(name, namespace, labels={}, annotations={}) =
  {
    apiVersion: 'cert-manager.io/v1alpha2',
    kind: 'Issuer',
    metadata: {
      name: name,
      namespace: namespace,
      [if labels != {} then 'labels']: labels,
      [if annotations != {} then 'annotations']: annotations,
    },
  };

/**
 * Creates a new Issuer resource for an ACME server
 *
 * @param name The name of the Issuer resource
 * @param namespace The namespace for the Issuer resource
 * @param email The email for the ACME account (for renewal emails, etc.)
 * @param server The address of the ACME server
 * @param privateKeySecretRef Name of a Kubernetes secret which will be used to
 *                            store the private key of the ACME account
 * @param solvers A list of Solvers to address the ACME challenges
 * @param labels Optionally add labels to the Issuer
 * @param annotations Optionally add annotations to the Issuer
 *
 * @return a fully configured Issuer resource for use with an ACME server
 */
local ACMEIssuer(
  name,
  namespace,
  email,
  server,
  privateKeySecretRef,
  solvers,
  labels={},
  annotations={}
      ) =
  _issuer(name, namespace, labels, annotations) {
    spec: {
      acme: {
        email: email,
        server: server,
        privateKeySecretRef: {
          name: privateKeySecretRef,
        },
        solvers: solvers,
      },
    },
  };

/**
 * Solver for ACME dns-01 challenges for use with AWS Route53
 *
 * The EC2 instance cert-manager is run on is expected to have an attached IAM
 * policy for access to Route53. If it does not, then this will not work.
 *
 * @param region The AWS region to use
 * @param dnsNames List of DNS names this solver is for
 *
 * @return a configured Solver for use with AWS Route53
 */
local route53Solver(region, dnsNames) =
  {
    dns01: {
      route53: {
        region: region,
      },
    },
    selector: {
      dnsNames: dnsNames,
    },
  };

/**
 * Creates a new Certificate resource
 *
 * Cert-manager abstraction for a single X.509 certificate.
 *
 * @param name The name of the Certificate
 * @param namespace The namespace for the Certificate
 * @param secretName Name of a Kubernetes Secret to store the certificate in
 * @param dnsNames List of DNS names for the certificate
 * @param issuerRef Reference to the Issuer resource to use
 * @param duration Duration for the certificate to last. Defaults to 90d
 * @param renewBefore Amount of time before expiry at which to renew. Defaults to 15d
 * @param labels Optionally add labels to the Certificate
 * @param annotations Optionally add annotations to the Certificate
 *
 * @return a fully configured Certificate resource
 */
local certificate(name,
                  namespace,
                  secretName,
                  dnsNames,
                  issuerRef,
                  duration='2160h',
                  renewBefore='360h',
                  labels={},
                  annotations={}) =
  {
    apiVersion: 'cert-manager.io/v1alpha2',
    kind: 'Certificate',
    metadata: {
      name: name,
      namespace: namespace,
      [if labels != {} then 'labels']: labels,
      [if annotations != {} then 'annotations']: annotations,
    },
    spec: {
      secretName: secretName,
      dnsNames: dnsNames,
      duration: duration,
      renewBefore: renewBefore,
      issuerRef: issuerRef,
    },
  };

{
  certificate:: certificate,
  ACMEIssuer:: ACMEIssuer,
  route53Solver:: route53Solver,
}
