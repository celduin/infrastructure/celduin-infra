/**
 * routing.libsonnet
 *
 * Contains abstractions for the traefik CRD used for routing traffic.
 */

local kube = import '../kube.libsonnet';

// Data for entrypoints to the cluster
local entrypoints = {
  web: {
    protocol: 'TCP',
    port: 80,
    name: 'web',
  },
  admin: {
    protocol: 'TCP',
    port: 8080,
    name: 'admin',
  },
  secure: {
    protocol: 'TCP',
    // Port 443 uses https by default if it is defined in the service too.
    port: 443,
    name: 'secure',
  },
  metrics: {
    protocol: 'TCP',
    // Port 443 uses https by default if it is defined in the service too.
    port: 8082,
    name: 'metrics',
  },
};

/**
 * Define a bare Traefik IngressRoute resource
 *
 * @param name The name for the IngressRoute
 * @param namespace The Kubernetes Namespace for this resource
 *
 * @return An empty IngressRoute object
 */
local _traefikIngress(name, namespace) = {
  apiVersion: 'traefik.containo.us/v1alpha1',
  kind: 'IngressRoute',
  metadata: {
    name: name,
    namespace: namespace,
  },
};

/**
 * Define a bare Traefik TLSOption resource
 *
 * @param name The name for the TLSOption
 * @param namespace The Kubernetes Namespace for this resource
 *
 * @return An empty TLSOption object
 */
local _traefikTLSOptions(name, namespace) = {
  apiVersion: 'traefik.containo.us/v1alpha1',
  kind: 'TLSOption',
  metadata: {
    name: name,
    namespace: namespace,
  },
};

/**
 * Defines a fully configured IngressRoute resource.
 *
 * @param name The name for the IngressRoute
 * @param namespace The Kubernetes Namespace for this resource
 * @param entryPoints A list of entryPoint names to listen on
 * @param routes A list of traefik route configurations used by this IngressRoute
 * @param tls TLS configuration for this IngressRoute
 *
 * @return A fully configured IngressRoute object.
 */
local ingress(name, namespace, entryPoints, routes, tls) =
  _traefikIngress(name, namespace) {
    spec: {
      entryPoints: entryPoints,
      routes: routes,
      tls: tls,
    },
  };

/**
 * Creates a route configuration for Traefik
 *
 * @param match The expression to match
 * @param services List of services (in the traefik sense) to forward traffic to
 * @param middlewares List of Middleware resources (name and namespace) to apply.
 *
 * @return a fully configured traefik route
 */
local route(match, services, middlewares=[]) = {
  kind: 'Rule',
  match: match,
  middlewares: middlewares,
  services: services,
};

/**
 * Creates a traefik service configuration to be used in a route
 *
 * @param name The name of the Service
 * @param namespace The Kubernetes Namespace of the service
 * @param port The port to forward traffic to
 * @param kind The kind of Service - Either 'Service' or 'TraefikService'
 * @param scheme The protocol used for communication. E.g. http,https,h2c
 *
 * @return A fully configured traefik service configuration for a route
 */
local service(name, namespace, port, kind='Service', scheme='http') = {
  kind: kind,
  name: name,
  namespace: namespace,
  port: port,
  scheme: scheme,
};

/**
 * Creates TLS configuration for a traefik IngressRoute
 *
 * @param tlsSecret The _name_ of a Kubernetes Secret containing a Key and Certificate
 *                  to be used for server TLS
 * @param options A dict holding a name, namespace reference to a TLSOption
 *
 * @return TLS configuration for a traefik IngressRoute
 */
local tls(tlsSecret, options) = {
  secretName: tlsSecret,
  options: options,
};

/**
 * Creates a fully configured TLSOption configuration for traefik
 *
 * @param name The name of the TLSOption
 * @param namespace The Kubernetes Namespace for the resource
 * @param minVersion The minimum version of TLS to accept
 * @param cipherSuites List of cipher suites to use for encryption
 * @param clientAuth Whether to use mutual TLS authentication
 * @param clientTLSSecrets List of names of Kubernetes secrets containing a key and
 *                         certificates for client authentication
 *
 * @return A fully configured traefik TLSOption resource
 */
local tlsOption(name,
                namespace,
                minVersion='VersionTLS12',
                cipherSuites=[
  'TLS_AES_128_GCM_SHA256',
  'TLS_AES_256_GCM_SHA384',
  'TLS_CHACHA20_POLY1305_SHA256',
  'TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256',
  'TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256',
  'TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384',
  'TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384',
  'TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256',
  'TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256',
],
                clientAuth=false,
                clientTLSSecrets=[]) =
  _traefikTLSOptions(name, namespace) {
    spec: {
      minVersion: minVersion,
      cipherSuites: cipherSuites,
      [if clientAuth then 'clientAuth']: {
        secretNames: clientTLSSecrets,
        clientAuthType: 'RequireAndVerifyClientCert',
      },
    },
  };

/**
 * Creates a Kubernetes secret containing a TLS key/cert pair for use with traefik
 *
 * @param name The name of the Secret
 * @param namespace The Kubernetes Namespace for the secret
 * @param key The TLS Key to use
 * @param cert The TLS Certificate to use
 *
 * @return A Kubernetes Secret containing key and cert for use with traefik
 */
local tlsSecret(name, namespace, key, cert) =
  kube.secret(name, namespace) {
    data: {
      'tls.key': std.base64(key),
      'tls.crt': std.base64(cert),
      'tls.ca': std.base64(cert),
    },
  };

{
  entrypoints:: entrypoints,
  ingress:: ingress,
  route:: route,
  service:: service,
  tls:: tls,
  tlsOption:: tlsOption,
  tlsSecret:: tlsSecret,
}
