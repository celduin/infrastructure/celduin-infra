# Remote Execution Kubernetes Library

This folder contains abstractions used to define a Remote Cache and Execution
service compatible with [BuildStream] and [Bazel].

Currently we only support [BuildBarn] as a remote execution/cache backend.
Abstractions used for BuildBarn configuration and deployment can be found in
`buildbarn.libsonnet`.

BuildStream requires a service of its own, called the artifact server.
Abstractions for this can be found in `artifactCache.libsonnet`. Due to
limitations with `bst-artifact-server`, this must exist as a stateful singleton
at present.

Abstractions for an entire remote execution stack may be found in
`rex.libsonnet`. This contains much of the in-depth configuration of the
deployment, such as ports used for internal traffic and routing rules.

## Architecture

There are three distinct entrypoints to services in the remote-execution stack.
Firstly, we have a read-write endpoint, which forwards REAPI, ByteStream and
BuildStream gRPC calls into the BuildBarn backend or the BuildStream artifact
cache, as appropriate. Secondly, we have a read-only endpoint which forwards
read operations through to the BuildBarn backend, but has additional routing
rules to block operations which Write to the storage. Finally, we have an
endpoint to access the `bb-browser` instance, used to inspect the CAS and Action
Cache.

The below diagram describes the topology in more detail:

```mermaid
graph LR;
  %% Nodes
  ingress[Traefik]:::loadBalancer;

  bb-frontend[bb-storage Frontend]:::service;

  bb-forbidden[bb-storage Forbidden]:::service;
  bst-artifact-server[BuildStream Artifact Server]:::service;
  bb-storage[bb-storage CAS and Action Cache]:::service;

  bb-scheduler[bb-scheduler]:::service;

  subgraph Worker
    bb-worker[bb-worker]:::service;
    bb-runner[bb-runner]:::service;
  end

  bb-browser[bb-browser]:::service;

  disk1[(On Disk Storage)]:::storage;
  disk2[(On Disk Storage)]:::storage;

  %% Edges
  ingress -- Auth Failed --> bb-forbidden
  ingress -- buildstream/ --> bst-artifact-server --> disk1;

  ingress -- build.bazel* --> bb-frontend;
  ingress -- google.bytestream* --> bb-frontend;

  ingress --> bb-browser --> bb-storage;

  bb-frontend --> bb-storage --> disk2;
  bb-frontend --> bb-scheduler --> bb-worker;

  %% Styling
  classDef loadBalancer fill:#591,stroke:#333;
  classDef service fill:#f75,stroke:#333;
  classDef storage fill:#57f,stroke:#333;
```

In practice, the "bb-storage Frontend" contains a sharding configuration, which
points to several instances of "bb-storage" which each contain a shard of
storage, stored locally on disk. Similarly there are several worker/runner pods
each of which are pointed at by the scheduler. The bb-browser has the same storage
configuration as the frontend, in order to minimise traffic.

In addition to the default on-disk storage methods, we also have abstractions
for several other bb-storage BlobAccess configurations. Particularly of note are
our S3 and Redis abstractions for the storage backend. We also include abstractions
for the new LocalBlobAccess, albeit an experimental modified version which has
a persistent block-device backed store. For a full list, see
[the buildbarn libsonnet](./buildbarn.libsonnet).

[//]: # (EXTERNAL LINKS BLOCK)

[BuildStream]: https://buildstream.build
[Bazel]: https://bazel.build
[BuildBarn]: https://github.com/buildbarn
