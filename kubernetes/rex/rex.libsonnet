/**
 * rex.libsonnet
 *
 * For deploying the microservices corresponding to the REAPI stack
 */
local NodeTypes = import '../../common/node-types.libsonnet';
local routing = import '../ingress/routing.libsonnet';
local kube = import '../kube.libsonnet';
local artifactCache = import 'artifactCache.libsonnet';
local bb = import 'buildbarn.libsonnet';

// The port that the bb-storage daemon listens at
local BB_STORAGE_ENDPOINT_PORT = 5501;

// The port that the bb-worker daemon listens at
local BB_WORKER_ENDPOINT_PORT = 5502;

// The port that the artifact cache daemon listens at
local ARTIFACT_CACHE_ENDPOINT_PORT = 1101;

// The port that bb-browser listens at
local BB_BROWSER_ENDPOINT_PORT = 80;

// The endpoint of the artifact cache
local ARTIFACT_CACHE_ENDPOINT = 'storage-artifact';

// The endpoint of bb-storage acting as the demultiplex
local DEMULTIPLEX_ENDPOINT = 'demultiplex';

// The endpoint of bb-scheduler acting as the scheduler
local SCHEDULER_ENDPOINT = 'scheduler';

// the endpoint of bb-storage acting as the storage backend
local STORAGE_ENDPOINT = 'storage-cas';

// The endpoint of bb-storage acting as a deny endpoint
local FORBIDDEN_ENDPOINT = 'forbidden';

// The endpoint of bb-browser
local BROWSER_ENDPOINT = 'browser';

// The maximum size used for messages sent to bb-storage
local MAX_MESSAGE_SIZE_BYTES = 16 * 1024 * 1024;

// Instance name for remote execution services
local REMOTE_EXECUTION_INSTANCE = 'remote-execution';

// Name for the Read Only TLSOption resource
local READ_ONLY_TLS_OPTION = 'readonlyssl';

// Name for the Read Write TLSOption resource
local READ_WRITE_TLS_OPTION = 'readwritessl';

/**
 * Create a nodeAffinity to limit allocation based on `type` node label
 *
 * @param nodeType The value of `type` required to allocate the pod
 *
 * @return a Kubernetes Affinity to select nodes based upon nodeType
 */
local limitNodeType(nodeType) = bb.addAffinity(
  kube.affinity(
    nodeAffinity=kube.nodeAffinity(
      required=true,
      selectors=[kube.nodeSelector(
        key='type',
        operator='In',
        values=[nodeType],
      )],
    ),
  ),
);

/**
 * Create shards for a ShardingBlobAccess, of equal weight. Each shard is a gRPC
 * BlobAccess pointing to a pod in a StatefulSet.
 *
 * @param storageEndpoint The endpoint for the storage. That is, the name of the StatefulSet
 * @param storageEndpointPort The port the backend bb-storage is listening on
 * @param numShards The number of storages in use
 *
 * @return a list of shards pointing to each pod in a StatefulSet
 */
local _shards(storageEndpoint, numShards, storageEndpointPort=BB_STORAGE_ENDPOINT_PORT) = [
  bb.shard(bb.grpc('%s-%d.%s:%s' % [storageEndpoint, n, storageEndpoint, storageEndpointPort]))
  for n in std.range(0, numShards - 1)
];

/**
 * Simple wrapper to hold information about a volume - its name and size
 *
 * @param name The volume's name
 * @param size The volume's size, in GiB
 * @param mount Whether to mount the volume
 *
 * @return an object representing a volume
 */
local _volumeSpec(name, size, mount=true) =
  {
    name: name,
    size: '%dGi' % size,
    mount: mount,
  };

/**
 * Create several persistent volume claims, with an identical size, along with
 * volumeMounts or volumeDevices for these claims, with the volume accessible
 * from /$name
 *
 * @param volumeSpecs List of VolumeSpecs as defined above.
 * @param namespace Namespace for the volume claims
 *
 * @return an object storing related volumes, mounts and devices
 */
local _storageVolumes(volumeSpecs, namespace) =
  {
    volumeClaims: [
      {
        metadata: {
          name: vol.name,
        },
        spec: {
          accessModes: [
            'ReadWriteOnce',
          ],
          volumeMode: if vol.mount then 'Filesystem' else 'Block',
          resources: {
            requests: {
              storage: vol.size,
            },
          },
        },
      }
      for vol in volumeSpecs
    ],
    volumeMounts: [
      kube.volumeMount(vol.name, '/%s' % vol.name)
      for vol in volumeSpecs
      if vol.mount
    ],
    volumeDevices: [
      kube.volumeDevice(vol.name, '/%s' % vol.name)
      for vol in volumeSpecs
      if !vol.mount
    ],
  };

/**
 * Creates kubernetes components for a remote execution deployment
 *
 * @param namespace The namespace to use for the deployment
 * @param storageImage The bb-storage image to use
 * @param browserImage The bb-browser image to use
 * @param schedulerImage The bb-scheduler image to use
 * @param browserDomain The domain for accessing bb browser
 * @param platforms An array of platforms that are used to define a group of workers.
 * @param defaultNodeAffinity The NodeAffinity applied to non-worker nodes
 * @param numShards The number of shards to use in the sharding backend
 * @param casShardSize The size in GiB of each shard of CAS
 * @param acShardSize The size in GiB of each shard of Action Cache
 * @param statelessReplicas The number of replicas for each demultiplex/forbidden proxy.
 * @param useLocalBlobAccess Whether to use experimental LocalBlobAccess backend
 * @param oldBlocks The number of Old Blocks in LocalBlobAccess (requires useLocalBlobAccess=true)
 * @param currentBlocks The number of Current Blocks in LocalBlobAccess (requires useLocalBlobAccess=true)
 * @param newBlocks The number of New Blocks in LocalBlobAccess (requires useLocalBlobAccess=true)
 * @param digestLocationMapSize The size of the digest location map in a LocalBlobAccess (requires useLocalBlobAccess=true)
 *
 * @returns an array of kubernetes definitions for all the buildbarn components
 *          in a deployment
 */
local _buildbarnComponents(namespace,
                           storageImage,
                           browserImage,
                           schedulerImage,
                           browserDomain,
                           platforms,
                           defaultNodeAffinity,
                           numShards=3,
                           casShardSize=250,
                           acShardSize=5,
                           statelessReplicas=3,
                           useLocalBlobAccess=false,
                           oldBlocks=8,
                           currentBlocks=24,
                           newBlocks=3,
                           digestLocationMapSize=2 * 1000 * 1000 * 1000) = {
  local sharding = bb.sharding(_shards(STORAGE_ENDPOINT, numShards)),
  local blobstore = bb.actionCache({
                      completenessChecking: sharding,
                    }) +
                    bb.contentAddressableStorage(sharding),

  local bbStorageListener = bb.grpcServer(endpoint=BB_STORAGE_ENDPOINT_PORT),
  local bbWorkerListener = bb.grpcServer(endpoint=BB_WORKER_ENDPOINT_PORT),
  components:: std.flattenArrays(
    [
      bb.component(
        name=bbStorage.name,
        image=storageImage,
        config=bbStorage.data,
        namespace=namespace,
        endpointPort=BB_STORAGE_ENDPOINT_PORT,
        replicas=bbStorage.replicas,
        service=true,
        specificationMixin=bbStorage.specMixin,
        deploymentMixin=bbStorage.deploymentMixin,
        deploymentType=bbStorage.deploymentType,
      )
      for bbStorage in [
        {
          // This facilitates a 30GiB buffer between the size allocated
          // for the shard, and what is allocated as the datafile size.
          assert casShardSize > 30,
          local storageVols = if useLocalBlobAccess then
            _storageVolumes([
              _volumeSpec('cas', casShardSize, false),
              _volumeSpec('ac', acShardSize, false),
              _volumeSpec('bb-state', 1, true),
            ], namespace)
          else
            _storageVolumes([_volumeSpec('storage', casShardSize)], namespace),

          local storageBlobstore = if useLocalBlobAccess then
            bb.actionCache(
              bb.localBlobAccess(
                backend=bb.blockDevice('/ac', '/bb-state/block-state-ac'),
                digestLocationMap=bb.fileBackedDigestLocationMap('/bb-state'),
                oldBlocks=oldBlocks,
                currentBlocks=currentBlocks,
                newBlocks=newBlocks,
                digestLocationMapSize=digestLocationMapSize,
                instances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''],
              )
            ) +
            bb.contentAddressableStorage(
              bb.localBlobAccess(
                backend=bb.blockDevice('/cas', '/bb-state/block-state-cas'),
                digestLocationMap=bb.fileBackedDigestLocationMap('/bb-state'),
                oldBlocks=oldBlocks,
                currentBlocks=currentBlocks,
                newBlocks=newBlocks,
                digestLocationMapSize=digestLocationMapSize,
              )
            )
          else
            bb.actionCache(bb.circular('/storage/ac', instances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''])) +
            bb.contentAddressableStorage(
              bb.circular(
                directory='/storage/cas',
                offsetFileSizeBytes=1 * 1024 * 1024 * 1024,
                dataFileSizeBytes=(casShardSize - 30) * 1024 * 1024 * 1024,
              )
            ),

          name: STORAGE_ENDPOINT,
          data: bb.bbStorageConfig(
            name='storage',
            blobstore=storageBlobstore,
            grpcServers=bb.grpcServer(
              endpoint=BB_STORAGE_ENDPOINT_PORT,
            ),
            allowAcUpdatesForInstances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''],
          ),
          replicas: numShards,
          specMixin: bb.addVolumeMountsToBuildbarnPod(volumeMounts=storageVols.volumeMounts) +
                     bb.addVolumeDevicesToBuildbarnPod(volumeDevices=storageVols.volumeDevices) +
                     bb.addInitContainersToBuildbarnPod(
                       initContainers=if useLocalBlobAccess then [] else [
                         {
                           name: 'setup-storage',
                           image: 'busybox:1.28',
                           command: ['sh', '-c', 'mkdir -p /storage/ac /storage/cas'],
                           volumeMounts: storageVols.volumeMounts,
                         },
                       ]
                     ) +
                     bb.addAffinity(defaultNodeAffinity),
          deploymentMixin: bb.addVolumeClaimsToBuildbarnStatefulSet(volumeClaims=storageVols.volumeClaims),
          deploymentType: kube.statefulSet,
        },
        {
          name: DEMULTIPLEX_ENDPOINT,
          data: bb.bbFrontendConfig(
            name='demultiplex',
            blobstore=blobstore,
            grpcServers=bbStorageListener,
            schedulers={
              ['%s' % REMOTE_EXECUTION_INSTANCE]: { address: SCHEDULER_ENDPOINT + ':' + BB_STORAGE_ENDPOINT_PORT },
            },
            allowAcUpdatesForInstances=['%s' % REMOTE_EXECUTION_INSTANCE] + [''],
          ),
          specMixin: bb.addAffinity(defaultNodeAffinity),
          deploymentMixin: {},
          replicas: statelessReplicas,
          deploymentType: kube.deployment,
        },
        {
          local err = bb.err(7),  // PERMISSION_DENIED gRPC error
          name: FORBIDDEN_ENDPOINT,
          data: bb.bbStorageConfig(
            name='forbidden',
            blobstore=bb.actionCache(err) + bb.contentAddressableStorage(err),
            grpcServers=bbStorageListener,
          ),
          specMixin: bb.addAffinity(defaultNodeAffinity),
          deploymentMixin: {},
          replicas: statelessReplicas,
          deploymentType: kube.deployment,
        },
      ]
    ] + [
      bb.component(
        name=SCHEDULER_ENDPOINT,
        image=schedulerImage,
        config=bb.bbSchedulerConfig(
          name='scheduler',
          contentAddressableStorage=bb.sharding(_shards(STORAGE_ENDPOINT, numShards)),
          demultiplexServers=bbStorageListener,
          workerServers=bbWorkerListener,
          browserUrl='https://%s' % browserDomain,
        ),
        namespace=namespace,
        specificationMixin=bb.addAffinity(defaultNodeAffinity),
        endpointPort=BB_STORAGE_ENDPOINT_PORT,
        workerPort=BB_WORKER_ENDPOINT_PORT,
        scheduler=true,
        service=true,
      ),
      bb.component(
        name=BROWSER_ENDPOINT,
        image=browserImage,
        config=bb.bbBrowserConfig(
          blobstore=blobstore,
          maxMessageSizeBytes=MAX_MESSAGE_SIZE_BYTES,
        ),
        namespace=namespace,
        specificationMixin=bb.addAffinity(defaultNodeAffinity),
        endpointPort=BB_BROWSER_ENDPOINT_PORT,
        workerPort=BB_WORKER_ENDPOINT_PORT,
        service=true,
      ),
    ] + [
      bb.worker(
        name=platform.name,
        workerImage=platform.workerImage,
        runnerBaseImage=platform.runnerBaseImage,
        runnerImage=platform.runnerImage,
        workerConfig=bb.bbWorkerConfig(
          name=platform.name,
          blobstore=blobstore,
          browserUrl='https://%s' % browserDomain,
          scheduler=SCHEDULER_ENDPOINT + ':' + BB_WORKER_ENDPOINT_PORT,
          instanceName=REMOTE_EXECUTION_INSTANCE,
          platform=platform.properties,
          numRunners=platform.numRunners,
          defaultExecutionTimeout=platform.defaultExecutionTimeout,
          maximumExecutionTimeout=platform.maximumExecutionTimeout,
          chroot=platform.chroot,
          workerConfigMixin=platform.workerConfigMixin,
        ),
        namespace=namespace,
        endpointPort=BB_WORKER_ENDPOINT_PORT,
        specificationMixin=limitNodeType(platform.nodeType),
        service=true,
        tmpfs=platform.tmpfs,
        workerDirectorySize=platform.workerDirectorySize,
        runnerMixin=if std.length(platform.capabilities) != 0 then
          bb.addCapabilitiesToContainer(platform.capabilities) else {}
      )
      for platform in platforms
    ],
  ),
};

/**
 * Defines a worker platform
 *
 * @param name The name of the platform
 * @param properties An array of key/value pairs defining the platform. Also see
 *             https: *github.com/bazelbuild/remote-apis/blob/master/build/bazel/remote/execution/v2/remote_execution.proto#L573
 * @param numRunners The number of runners per worker. Defaults to 8.
 * @param workerImage The docker image to use for the worker
 * @param runnerBaseImage The container image that bb-runner will run in
 * @param runnerImage The docker image to use for the runner
 * @param nodeType The value of the `type` Kubernetes label applied to the nodes dedicated to this platform
 * @param capabilities List of additional capabilities to allow the worker to use, e.g. "SYS_CHROOT"
 * @param workerConfigMixin Mixin to add to the worker configuration
 * @param defaultExecutionTimeout Default timeout for actions which don't specify a timeout
 * @param maximumExecutionTimeout Maximum possible timeout for an action
 * @param tmpfs A boolean to decide if the worker directory should be in a tmpfs
 * @param workerDirectorySize Size of the worker directory volume in GiB when not using tmpfs
 *
 * @return an object to represent an bb-worker's platform
 */
local platform(name,
               properties,
               numRunners=3,
               workerImage,
               runnerBaseImage,
               runnerImage,
               nodeType,
               capabilities=[],
               chroot=false,
               workerConfigMixin={},
               defaultExecutionTimeout='1800s',
               maximumExecutionTimeout='3600s',
               tmpfs=true,
               workerDirectorySize=20) = {
  name: name,
  properties: properties,
  numRunners: numRunners,
  workerImage: workerImage,
  runnerBaseImage: runnerBaseImage,
  runnerImage: runnerImage,
  capabilities: capabilities,
  chroot: chroot,
  workerConfigMixin: workerConfigMixin,
  nodeType: nodeType,
  defaultExecutionTimeout: defaultExecutionTimeout,
  maximumExecutionTimeout: maximumExecutionTimeout,
  tmpfs: tmpfs,
  workerDirectorySize: workerDirectorySize,
};

/**
 * Returns an array of kubernetes objects corresponding to a remote execution deployment
 *
 * @param namespace The kubernetes namespace that the rex stack should reside in
 * @param bbStorageImage The bb-storage image to use
 * @param bbBrowserImage The bb-browser image to use
 * @param artifactCacheImage The image and tag of the artifact cache
 * @param schedulerImage The bb-scheduler image to use
 * @param browserDomain The domain for accessing bb browser
 * @param readOnlyDomain The domain for the Read Only endpoint
 * @param readWriteDomain The domain for the Read/Write endpoint
 * @param platforms The platforms that are used to define a group of workers
 * @param defaultNodeAffinity NodeAffinity to apply to non-worker pods
 * @param readOnlyClientSSL Name of a secret containing TLS key pair for the Read Only Port. '' for no TLS
 * @param readWriteClientSSL Name of a secret containing TLS key pair for the Read/Write Port. '' for no TLS
 * @param serverSecretName Name of a secret containing a TLS key pair for server authentication
 * @param artifactStoreSize The size, in GiB, for the BuildStream artifact storage
 * @param statelessReplicas The number of replicas for each demultiplex/forbidden proxy.
 * @param numShards The number of shards to use in the sharding backend
 * @param casShardSize The size in GiB of each shard of CAS
 * @param acShardSize The size in GiB of each shard of Action Cache
 * @param useLocalBlobAccess Whether to use experimental LocalBlobAccess backend
 *
 * @return an array with preconfigured buildstream artifact cache and buildbarn components
 */
local deployment(namespace,
                 bbStorageImage,
                 bbBrowserImage,
                 artifactCacheImage,
                 schedulerImage,
                 browserDomain,
                 readOnlyDomain,
                 readWriteDomain,
                 platforms,
                 defaultNodeAffinity,
                 readOnlyClientSSL,
                 readWriteClientSSL,
                 serverSecretName,
                 artifactStoreSize=5,
                 statelessReplicas=3,
                 numShards=3,
                 casShardSize=250,
                 acShardSize=5,
                 useLocalBlobAccess=false) = std.flattenArrays([
  artifactCache.deployment(name=ARTIFACT_CACHE_ENDPOINT,
                           namespace=namespace,
                           image=artifactCacheImage,
                           port=ARTIFACT_CACHE_ENDPOINT_PORT,
                           storageSize=artifactStoreSize,
                           specificationMixin=defaultNodeAffinity),
  _buildbarnComponents(namespace,
                       storageImage=bbStorageImage,
                       browserImage=bbBrowserImage,
                       schedulerImage=schedulerImage,
                       browserDomain=browserDomain,
                       platforms=platforms,
                       numShards=numShards,
                       defaultNodeAffinity=defaultNodeAffinity,
                       casShardSize=casShardSize,
                       acShardSize=acShardSize,
                       useLocalBlobAccess=useLocalBlobAccess,
                       statelessReplicas=statelessReplicas).components,
  [
    routing.ingress(
      name='buildbarn-read-only',
      namespace=namespace,
      entryPoints=[routing.entrypoints.secure.name],
      // Note that path priority will mean that this always takes precedence
      // https://docs.traefik.io/routing/routers/#priority
      routes=[
        routing.route(match='Host(`%s`) && Path(`%s`)' % [readOnlyDomain, path], services=[routing.service(FORBIDDEN_ENDPOINT, namespace, port=BB_STORAGE_ENDPOINT_PORT, scheme='h2c')])
        for path in [
          '/buildstream.v2.ReferenceStorage/UpdateReference',
          '/buildstream.v2.ArtifactService/UpdateArtifact',
          '/buildstream.v2.SourceService/UpdateSource',
          '/build.bazel.remote.execution.v2.ContentAddressableStorage/BatchUpdateBlobs',
          '/google.bytestream.ByteStream/Write',
        ]
      ] + [
        routing.route(match='Host(`%s`) && PathPrefix(`%s`)' % [readOnlyDomain, path], services=[routing.service(DEMULTIPLEX_ENDPOINT, namespace, port=BB_STORAGE_ENDPOINT_PORT, scheme='h2c')])
        for path in [
          '/build.bazel',
          '/google.bytestream',
        ]
      ] + [
        routing.route(match='Host(`%s`) && PathPrefix(`%s`)' % [readOnlyDomain, path], services=[routing.service(ARTIFACT_CACHE_ENDPOINT, namespace, port=ARTIFACT_CACHE_ENDPOINT_PORT, scheme='h2c')])
        for path in [
          '/buildstream',
        ]
      ],
      tls=routing.tls(
        serverSecretName,
        options={
          name: READ_ONLY_TLS_OPTION,
          namespace: namespace,
        }
      ),
    ),
    routing.ingress(
      name='buildbarn-read-write',
      namespace=namespace,
      entryPoints=[routing.entrypoints.secure.name],
      routes=[
        routing.route(match='Host(`%s`) && PathPrefix(`%s`)' % [readWriteDomain, path], services=[routing.service(DEMULTIPLEX_ENDPOINT, namespace, port=BB_STORAGE_ENDPOINT_PORT, scheme='h2c')])
        for path in [
          '/build.bazel',
          '/google.bytestream',
        ]
      ] + [
        routing.route(match='Host(`%s`) && PathPrefix(`%s`)' % [readWriteDomain, path], services=[routing.service(ARTIFACT_CACHE_ENDPOINT, namespace, port=ARTIFACT_CACHE_ENDPOINT_PORT, scheme='h2c')])
        for path in [
          '/buildstream',
        ]
      ],
      tls=routing.tls(
        serverSecretName,
        options={
          name: READ_WRITE_TLS_OPTION,
          namespace: namespace,
        },
      ),
    ),
    routing.ingress(
      name='buildbarn-browser',
      namespace=namespace,
      entryPoints=[routing.entrypoints.secure.name],
      routes=[
        routing.route(match='Host(`%s`)' % browserDomain, services=[routing.service(BROWSER_ENDPOINT, namespace, port=BB_BROWSER_ENDPOINT_PORT, scheme='http')]),
      ],

      // Use Client TLS for the Read Only endpoint. Motivation for this is that
      // bb-browser allows download of blobs from CAS.
      tls=routing.tls(
        serverSecretName,
        options={
          name: READ_ONLY_TLS_OPTION,
          namespace: namespace,
        },
      )
    ),
  ] + [
    routing.tlsOption(
      READ_WRITE_TLS_OPTION,
      namespace,
      clientAuth=readWriteClientSSL != '',
      clientTLSSecrets=if readWriteClientSSL != '' then [readWriteClientSSL] else []
    ),
    routing.tlsOption(
      READ_ONLY_TLS_OPTION,
      namespace,
      clientAuth=readOnlyClientSSL != '',
      clientTLSSecrets=if readOnlyClientSSL != '' then [readOnlyClientSSL] else []
    ),
  ],
]);

{
  deployment:: deployment,
  platform:: platform,
  config:: {
    artifactCacheEndpoint:: ARTIFACT_CACHE_ENDPOINT,
    artifactCacheEndpointPort:: ARTIFACT_CACHE_ENDPOINT_PORT,
    bbStorageEndpointPort:: BB_STORAGE_ENDPOINT_PORT,
    bbBrowserEndpointPort:: BB_BROWSER_ENDPOINT_PORT,
    demultiplexEndpoint:: DEMULTIPLEX_ENDPOINT,
    storageEndpoint:: STORAGE_ENDPOINT,
    forbiddenEndpoint:: FORBIDDEN_ENDPOINT,
    browserEndpoint:: BROWSER_ENDPOINT,
  },
}
