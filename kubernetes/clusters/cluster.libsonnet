/**
 * cluster.libsonnet
 *
 * Abstractions to provide a full deployment of the remote execution service.
 * Deploys ArgoCD, the remote execution stack, the ingress routing and the
 * monitoring stack. Most shared configuration is specified here.
 */

local NodeTypes = import '../../common/node-types.libsonnet';
local argo = import '../argo-cd/argo-cd.libsonnet';
local cert = import '../ingress/certmanager.libsonnet';
local routing = import '../ingress/routing.libsonnet';
local traefik = import '../ingress/traefik.libsonnet';
local kube = import '../kube.libsonnet';
local monitoring = import '../monitoring/monitoring.libsonnet';
local bb = import '../rex/buildbarn.libsonnet';
local rex = import '../rex/rex.libsonnet';

/**
 * Defines the default ACME solver for a set of DNS names.
 *
 * The default is a Route53 solver.
 *
 * @param dnsNames List of DNS names for this solver
 *
 * @return a cert-manager ACME solver
 */
local defaultAcmeSolver(dnsNames) = cert.route53Solver(
  region='eu-west-1',
  dnsNames=dnsNames,
);

// NodeAffinity to limit deployment of containers to only node with the label `type=default`
local DEFAULT_NODE_AFFINITY = kube.affinity(
  nodeAffinity=kube.nodeAffinity(
    required=true,
    selectors=[kube.nodeSelector(
      key='type',
      operator='In',
      values=[NodeTypes.default],
    )],
  ),
);

// Port used by bb-storage
local bbPort = rex.config.bbStorageEndpointPort;

// Port used by the bst-artifact-server
local artifactPort = rex.config.artifactCacheEndpointPort;

/**
 * Utility to create an endpoint from a domain and a port
 *
 * @param domain The domain
 * @param port The port
 *
 * @returns String of the form "domain:port"
 */
local _constructEndpoint(domain, port) = domain + ':' + port;

/**
 * Create an entire deployment of Celduin-infra
 *
 * Also creates the kubernetes namespace
 *
 * @param namespace The name of the namespace.
 * @param statsDomain The domain for accessing all monitoring services on the cluster
 * @param readOnlyDomain The domain for accessing the read-only endpoint for the cache service
 * @param readWriteDomain The domain for accessing the read-write endpoint for the cache service
 * @param browserDomain The domain for accessing bb-browser
 * @param targetRevision The target revision for argo-cd
 * @param readOnlyClientSSL Name of a Kubernetes Secret containing a TLS cert/key pair to be used for client authorisation on Read Only endpoint. Empty for no client auth
 * @param readWriteClientSSL Name of a Kubernetes Secret containing a TLS cert/key pair to be used for client authorisation on Read/Write endpoint. Empty for no client auth
 * @param artifactStoreSize The size, in GiB, for the BuildStream artifact storage
 * @param bbReplicas The number of replicas for demultiplex/forbidden
 * @param numShards The number of shards to be used in the sharding storage
 * @param casShardSize The size of each shard for the CAS storage
 * @param acShardSize The size of each shard for the Action Cache storage
 * @param useLocalBlobAccess Whether to use an experimental LocalBlobAccess backend for storage
 * @param useCertManager Whether to use cert-manager to obtain certificates
 * @param letsEncryptEmail If useCertManager is true, then use this email for the Let's Encrypt ACME account
 * @param letsEncryptSolver The cert-manager ACME Solver to use for Let's Encrypt, if useCertManager is true
 * @param loggingBucket Name of an S3 bucket to be used for log storage. Blank for local storage
 *
 * @return an array with preconfigured kubernetes deployment for the cluster
 */
local deployment(namespace,
                 statsDomain,
                 readOnlyDomain,
                 readWriteDomain,
                 browserDomain,
                 targetRevision,
                 readOnlyClientSSL,
                 readWriteClientSSL,
                 serverSSL,
                 artifactStoreSize=5,
                 bbReplicas=3,
                 numShards=3,
                 casShardSize=250,
                 acShardSize=5,
                 useLocalBlobAccess=false,
                 useCertManager=true,
                 letsEncryptEmail='',
                 letsEncryptSolver={},
                 loggingBucket='') =
  {
    ['%s-%s.json' % [kubeObject.metadata.name, kubeObject.kind]]: kubeObject
    for kubeObject in
      std.flattenArrays(
        [
          [kube.namespace(namespace)],
          [
            argo.project(
              name='celduin-infra',
              description='Project representing Celduin kubernetes infrastructure',
              destinations='%s|https://kubernetes.default.svc,argocd|https://kubernetes.default.svc' % namespace,
            ),
            argo.application(
              name=namespace,
              project='celduin-infra',
              targetRevision=targetRevision,
              path='kubernetes/complete-solution',  //TODO: Remove this hardcoding.
              finalizer=true,
              autoSync=true,
              prune=true,
              namespace=namespace,
              server='https://kubernetes.default.svc',
            ),
          ],
          traefik.deployment(
            name='traefik-ingress',
            namespace=namespace,
            containerImage='traefik:v2.2.5',
          ),
          rex.deployment(
            namespace,
            bbStorageImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_storage:20200610T112904Z',
            bbBrowserImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_browser:20200601T123725Z',
            artifactCacheImage='buildstream/buildstream:dev',
            schedulerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_scheduler:20200601T123725Z',
            browserDomain=browserDomain,
            readOnlyDomain=readOnlyDomain,
            readWriteDomain=readWriteDomain,
            platforms=[
              rex.platform(
                name='ubuntu-16-04',
                properties=[
                  { name: 'OSFamily', value: 'Linux' },
                  { name: 'container-image', value: 'docker://marketplace.gcr.io/google/rbe-ubuntu16-04@sha256:6ad1d0883742bfd30eba81e292c135b95067a6706f3587498374a083b7073cb9' },
                ],
                workerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_worker:20200601T123725Z',
                runnerBaseImage='registry.gitlab.com/celduin/buildstream-bazel/minimal-rootfs/platform:84b362e4f28f6ba1b157fabe2feabf7c7e46bdab@sha256:2cc2a0dff205711588c50808be3ef20f64c8c82cebffab9a4356467c7fed5238',
                runnerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_runner_installer:20200717T082527Z',
                nodeType=NodeTypes.worker,
              ),
              rex.platform(
                name='buildstream',
                properties=[
                  { name: 'ISA', value: 'x86-64' },
                  { name: 'OSFamily', value: 'linux' },
                ],
                workerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_worker:20200601T123725Z',
                runnerBaseImage='registry.gitlab.com/celduin/buildstream-bazel/minimal-rootfs/platform:84b362e4f28f6ba1b157fabe2feabf7c7e46bdab@sha256:2cc2a0dff205711588c50808be3ef20f64c8c82cebffab9a4356467c7fed5238',
                runnerImage='registry.gitlab.com/celduin/infrastructure/celduin-infra/bb_runner_installer:20200717T082527Z',
                nodeType=NodeTypes.worker,
                capabilities=['SYS_CHROOT'],
                chroot=true,
                workerConfigMixin=bb.addFieldsToWorkerConfig(
                  devices=['null', 'zero', 'random', 'full']
                ),
                defaultExecutionTimeout='7200s',
                maximumExecutionTimeout='7200s',
                tmpfs=false,
              ),
            ],
            defaultNodeAffinity=DEFAULT_NODE_AFFINITY,
            numShards=numShards,
            casShardSize=casShardSize,
            acShardSize=acShardSize,
            useLocalBlobAccess=useLocalBlobAccess,
            readOnlyClientSSL=readOnlyClientSSL,
            readWriteClientSSL=readWriteClientSSL,
            statelessReplicas=bbReplicas,
            serverSecretName=serverSSL,
          ),
          monitoring.deployment(
            namespace=namespace,
            statsDomain=statsDomain,
            frontendDomain='nginx.' + namespace,
            grafanaImage='grafana/grafana:6.6.2',
            prometheusImage='prom/prometheus:latest',
            nodeExporterImage='prom/node-exporter:latest',
            grafanaSpecificationMixin=DEFAULT_NODE_AFFINITY,
            prometheusSpecificationMixin=DEFAULT_NODE_AFFINITY,
            lokiImage='grafana/loki:1.5.0',
            promtailImage='grafana/promtail:1.5.0',
            serverSecretName=serverSSL,
            lokiBucket=loggingBucket,
          ),
        ] + if useCertManager then [
          assert letsEncryptEmail != '';
          [
            cert.ACMEIssuer(
              name='letsencrypt',
              namespace=namespace,
              email=letsEncryptEmail,
              server='https://acme-v02.api.letsencrypt.org/directory',
              privateKeySecretRef='le-account',
              solvers=[
                if letsEncryptSolver == {} then defaultAcmeSolver(
                  [statsDomain, readOnlyDomain, readWriteDomain, browserDomain]
                )
                else letsEncryptSolver,
              ],
            ),
            cert.certificate(
              name='serversslcert',
              namespace=namespace,
              secretName=serverSSL,
              dnsNames=[
                statsDomain,
                readOnlyDomain,
                readWriteDomain,
                browserDomain,
              ],
              issuerRef={
                name: 'letsencrypt',
                kind: 'Issuer',
              },
            ),
          ],
        ] else [],
      )
    if kubeObject != null
  };

{
  deployment:: deployment,
}
