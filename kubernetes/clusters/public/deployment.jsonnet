/**
 * Public deployment configuration
 *
 * @param testGen Optionally avoid importing the terraform output in order to
 *                reliably test generation of JSON
 *
 * @return full configuration for a "public" cluster
 */
function(testGen=false)
  local cluster = import '../cluster.libsonnet';
  local tf = if testGen then {} else import '_tfOutput.jsonnet';

  cluster.deployment(
    namespace='rex',
    targetRevision='public',
    statsDomain=if testGen then 'stats' else tf.stats_domain.value,
    readOnlyDomain=if testGen then 'pull' else tf.read_only_domain.value,
    readWriteDomain=if testGen then 'push' else tf.read_write_domain.value,
    browserDomain=if testGen then 'browser' else tf.browser_domain.value,
    readOnlyClientSSL='',
    readWriteClientSSL='readwritessl',  // Note that the names of these secrets must match what is specified in certs.jsonnet
    serverSSL='serverssl',
    letsEncryptEmail='christopher.phang@codethink.co.uk',
    loggingBucket=if testGen then 'loki-s3' else tf.loki_bucket.value,
  )
