// deployment.jsonnet
//
// Top level definition for a dev cluster

local cluster = import '../cluster.libsonnet';

cluster.deployment(
  namespace='rex',
  targetRevision='dev',
  statsDomain='stats.cache.com',
  readOnlyDomain='pull.cache.com',
  readWriteDomain='push.cache.com',
  browserDomain='browser.cache.com',
  readOnlyClientSSL='readonlyssl',  // Note that the names of these secrets must match what is specified in certs.jsonnet
  readWriteClientSSL='readwritessl',
  serverSSL='serverssl',
  casShardSize=31,
  useLocalBlobAccess=false,
  bbReplicas=1,
  numShards=1,
  useCertManager=false,
)
