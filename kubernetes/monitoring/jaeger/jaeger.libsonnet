/**
 * Jaeger Libsonnet
 *
 * Template for jaeger configuration and deployment.
 */

local kube = import '../../kube.libsonnet';

// List of container images needed to deloy Jaeger
local containerImages = {
  collector: 'jaegertracing/jaeger-collector:1.17',
  query: 'jaegertracing/jaeger-query:1.9.0',
  cassandra: 'cassandra:3.11',
  schema: 'jaegertracing/jaeger-cassandra-schema:1.6.0',
};

// List of labels for each jaeger element
local labels = {
  collector: {
    app_name: 'jaeger',
    app_component: 'collector',
    app_part_of: 'grpc-tracing',
  },
  query: {
    app_name: 'jaeger',
    app_component: 'query',
    app_part_of: 'grpc-tracing',
  },
  configMap: {
    app_name: 'jaeger',
    app_component: 'configuration',
    app_part_of: 'grpc-tracing',
  },
  cassandra: {
    cassandra: {
      app_name: 'cassandra',
      app_component: 'storage-backend',
      app_part_of: 'grpc-tracing',
    },
    schema: {
      app_name: 'cassandra-schema',
      app_component: 'storage-backend',
      app_part_of: 'grpc-tracing',
    },
  },
};

// Ports used by the Collector
local jaegerCollectorPorts = {
  Tchannel: {
    name: 'jaeger-collector-tchannel',
    port: 14267,
    protocol: 'TCP',
  },
  Http: {
    name: 'jaeger-collector-http',
    port: 14268,
    protocol: 'TCP',
  },
};


// Ports used by jaeger-query
local jaegerQueryPort = {
  name: 'jaeger-query',
  port: 16686,
  protocol: 'TCP',
};

// Ports used by the Cassandra Database
local jaegerCassandraPorts = {
  intraNode: {
    name: 'intra-node',
    port: 7000,
  },
  tlsIntraNode: {
    name: 'tls-intra-node',
    port: 7001,
  },
  jmx: {
    name: 'jmx',
    port: 7199,
  },
  cql: {
    name: 'cql',
    port: 9042,
  },
  thrift: {
    name: 'thrift',
    port: 9160,
  },
};

// Jaeger configmap's data
local configData = {
  span_storage_type: 'cassandra',
  collector: {
    collector: {
      zipkin: {
        'http-port': 9411,
      },
    },
    cassandra: {
      servers: 'cassandra',
      keyspace: 'jaeger_v1_dc1',
    },
  },
  query: {
    cassandra: {
      servers: 'cassandra',
      keyspace: 'jaeger_v1_dc1',
    },
  },
  agent: {
    collector: {
      'host-port': 'jaeger-collector:14267',
    },
  },
};

/**
 * Creates the confipmap for jaeger Collector and Query
 *
 * @param name The desired name for the configMap
 * @param namespace The namespace in which the configmap should be deployed
 *
 * @return configured ConfigMap for Jaeger components
 */
local jaegerConfigMap(name, namespace) = [
  kube.configMap(name, namespace, labels.configMap) {
    data: {
      span_storate_type: std.toString(configData.span_storage_type),
      collector: std.toString(configData.collector),
      query: std.toString(configData.query),
      agent: std.toString(configData.agent),
    },
  },
];

/**
 * Deploys jaeger collector
 *
 * @param name The desired name for the collector
 * @param namespace The namespace in which the collector should be deployed
 * @param replicas Number of replicas of the collector desired
 *
 * @return a fully configured deployment of Jaeger Collector
 */
local jaegerCollector(name, namespace, replicas=1) = [
  kube.deployment(name, namespace, labels.collector) {
    spec: {
      replicas: replicas,
      strategy: {
        type: 'Recreate',
      },
      selector: {
        matchLabels: labels.collector,
      },
      template: {
        metadata: {
          labels: labels.collector,
        },
        spec: {
          containers: [
            kube.container(name, containerImages.collector) {
              args: [
                '--config-file=/conf/collector.yaml',
                '--cassandra.servers=cassandra',
              ],
              ports: [
                {
                  containerPort: jaegerCollectorPorts.Tchannel.port,
                  protocol: jaegerCollectorPorts.Tchannel.protocol,
                },
                {
                  containerPort: jaegerCollectorPorts.Http.port,
                  protocol: jaegerCollectorPorts.Http.protocol,
                },
              ],
              readinessProbe: {
                httpGet: {
                  path: '/',
                  port: 14269,
                },
              },
              volumeMounts: [
                {
                  name: 'jaeger-configuration-volume',
                  mountPath: '/conf',
                },
              ],
              env: [
                {
                  name: 'SPAN_STORAGE_TYPE',
                  valueFrom: {
                    configMapKeyRef: {
                      name: 'jaeger-configuration',
                      key: 'span-storage-type',
                    },
                  },
                },
              ],
            },
          ],
          volumes: [
            {
              configMap: {
                name: 'jaeger-configuration',
                items: [
                  {
                    key: 'collector',
                    path: 'collector.yaml',
                  },
                ],
              },
              name: 'jaeger-configuration-volume',
            },
          ],
        },
      },
    },
  },

  kube.service(name, namespace, labels.collector) {
    spec: {
      ports: [
        {
          name: jaegerCollectorPorts.Tchannel.name,
          port: jaegerCollectorPorts.Tchannel.port,
          protocol: jaegerCollectorPorts.Tchannel.protocol,
        },
        {
          name: jaegerCollectorPorts.Http.name,
          port: jaegerCollectorPorts.Http.port,
          protocol: jaegerCollectorPorts.Http.protocol,
        },
      ],
      selector: labels.collector,
      type: 'ClusterIP',
    },
  },
];

/**
 * Deploys jaeger Query
 *
 * @param name The desired name for the Query
 * @param namespace The namespace in which the Query should be deployed
 * @param replicas Number of replicas of the Query desired
 *
 * @return a fully configured deployment of Jaeger Query
 */
local jaegerQuery(name, namespace, replicas=1) = [
  kube.deployment(name, namespace, labels.query) {
    spec: {
      replicas: replicas,
      strategy: {
        type: 'Recreate',
      },
      selector: {
        matchLabels: labels.query,
      },
      template: {
        metadata: {
          labels: labels.query,
        },
        spec: {
          containers: [
            kube.container(name, containerImages.query) {
              args: [
                '--config-file=/conf/query.yaml',
                '--cassandra.servers=cassandra',
                '--query.base-path=/jaeger',
              ],
              ports: [
                {
                  containerPort: jaegerQueryPort.port,
                  protocol: jaegerQueryPort.protocol,
                },
              ],
              readinessProbe: {
                httpGet: {
                  path: '/',
                  port: 16687,
                },
              },
              volumeMounts: [
                {
                  name: 'jaeger-configuration-volume',
                  mountPath: '/conf',
                },
              ],
              env: [
                {
                  name: 'SPAN_STORAGE_TYPE',
                  valueFrom: {
                    configMapKeyRef: {
                      name: 'jaeger-configuration',
                      key: 'span-storage-type',
                    },
                  },
                },
              ],
            },
          ],
          volumes: [
            {
              configMap: {
                name: 'jaeger-configuration',
                items: [
                  {
                    key: 'query',
                    path: 'query.yaml',
                  },
                ],
              },
              name: 'jaeger-configuration-volume',
            },
          ],
        },
      },
    },
  },

  kube.service(name, namespace, labels.query) {
    spec: {
      ports: [
        {
          name: jaegerQueryPort.name,
          port: jaegerQueryPort.port,
          protocol: jaegerQueryPort.protocol,
        },
      ],
      selector: labels.query,
      type: 'ClusterIP',
    },
  },
];

/**
 * Deploys Cassandra database
 *
 * @param name The desired name for the Cassandra
 * @param namespace The namespace in which the Casandra should be deployed
 * @param replicas Number of replicas of the Cassandra desired
 *
 * @return a fully configured Cassandra deployment used by Jaeger
 */
local jaegerCassandra(name, namespace, replicas=3) = [
  kube.service(name, namespace, labels.cassandra.cassandra) {
    spec: {
      ports: [
        {
          name: jaegerCassandraPorts.intraNode.name,
          port: jaegerCassandraPorts.intraNode.port,
        },
        {
          name: jaegerCassandraPorts.tlsIntraNode.name,
          port: jaegerCassandraPorts.tlsIntraNode.port,
        },
        {
          name: jaegerCassandraPorts.jmx.name,
          port: jaegerCassandraPorts.jmx.port,
        },
        {
          name: jaegerCassandraPorts.thrift.name,
          port: jaegerCassandraPorts.thrift.port,
        },
        {
          name: jaegerCassandraPorts.cql.name,
          port: jaegerCassandraPorts.cql.port,
        },
      ],
      selector: labels.cassandra.cassandra,
      type: 'ClusterIP',
    },
  },

  kube.statefulSet(name, namespace, labels=labels.cassandra.cassandra) {
    spec: {
      serviceName: name,
      replicas: replicas,
      selector: {
        matchLabels: labels.cassandra.cassandra,
      },
      template: {
        metadata: {
          labels: labels.cassandra.cassandra,
        },
        spec: {
          terminationGracePeriodSeconds: 1800,
          containers: [
            kube.container(name, containerImages.cassandra) {
              command: [
                '/docker-entrypoint.sh',
                '-R',
              ],
              ports: [
                {
                  name: jaegerCassandraPorts.intraNode.name,
                  containerPort: jaegerCassandraPorts.intraNode.port,
                },
                {
                  name: jaegerCassandraPorts.tlsIntraNode.name,
                  containerPort: jaegerCassandraPorts.tlsIntraNode.port,
                },
                {
                  name: jaegerCassandraPorts.jmx.name,
                  containerPort: jaegerCassandraPorts.jmx.port,
                },
                {
                  name: jaegerCassandraPorts.thrift.name,
                  containerPort: jaegerCassandraPorts.thrift.port,
                },
                {
                  name: jaegerCassandraPorts.cql.name,
                  containerPort: jaegerCassandraPorts.cql.port,
                },
              ],
              lifecycle: {
                preStop: {
                  exec: {
                    command: [
                      '/bin/sh',
                      '-c',
                      'nodetool drain',
                    ],
                  },
                },
              },
              env: [
                {
                  name: 'MAX_HEAP_SIZE',
                  value: '512M',
                },
                {
                  name: 'HEAP_NEWSIZE',
                  value: '100M',
                },
                {
                  name: 'CASSANDRA_LISTEN_ADDRESS',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'status.podIP',
                    },
                  },
                },
                {
                  name: 'CASSANDRA_CLUSTER_NAME',
                  value: 'jaeger',
                },
                {
                  name: 'CASSANDRA_DC',
                  value: 'dc1',
                },
                {
                  name: 'CASSANDRA_RACK',
                  value: 'rack1',
                },
                {
                  name: 'CASSANDRA_ENDPOINT_SNITCH',
                  value: 'GossipingPropertyFileSnitch',
                },
                {
                  name: 'CASSANDRA_SEEDS',
                  value: 'cassandra-0.cassandra',
                },
              ],
              volumeMounts: [
                {
                  name: 'cassandra-data',
                  mountPath: '/var/lib/cassandra',
                },
                {
                  name: 'cassandra-logs',
                  mountPath: '/var/log/cassandra',
                },
              ],
            },
          ],
          volumes: [
            {
              name: 'cassandra-data',
              emptyDir: {},
            },
            {
              name: 'cassandra-logs',
              emptyDir: {},
            },
          ],
        },
      },
    },
  },

  kube.job('jaeger-' + name + '-schema-job', namespace, labels.cassandra.schema) {
    spec: {
      activeDeadlineSeconds: 120,
      template: {
        metadata: {
          name: name + '-schema',
        },
        spec: {
          containers: [
            kube.container('jaeger-' + name + '-schema', containerImages.schema)
            {
              env: [
                {
                  name: 'MODE',
                  value: 'prod',
                },
                {
                  name: 'DATACENTER',
                  value: 'dc1',
                },
              ],
            },
          ],
          restartPolicy: 'OnFailure',
        },
      },
    },
  },
];

{
  jaegerCollector:: jaegerCollector,
  jaegerQuery:: jaegerQuery,
  jaegerConfigMap:: jaegerConfigMap,
  jaegerCassandra:: jaegerCassandra,
}
