/**
 * loki.libsonnet
 *
 * Templates and abstractions for deploying Grafana's Loki
 */

local kube = import '../../kube.libsonnet';

// Ports used by a Loki deployment
local lokiPorts = {
  name: 'http',
  port: 3100,
  protocol: 'TCP',
};

// Labels attached to Loki Resources
local labels = {
  app: 'loki',
  stack: 'log_monitoring',
};

/**
 * Creates a new Loki Configuration
 *
 * This is configured to use S3 for the object store, and local storage for
 * Index.
 *
 * @param name The name of the configmap
 * @param s3Bucket Name of the s3 bucket to use. Blank for local storage
 *
 * @return a full loki configuration
 */
local config(name, s3Bucket) = {
  name: name,
  data: {
    auth_enabled: false,
    server: {
      http_listen_port: lokiPorts.port,
    },
    ingester: {
      lifecycler: {
        address: '127.0.0.1',
        ring: {
          kvstore: {
            store: 'inmemory',
          },
          replication_factor: 1,
        },
        final_sleep: '0s',
      },
      chunk_idle_period: '5m',
      chunk_retain_period: '30s',
    },
    schema_config: {
      configs: [
        {
          from: '2020-06-29',
          store: 'boltdb',
          object_store: if s3Bucket != '' then 's3' else 'filesystem',
          schema: 'v11',
          index: {
            prefix: 'index_',
            period: '168h',
          },
        },
      ],
    },
    storage_config: {
      [if s3Bucket != '' then 'aws']: {
        s3: 's3://%s' % s3Bucket,
      },
      boltdb: {
        directory: '/tmp/loki/index',
      },
      filesystem: {
        directory: '/tmp/loki/chunks',
      },
    },
    limits_config: {
      enforce_metric_name: false,
      reject_old_samples: true,
      reject_old_samples_max_age: '168h',
    },
  },
};

/**
 * Creates the Loki monolithic deployment
 *
 * @param name The desired name for Loki.
 * @param namespace The namespace in which Loki should be deployed.
 * @param containerImage Desired Loki image to be deployed.
 * @param replicas Number of Loki replicas to be deployed
 *
 * @return a fully configured Loki Deployment
 */
local deployment(name,
                 namespace,
                 containerImage,
                 config,
                 replicas=1) = [
  kube.deployment(name, namespace, labels) {
    spec: {
      replicas: replicas,
      selector: {
        matchLabels: labels,
      },
      template: {
        metadata: {
          labels: labels,
        },
        spec: {
          containers: [
            kube.container(name, containerImage) {
              args: ['-config.file=/conf/loki.yaml'],
              ports: [
                {
                  containerPort: lokiPorts.port,
                  protocol: lokiPorts.protocol,
                },
              ],
              volumeMounts: [
                {
                  mountPath: '/conf',
                  name: 'config',
                },
              ],
            },
          ],
          volumes: [
            {
              configMap: {
                name: name,
              },
              name: 'config',
            },
          ],
        },
      },
    },
  },
  kube.configMap(name, namespace) {
    data: {
      'loki.yaml': std.manifestYamlDoc(config.data),
    },
  },
  kube.service(name, namespace, labels) {
    spec: {
      selector: labels,
      ports: [
        {
          name: lokiPorts.name,
          port: lokiPorts.port,
          protocol: lokiPorts.protocol,
        },
      ],
      type: 'ClusterIP',
    },
  },
];

{
  config:: config,
  deployment:: deployment,
}
