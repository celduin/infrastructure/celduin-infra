/**
 * logProcessor.libsonnet
 *
 * Abstractions for Promtail, used to scrape logs from services
 *
 *
 * The contents of this file are covered by APACHE License Version 2.
 * See licenses/APACHEV2-LICENSE.txt for more information.
 */

local kube = import '../../kube.libsonnet';

/**
 * Labels for a given Kubernetes Resource
 *
 * @param name Name of the service
 *
 * @return a list of Kubernetes labels for a resource
 */
local labels(name) = {
  'app.name': name,
  'app.component': 'daemonSet',
  'app.part_of': 'logging-aggreg',
};

// Configuration of promtail
local promtailConfig = {
  server: {
    //Promtail Web server configuration.
    http_listen_port: 9080,
    grpc_listen_port: 0,
    log_level: 'debug',
  },
  clients: [
    // Configure connection to Loki
    {
      url: 'http://loki:3100/loki/api/v1/push',
    },
  ],
  positions: {
    // Describes how to save read file offsets to disk
    // Define where Promtail will save
    filename: '/tmp/positions.yaml',
  },
  scrape_configs: [
    // Configures how Promtail can scrape logs from a series of targets using a
    // specified discovery method.
    {
      job_name: 'system',
      // allows specifying a list of targets and a common label set for them.
      static_configs: [
        {
          targets: [
            'localhost',
          ],
          labels: {
            job: 'varlogs',
            __path__: '/var/log/*log',
          },
        },
      ],
    },
    {
      job_name: 'kubernetes-pods',
      // allow retrieving scrape targets from Kubernetes' REST API
      kubernetes_sd_configs: [
        {
          // The pod role discovers all pods and exposes their containers as targets.
          role: 'pod',
        },
      ],
      // relabel_configs allows discovered labels to be mutated into the desired form.
      // Relabling stepos are applied in order of appearance.
      relabel_configs: [
        // Include all labels on the pod
        {
          action: 'labelmap',
          regex: '__meta_kubernetes_pod_label_(.+)',
        },
        // Rename jobs to be <namespace>/<service>.
        {
          source_labels: ['__meta_kubernetes_namespace', '__service__'],
          action: 'replace',
          separator: '/',
          target_label: 'job',
          replacement: '$1',
        },
        // Create label for namespace
        {
          source_labels: ['__meta_kubernetes_namespace'],
          action: 'replace',
          target_label: 'namespace',
        },
        // Create label for pod name
        {
          source_labels: ['__meta_kubernetes_pod_name'],
          action: 'replace',
          target_label: 'pod_name',
        },
        // Create label for  container name
        {
          source_labels: ['__meta_kubernetes_pod_container_name'],
          action: 'replace',
          target_label: 'container_name',
        },
        // Create path to log files.
        {
          source_labels: ['__meta_kubernetes_pod_uid', '__meta_kubernetes_pod_container_name'],
          target_label: '__path__',
          separator: '/',
          replacement: '/var/log/pods/*$1/*.log',
        },
      ],
    },
    {
      job_name: 'syslog',
      syslog: {
        listen_address: '0.0.0.0:80',
        labels: {
          job: 'syslog',
        },
      },
      relabel_configs: [
        {
          source_labels: ['__syslog_message_hostname'],
          target_label: 'host',
        },
      ],
    },
  ],
  target_config: {
    // Period to resync directories being watched and files being tailed to discover
    // new ones or stop watching removed ones.
    sync_period: '10s',
  },
};

/**
 * Creates promtail deployment
 *
 * @param name The desired name for the deployment
 * @param namespace The namespace in which promtail should be deployed
 * @containerImage Desired promtail container to be deployed.
 *
 * @return a fully configured promtail deployment
 */
local promtail(name, namespace, containerImage) = [
  kube.daemonSet(name, namespace, labels(name))
  {
    spec: {
      selector: {
        matchLabels: labels(name),
      },
      template: {
        metadata: {
          labels: labels(name),
        },
        spec: {
          containers: [
            {
              name: name,
              image: containerImage,
              args: ['-config.file=/etc/promtail/promtail.yaml'],
              volumeMounts: [
                {
                  name: 'promtail-config',
                  mountPath: '/etc/promtail',
                },
                {
                  name: 'var-log-pods',
                  mountPath: '/var/log/pods',
                  readOnly: true,
                },
                {
                  name: 'var-lib-docker-containers',
                  mountPath: '/var/lib/docker/containers',
                  readOnly: true,
                },
              ],
            },
          ],
          volumes: [
            {
              name: 'promtail-config',
              configMap: {
                name: name,
              },
            },
            {
              name: 'var-log-pods',
              hostPath: {
                path: '/var/log/pods',
              },
            },
            {
              // This is needed because logs in /var/log/pods are symlinks to
              // /var/lib/docker/containers on the EKS
              name: 'var-lib-docker-containers',
              hostPath: {
                path: '/var/lib/docker/containers',
              },
            },
          ],
          serviceAccountName: name,
        },
      },
    },
  },
  kube.serviceAccount(name, namespace, labels(name)),
  kube.clusterRole(name, labels(name)) {
    rules: [
      {
        apiGroups: [
          '',
        ],
        resources: [
          'namespaces',
          'nodes',
          'nodes/proxy',
          'services',
          'endpoints',
          'pods',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
      {
        apiGroups: [
          'extensions',
        ],
        resources: [
          'ingresses',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
    ],
  },
  kube.clusterRoleBinding(name, labels(name)) {
    roleRef: {
      apiGroup: 'rbac.authorization.k8s.io',
      kind: 'ClusterRole',
      name: name,
    },
    subjects: [
      {
        kind: 'ServiceAccount',
        name: name,
        namespace: namespace,
      },
    ],
  },
  kube.configMap(name, namespace, labels(name)) {
    data: {
      'promtail.yaml': std.manifestYamlDoc(promtailConfig),
    },
  },
];

{
  promtail:: promtail,
}
