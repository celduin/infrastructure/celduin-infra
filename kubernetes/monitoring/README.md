# Monitoring Kubernetes Library

This folder contains abstractions and templates for the components of our
monitoring stack. Currently this stack contains:

- [Grafana] - Metrics visualisation
- [Loki] - Log aggregation
- [Jaeger] - gRPC tracing
- [Prometheus] - Metric scraping

Each component lives in its own folder. We also have `monitoring.libsonnet`,
which combines these components into a single deployable stack for ease of API.

## Architecture

We only currently provide ingress to Grafana and Prometheus. While we have
abstractions for jaeger, it currently doesn't get deployed due to intricacies in
the deployment process. The diagram below shows the ideal architecture, with
Jaeger enabled, just note that this is not currently the case in practice.

```mermaid
graph LR;
  subgraph Monitoring Stack
    %% Nodes
    grafana[Grafana]:::service;

    loki[Loki]:::service;
    promtail[Promtail]:::service;

    prometheus[Prometheus]:::service;
    node-exporter[Node Exporter]:::service;

    cassandra[Cassandra Database]:::service;
    jaeger-collector[Jaeger Collector]:::service;
    jaeger-query[Jaeger Query]:::service;

    %% Edges
    grafana --> loki --> promtail;
    grafana --> prometheus --> node-exporter;
    jaeger-query --> cassandra --> jaeger-collector;
  end

  %% Nodes

  ingress[Traefik]:::loadBalancer;

  ext-services[Other Services]:::external;
  %% Edges

  ingress --> grafana;
  ingress --> prometheus;
  ingress --> jaeger-query;

  promtail --> ext-services;
  node-exporter --> ext-services;
  prometheus --> ext-services;
  jaeger-collector --> ext-services;

  %% Styling
  classDef loadBalancer fill:#591,stroke:#333;
  classDef service fill:#f75,stroke:#333;
  classDef external fill:#57f,stroke:#333;
```

[//]: # (EXTERNAL LINKS BLOCK)

[Grafana]: https://grafana.com
[Loki]: https://github.com/grafana/loki
[Jaeger]: https://www.jaegertracing.io
[Prometheus]: https://prometheus.io
