/**
 * Grafana Libsonnet
 *
 * Template for grafana configuration and deployment
 */

local kube = import '../../kube.libsonnet';

/**
 * Defines a new grafana configuration
 *
 * @param name The name for the configmap that configures grafana
 * @param domain The domain pointing to grafana, e.g. nginx
 * @param anonymousAuth Whether to allow anonymous authentication. Defaults to false
 * @param organisation The organistaion used for anonymous authentication, if enabled. Defaults to 'Main Org.'
 *
 * @return an object representing grafana configuration
 */
local config(name, domain, anonymousAuth=false, organisation='Main Org.') = {
  name: name,
  data: {
    sections: {
      server: {
        domain: domain,
      },
      'auth.anonymous': {
        enabled: anonymousAuth,
        org_name: organisation,
        org_role: 'Viewer',
      },
    },
  },
};

/**
 * Defines a new grafana datasource
 *
 * @param name The name for the configmap that configures the grafana datasource
 * @param url The url of the datasource
 * @param access mode Must be 'server' or 'proxy'. Default to proxy
 * @param type Datasource type. defaults to 'prometheus'
 * @param editable Whether the datasource is editable in the UI. Default to false
 *
 * @return an object representing grafana configuration
 */
local datasource(name, url, access='proxy', type='prometheus', editable=false) = {
  name:: name,
  data:: {
    apiVersion: 1,
    datasources: [
      {
        access: access,
        editable: editable,
        name: name,
        orgId: 1,
        type: type,
        url: url,
        version: 1,
      },
    ],
  },
};

/**
 * Defines a full grafana deployment, including configuration and service, if
 * specified
 *
 * @param name The name of the deployment
 * @param image The docker image to use
 * @param config Configuration for the grafana instance
 * @param namespace The Kubernetes namespace for this deployment
 * @param endpointPort The port to expose grafana on
 * @param specificationMixin Mixin to be added to the Deployment
 * @param datasources List of Data Sources to add to grafana
 * @param dashboards List of Dashboards to add to grafana
 * @param dashboardPath Path to directory in which dashboards are stored
 * @param dashboardConfigName Name of configmap containing dashboards
 * @param replicas How many replicas of the pod to run. Defaults to 1.
 * @param service Whether to provision a Kubernetes service for grafana. Defaults to false.
 *
 * @return a configured grafana deployment, configmap and service (if specified)
 */
local deployment(name,
                 image,
                 config,
                 namespace,
                 endpointPort,
                 specificationMixin={},
                 datasources=[],
                 dashboards=[],
                 dashboardPath='/var/lib/grafana/dashboards',
                 dashboardConfigName='grafana-dashboard-config',
                 replicas=1,
                 service=false) = [
  kube.deployment(name, namespace) {
    spec: {
      replicas: replicas,
      selector: {
        matchLabels: {
          'app.name': name,
          'app.part-of': 'monitoring',
        },
      },
      template: {
        metadata: {
          labels: {
            'app.name': name,
            'app.part-of': 'monitoring',
          },
        },
        spec: {
          containers: [
            {
              image: image,
              name: name,
              ports: [
                {
                  containerPort: endpointPort,
                  protocol: 'TCP',
                },
              ],
              env: [
                {
                  name: 'GF_PATHS_CONFIG',
                  value: '/conf/custom.ini',
                },
              ],
              volumeMounts: [
                {
                  mountPath: '/conf',
                  name: 'config',
                },
                {
                  mountPath: '/etc/grafana/provisioning/dashboards',
                  name: dashboardConfigName,
                },
              ] + [
                {
                  name: ds.name,
                  mountPath: '/etc/grafana/provisioning/datasources/%s.yaml' % ds.name,
                  subPath: '%s.yaml' % ds.name,
                }
                for ds in datasources
              ] + [
                {
                  name: db.name,
                  mountPath: dashboardPath + '/%s.json' % db.name,
                  subPath: '%s.json' % db.name,
                }
                for db in dashboards
              ],
            },
          ],
          volumes: [
            {
              configMap: {
                name: name,
              },
              name: 'config',
            },
            {
              configMap: {
                name: dashboardConfigName,
              },
              name: dashboardConfigName,
            },
          ] + [
            {
              configMap: {
                name: ds.name,
              },
              name: ds.name,
            }
            for ds in datasources
          ] + [
            {
              configMap: {
                name: db.name,
              },
              name: db.name,
            }
            for db in dashboards
          ],
        } + specificationMixin,
      },
    },
  },

  kube.configMap(config.name, namespace) {
    data: {
      'custom.ini': std.manifestIni(config.data),
    },
  },

  kube.configMap(dashboardConfigName, namespace) {
    data: {
      'dashboard-config.yaml': std.toString({
        apiVersion: 1,
        providers: [
          {
            name: 'grafana-dashboards',
            folder: '',
            editable: true,
            orgId: 1,
            type: 'file',
            options: {
              path: dashboardPath,
            },
          },
        ],
      }),
    },
  },

  if service then
    kube.service(name, namespace) {
      spec: {
        type: 'ClusterIP',
        ports: [
          {
            name: 'endpoint',
            port: endpointPort,
            protocol: 'TCP',
          },
        ],
        selector: {
          'app.name': name,
          'app.part-of': 'monitoring',
        },
      },
    },
] + [
  kube.configMap(ds.name, namespace) {
    data: {
      [ds.name + '.yaml']: std.toString(ds.data),
    },
  }
  for ds in datasources
] + [
  kube.configMap(db.name, namespace) {
    data: {
      [db.name + '.json']: std.toString(db.data),
    },
  }
  for db in dashboards
];

{
  config:: config,
  datasource:: datasource,
  deployment:: deployment,
}
