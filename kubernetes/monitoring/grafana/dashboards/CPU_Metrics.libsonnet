/**
 * CPU_Metrics.libsonnet
 *
 * Dashboard for CPU usage metrics
 */

local dashboard(name, datasource) = {
  name:: name,
  data:: {
    annotations: {
      list: [
        {
          builtIn: 1,
          datasource: '-- Grafana --',
          enable: true,
          hide: true,
          iconColor: 'rgba(0, 211, 255, 1)',
          name: 'Annotations & Alerts',
          type: 'dashboard',
        },
      ],
    },
    editable: true,
    gnetId: null,
    graphTooltip: 0,
    id: 10,
    links: [],
    panels: [
      {
        aliasColors: {},
        bars: false,
        dashLength: 10,
        dashes: false,
        datasource: datasource,
        description: 'This was obtained by:\n % = (T  -  t(i) * T) / T) * 100=\n(T * (1 - t(i))) / T) * 100 = \n(1 -  t(i)) * 100 \n\nBeing:\nT:  Measured range of time (10M)\nt(i): Time spent in idle during one minute (node_cpu_seconds_total).\n%: Percentage',
        fill: 1,
        fillGradient: 0,
        gridPos: {
          h: 8,
          w: 12,
          x: 0,
          y: 0,
        },
        hiddenSeries: false,
        id: 4,
        legend: {
          alignAsTable: true,
          avg: true,
          current: true,
          max: true,
          min: true,
          show: true,
          total: false,
          values: true,
        },
        lines: true,
        linewidth: 1,
        nullPointMode: 'null',
        options: {
          dataLinks: [],
        },
        percentage: false,
        pointradius: 2,
        points: false,
        renderer: 'flot',
        seriesOverrides: [],
        spaceLength: 10,
        stack: false,
        steppedLine: false,
        targets: [
          {
            expr: '(1 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[1m])))) * 100',
            legendFormat: '{{instance}}',
            refId: 'A',
          },
        ],
        thresholds: [],
        timeFrom: null,
        timeRegions: [],
        timeShift: null,
        title: 'Total CPU Work Time',
        tooltip: {
          shared: true,
          sort: 0,
          value_type: 'individual',
        },
        type: 'graph',
        xaxis: {
          buckets: null,
          mode: 'time',
          name: null,
          show: true,
          values: [],
        },
        yaxes: [
          {
            decimals: null,
            format: 'percent',
            label: 'CPU Usage Percentage',
            logBase: 1,
            max: null,
            min: '0',
            show: true,
          },
          {
            format: 'short',
            label: null,
            logBase: 1,
            max: null,
            min: null,
            show: false,
          },
        ],
        yaxis: {
          align: false,
          alignLevel: null,
        },
      },
      {
        aliasColors: {},
        bars: false,
        dashLength: 10,
        dashes: false,
        datasource: datasource,
        fill: 1,
        fillGradient: 0,
        gridPos: {
          h: 8,
          w: 12,
          x: 12,
          y: 0,
        },
        hiddenSeries: false,
        id: 6,
        legend: {
          alignAsTable: true,
          avg: true,
          current: true,
          max: true,
          min: true,
          show: true,
          total: false,
          values: true,
        },
        lines: true,
        linewidth: 1,
        nullPointMode: 'null',
        options: {
          dataLinks: [],
        },
        percentage: false,
        pointradius: 2,
        points: false,
        renderer: 'flot',
        seriesOverrides: [],
        spaceLength: 10,
        stack: false,
        steppedLine: false,
        targets: [
          {
            expr: 'sum(irate(process_cpu_seconds_total[1m]))',
            legendFormat: '{{instance}}',
            refId: 'A',
          },
        ],
        thresholds: [],
        timeFrom: null,
        timeRegions: [],
        timeShift: null,
        title: 'Prometheus CPU Usage',
        tooltip: {
          shared: true,
          sort: 0,
          value_type: 'individual',
        },
        type: 'graph',
        xaxis: {
          buckets: null,
          mode: 'time',
          name: null,
          show: true,
          values: [],
        },
        yaxes: [
          {
            format: 's',
            label: 'Time Usage',
            logBase: 1,
            max: null,
            min: null,
            show: true,
          },
          {
            format: 'short',
            label: null,
            logBase: 1,
            max: null,
            min: null,
            show: false,
          },
        ],
        yaxis: {
          align: false,
          alignLevel: null,
        },
      },
      {
        aliasColors: {},
        bars: false,
        dashLength: 10,
        dashes: false,
        datasource: datasource,
        fill: 1,
        fillGradient: 0,
        gridPos: {
          h: 9,
          w: 12,
          x: 0,
          y: 8,
        },
        hiddenSeries: false,
        id: 2,
        legend: {
          alignAsTable: true,
          avg: true,
          current: true,
          max: true,
          min: true,
          rightSide: true,
          show: true,
          total: false,
          values: true,
        },
        lines: true,
        linewidth: 1,
        nullPointMode: 'null',
        options: {
          dataLinks: [],
        },
        percentage: false,
        pointradius: 2,
        points: false,
        renderer: 'flot',
        seriesOverrides: [],
        spaceLength: 10,
        stack: false,
        steppedLine: false,
        targets: [
          {
            expr: 'sum(irate(node_cpu_seconds_total[1m])) by (mode)',
            legendFormat: '{{mode}}',
            refId: 'A',
          },
        ],
        thresholds: [],
        timeFrom: null,
        timeRegions: [],
        timeShift: null,
        title: 'Time Spent per mode',
        tooltip: {
          shared: true,
          sort: 0,
          value_type: 'individual',
        },
        type: 'graph',
        xaxis: {
          buckets: null,
          mode: 'time',
          name: null,
          show: true,
          values: [],
        },
        yaxes: [
          {
            format: 's',
            label: 'Time Spent (Seconds)',
            logBase: 1,
            max: null,
            min: null,
            show: true,
          },
          {
            format: 'short',
            label: null,
            logBase: 1,
            max: null,
            min: null,
            show: false,
          },
        ],
        yaxis: {
          align: false,
          alignLevel: null,
        },
      },
    ],
    schemaVersion: 22,
    style: 'dark',
    tags: [],
    templating: {
      list: [],
    },
    time: {
      from: 'now-30m',
      to: 'now',
    },
    timepicker: {},
    timezone: '',
    title: 'CPU Metrics',
    uid: 's_E9acmGz',
    version: 2,
  },
};

{
  dashboard:: dashboard,
}
