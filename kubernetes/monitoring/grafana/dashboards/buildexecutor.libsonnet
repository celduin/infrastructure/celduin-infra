/**
 * buildexecutor.libsonnet
 *
 * Grafana dashboard for bb-worker/bb-runner metrics

 * The contents of this file were taken from bb-deploments project.
 * See https: *github.com/buildbarn/bb-deployments
 * This is file covered by APACHE License Version 2.
 * See licenses/APACHEV2-LICENSE.txt for more information.
 */

local dashboard(name, datasource) = {
  name:: name,
  data:: {
    __inputs: [],
    __requires: [],
    annotations: {
      list: [],
    },
    editable: false,
    gnetId: null,
    graphTooltip: 0,
    hideControls: false,
    id: null,
    links: [],
    panels: [
      {
        collapse: false,
        collapsed: false,
        gridPos: {
          h: 1,
          w: 24,
          x: 0,
          y: 0,
        },
        id: 2,
        panels: [],
        repeat: null,
        repeatIteration: null,
        repeatRowId: null,
        showTitle: true,
        title: 'Operations',
        titleSize: 'h6',
        type: 'row',
      },
      {
        aliasColors: {},
        bars: false,
        dashLength: 10,
        dashes: false,
        datasource: datasource,
        decimals: 0,
        fill: 7,
        gridPos: {
          h: 8,
          w: 24,
          x: 0,
          y: 1,
        },
        id: 3,
        legend: {
          alignAsTable: false,
          avg: false,
          current: false,
          max: false,
          min: false,
          rightSide: false,
          show: true,
          sideWidth: null,
          total: false,
          values: false,
        },
        lines: true,
        linewidth: 1,
        links: [],
        nullPointMode: 'null',
        percentage: false,
        pointradius: 5,
        points: false,
        renderer: 'flot',
        repeat: null,
        seriesOverrides: [],
        spaceLength: 10,
        stack: true,
        steppedLine: false,
        targets: [
          {
            expr: 'kubernetes_service:buildbarn_builder_build_executor_operations:irate1m{kubernetes_service=~"$kubernetes_service"}',
            format: 'time_series',
            intervalFactor: 2,
            legendFormat: '{{kubernetes_service}}',
            refId: 'A',
          },
        ],
        thresholds: [],
        timeFrom: null,
        timeShift: null,
        title: 'Operation rate',
        tooltip: {
          shared: true,
          sort: 2,
          value_type: 'individual',
        },
        type: 'graph',
        xaxis: {
          buckets: null,
          mode: 'time',
          name: null,
          show: true,
          values: [],
        },
        yaxes: [
          {
            decimals: 0,
            format: 'ops',
            label: null,
            logBase: null,
            max: null,
            min: 0,
            show: true,
          },
          {
            decimals: 0,
            format: 'ops',
            label: null,
            logBase: 1,
            max: null,
            min: 0,
            show: true,
          },
        ],
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 0,
          y: 2,
        },
        hideZeroBuckets: true,
        id: 4,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_stage:buildbarn_builder_build_executor_duration_seconds_bucket:irate1m{kubernetes_service=~"$kubernetes_service",stage="FetchingInputs"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'FetchingInputs stage duration',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 's',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 8,
          y: 2,
        },
        hideZeroBuckets: true,
        id: 5,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_stage:buildbarn_builder_build_executor_duration_seconds_bucket:irate1m{kubernetes_service=~"$kubernetes_service",stage="Running"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Running stage duration',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 's',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 16,
          y: 2,
        },
        hideZeroBuckets: true,
        id: 6,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_stage:buildbarn_builder_build_executor_duration_seconds_bucket:irate1m{kubernetes_service=~"$kubernetes_service",stage="UploadingOutputs"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'UploadingOutputs stage duration',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 's',
        },
      },
      {
        collapse: false,
        collapsed: false,
        gridPos: {
          h: 1,
          w: 24,
          x: 0,
          y: 3,
        },
        id: 7,
        panels: [],
        repeat: null,
        repeatIteration: null,
        repeatRowId: null,
        showTitle: true,
        title: 'POSIX resource usage',
        titleSize: 'h6',
        type: 'row',
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 12,
          x: 0,
          y: 4,
        },
        hideZeroBuckets: true,
        id: 8,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_user_time_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'CPU user time',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 's',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 12,
          x: 12,
          y: 4,
        },
        hideZeroBuckets: true,
        id: 9,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_system_time_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'CPU system time',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 's',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 0,
          y: 5,
        },
        hideZeroBuckets: true,
        id: 10,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_maximum_resident_set_size_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Maximum resident set size',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'bytes',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 6,
          y: 5,
        },
        hideZeroBuckets: true,
        id: 11,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_page_reclaims_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Page reclaims',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 12,
          y: 5,
        },
        hideZeroBuckets: true,
        id: 12,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_page_faults_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Page faults',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 18,
          y: 5,
        },
        hideZeroBuckets: true,
        id: 13,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_swaps_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Swaps',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 0,
          y: 6,
        },
        hideZeroBuckets: true,
        id: 14,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_block_input_operations_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Block input operations',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 6,
          y: 6,
        },
        hideZeroBuckets: true,
        id: 15,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_block_output_operations_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Block output operations',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 12,
          y: 6,
        },
        hideZeroBuckets: true,
        id: 16,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_messages_sent_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Messages sent',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 6,
          x: 18,
          y: 6,
        },
        hideZeroBuckets: true,
        id: 17,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_messages_received_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Messages received',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 0,
          y: 7,
        },
        hideZeroBuckets: true,
        id: 18,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_signals_received_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Signals received',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 8,
          y: 7,
        },
        hideZeroBuckets: true,
        id: 19,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_voluntary_context_switches_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Voluntary context switches',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 16,
          y: 7,
        },
        hideZeroBuckets: true,
        id: 20,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_posix_involuntary_context_switches_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Involuntary context switches',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        collapse: false,
        collapsed: false,
        gridPos: {
          h: 1,
          w: 24,
          x: 0,
          y: 8,
        },
        id: 21,
        panels: [],
        repeat: null,
        repeatIteration: null,
        repeatRowId: null,
        showTitle: true,
        title: 'File pool resource usage',
        titleSize: 'h6',
        type: 'row',
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 0,
          y: 9,
        },
        hideZeroBuckets: true,
        id: 22,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_file_pool_files_created_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Files created',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 8,
          y: 9,
        },
        hideZeroBuckets: true,
        id: 23,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_file_pool_files_count_peak_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Peak file count',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 16,
          y: 9,
        },
        hideZeroBuckets: true,
        id: 24,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le:buildbarn_builder_build_executor_file_pool_files_size_bytes_peak_bucket:irate1m{kubernetes_service=~"$kubernetes_service"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Peak file size',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'bytes',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 0,
          y: 10,
        },
        hideZeroBuckets: true,
        id: 25,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_operation:buildbarn_builder_build_executor_file_pool_operations_count_bucket:irate1m{kubernetes_service=~"$kubernetes_service",operation="Read"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Read operations',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 8,
          y: 10,
        },
        hideZeroBuckets: true,
        id: 26,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_operation:buildbarn_builder_build_executor_file_pool_operations_count_bucket:irate1m{kubernetes_service=~"$kubernetes_service",operation="Write"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Write operations',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 8,
          x: 16,
          y: 10,
        },
        hideZeroBuckets: true,
        id: 27,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_operation:buildbarn_builder_build_executor_file_pool_operations_count_bucket:irate1m{kubernetes_service=~"$kubernetes_service",operation="Truncate"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Truncate operations',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'short',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 12,
          x: 0,
          y: 11,
        },
        hideZeroBuckets: true,
        id: 28,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_operation:buildbarn_builder_build_executor_file_pool_operations_size_bytes_bucket:irate1m{kubernetes_service=~"$kubernetes_service",operation="Read"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Total read size',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'bytes',
        },
      },
      {
        color: {
          colorScheme: 'interpolateSpectral',
          mode: 'spectrum',
        },
        dataFormat: 'tsbuckets',
        datasource: datasource,
        gridPos: {
          h: 8,
          w: 12,
          x: 12,
          y: 11,
        },
        hideZeroBuckets: true,
        id: 29,
        legend: {
          show: true,
        },
        targets: [
          {
            expr: 'sum(kubernetes_service_le_operation:buildbarn_builder_build_executor_file_pool_operations_size_bytes_bucket:irate1m{kubernetes_service=~"$kubernetes_service",operation="Write"}) by (le)',
            format: 'heatmap',
            intervalFactor: 10,
            legendFormat: '{{le}}',
          },
        ],
        title: 'Total write size',
        type: 'heatmap',
        yAxis: {
          decimals: 0,
          format: 'bytes',
        },
      },
    ],
    refresh: '10s',
    rows: [],
    schemaVersion: 16,
    style: 'dark',
    tags: [],
    templating: {
      list: [
        {
          allValue: null,
          current: {},
          datasource: datasource,
          hide: 0,
          includeAll: true,
          label: 'Service name',
          multi: true,
          name: 'kubernetes_service',
          options: [],
          query: 'label_values(kubernetes_service:buildbarn_builder_build_executor_operations:irate1m, kubernetes_service)',
          refresh: 2,
          regex: '',
          sort: 1,
          tagValuesQuery: '',
          tags: [],
          tagsQuery: '',
          type: 'query',
          useTags: false,
        },
      ],
    },
    time: {
      from: 'now-3h',
      to: 'now',
    },
    timepicker: {
      refresh_intervals: [
        '5s',
        '10s',
        '30s',
        '1m',
        '5m',
        '15m',
        '30m',
        '1h',
        '2h',
        '1d',
      ],
      time_options: [
        '5m',
        '15m',
        '1h',
        '6h',
        '12h',
        '24h',
        '2d',
        '7d',
        '30d',
      ],
    },
    timezone: 'browser',
    title: 'BuildExecutor',
    uid: 'buildexecutor',
    version: 0,
  },
};

{
  dashboard:: dashboard,
}
