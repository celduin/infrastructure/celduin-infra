/**
 * Prometheus libsonnet
 *
 * Templates and abstractions for Prometheus, including the Node Exporter.
 */

local kube = import '../../kube.libsonnet';
local rules = import 'rules.libsonnet';

/**
 * Template for Prometheus configuration. This is configured to listen on pods
 * with the `prometheus_io_scrape` annotation.
 *
 * @param name The name for this config
 * @param prom_port The port to listen ot prometheus on. Defaults to 9090
 *
 * @return an object containing Prometheus configuration
 */
local config(name, prom_port=9090) = {
  name: name,
  data: {
    global: {
      scrape_interval: '15s',
    },
    rule_files: [
      '/var/rules/recording-rules.json',
    ],
    scrape_configs: [
      {
        job_name: 'prometheus',
        scrape_interval: '5s',
        static_configs: [
          {
            targets: ['localhost:%d' % prom_port],
          },
        ],
      },

      {
        job_name: 'kubernetes-pods',
        kubernetes_sd_configs: [
          {
            role: 'pod',
          },
        ],
        relabel_configs: [
          {
            // Scrape only pods that have \"prometheus_io_scrape: 'true'\" annotation.
            source_labels: [
              '__meta_kubernetes_pod_annotation_prometheus_io_scrape',
            ],
            action: 'keep',
            regex: 'true',
          },

          {
            // Customize the port from where Prometheus should access a container.
            // By default this will the Prometheus default port, this can be changed
            // by setting \"prometheus_io_port={YOUR_PORT}\"
            source_labels: [
              '__meta_kubernetes_pod_annotation_prometheus_io_port',
            ],
            action: 'replace',
            target_label: '__metrics_port__',
            regex: '(.+)',
          },

          {
            // Customize the path from where Prometheus should gather metrics in a
            // container. By default this will the Prometheus default path, this can
            // be changed by setting \"prometheus_io_path={YOUR_PATH}\"
            source_labels: [
              '__meta_kubernetes_pod_annotation_prometheus_io_path',
            ],
            action: 'replace',
            target_label: '__metrics_path__',
            regex: '(.+)',
          },
          {
            source_labels: [
              '__meta_kubernetes_pod_container_name',
            ],
            action: 'replace',
            target_label: 'container',
            regex: '(.+)',
          }
          {
            source_labels: [
              '__meta_kubernetes_pod_name',
            ],
            action: 'replace',
            target_label: 'pod',
            regex: '(.+)',
          },
        ],
      },
      {
        job_name: 'kubernetes-service-endpoints',
        kubernetes_sd_configs: [
          {
            role: 'endpoints',
          },
        ],
        relabel_configs: [
          {
            action: 'labelmap',
            regex: '__meta_kubernetes_service_label_(.+)',
          },
          {
            source_labels: [
              '__meta_kubernetes_namespace',
            ],
            action: 'replace',
            target_label: 'kubernetes_namespace',
          },
          {
            source_labels: [
              '__meta_kubernetes_service_name',
            ],
            action: 'replace',
            target_label: 'kubernetes_service',
          },
        ],
      },
    ],
  },
};

/**
 * Generates a list of default arguments for prometheus
 *
 * @param externalDomain The domain of the server to redirect to
 * @param logLevel The level of logging verbosity. Defaults to 'debug'
 * @param extras Optional list of additional arguments
 *
 * @return a list of arguments to be used in the deployment
 */
local promArgs(externalDomain, logLevel='debug', extras=[]) = [
  '--config.file=/var/prometheus/prometheus.yml',
  '--storage.tsdb.path=/prometheus',
  '--web.console.libraries=/usr/share/prometheus/console_libraries',
  '--web.console.templates=/usr/share/prometheus/consoles',
  '--log.level=%s' % logLevel,
  '--web.external-url=http://%s/prometheus/' % externalDomain,
] + extras;

/**
 * Creates a fully configured Node Exporter deployment
 *
 * @param name The name of the components
 * @param image The docker image to use
 * @param namespace The namespace to use
 * @param endpointPort The port to expose node-exporter on
 * @param service Whether to create a service as well. Defaults to false.
 *
 * @return a fully configured prometheus node exporter deployment
 */
local nodeExporter(name,
                   image,
                   namespace,
                   endpointPort=9100,
                   service=false,
                   specificationMixin={},) = [
  kube.daemonSet(name, namespace) {
    spec: {
      selector: {
        matchLabels: {
          app: name,
        },
      },
      template: {
        metadata: {
          labels: {
            app: name,
          },
          annotations: {
            prometheus_io_scrape: 'true',
            prometheus_io_port: '9100',
          },
        },
        spec: {
          serviceAccountName: name,
          automountServiceAccountToken: true,
          containers: [
            {
              name: name,
              image: image,
              ports: [
                {
                  containerPort: endpointPort,
                  protocol: 'TCP',
                },
              ],
              volumeMounts: [
                {
                  name: 'proc',
                  mountPath: '/host/prometheus',
                  readOnly: true,
                },
                {
                  name: 'sys',
                  mountPath: '/host/sys',
                  readOnly: true,
                },
              ],
            },
          ],
          volumes: [
            {
              name: 'proc',
              hostPath: {
                path: '/proc',
              },
            },
            {
              name: 'sys',
              hostPath: {
                path: '/sys',
              },
            },
          ],
        } + specificationMixin,
      },
    },
  },

  kube.serviceAccount(name, namespace) {
    automountServiceAccountToken: true,
  },

  kube.clusterRole(name) {
    rules: [
      {
        apiGroups: [
          '',
        ],
        resources: [
          'nodes',
          'nodes/proxy',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
    ],
  },

  kube.clusterRoleBinding(name) {
    roleRef: {
      apiGroup: 'rbac.authorization.k8s.io',
      kind: 'ClusterRole',
      name: 'node-exporter-rbac',
    },
    subjects: [
      {
        kind: 'ServiceAccount',
        name: 'node-exporter-serv-acc',
        namespace: '%s' % namespace,
      },
    ],
  },

  if service then
    kube.service(name, namespace) {
      spec: {
        ports: [
          {
            name: name,
            port: endpointPort,
            protocol: 'TCP',
          },
        ],
        selector: {
          app: name,
        },
      },
    },
];

/**
 * Generates a fully configured prometheus deployment
 *
 * @param name The name of the components
 * @param image The docker image to use
 * @param config A config object for prometheus
 * @param namespace The namespace to use
 * @param endpointPort The port to expose prometheus on
 * @param replicas The number of replicas to create. Defaults to 1.
 * @param service Whether to create a service as well. Defaults to false.
 *
 * @return a fully configured prometheus deployment
 */
local deployment(name,
                 image,
                 config,
                 namespace,
                 endpointPort,
                 specificationMixin={},
                 replicas=1,
                 service=false,
                 prometheusArgs=[]) = [
  kube.deployment(name, namespace) {
    spec: {
      replicas: replicas,
      selector: {
        matchLabels: {
          app: name,
        },
      },
      template: {
        metadata: {
          labels: {
            app: name,
          },
        },
        spec: {
          serviceAccountName: name,
          automountServiceAccountToken: true,
          containers: [
            {
              name: name,
              image: image,
              command: [
                'prometheus',
              ],
              args: prometheusArgs,
              ports: [
                {
                  containerPort: endpointPort,
                  protocol: 'TCP',
                },
              ],
              volumeMounts: [
                {
                  name: 'config',
                  mountPath: '/var/prometheus',
                },
                {
                  name: 'rules',
                  mountPath: '/var/rules',
                },
              ],
            },
          ],
          volumes: [
            {
              name: 'config',
              configMap: {
                name: config.name,
              },
            },
            {
              name: 'rules',
              configMap: {
                name: 'rules',
              },
            },
          ],
        } + specificationMixin,
      },
    },
  },

  kube.configMap(config.name, namespace) {
    data: {
      'prometheus.yml': std.manifestYamlDoc(config.data),
    },
  },

  kube.serviceAccount(name, namespace) {
    automountServiceAccountToken: true,
  },

  kube.configMap('rules', namespace) {
    data: {
      'recording-rules.json': std.manifestJsonEx(rules.bbRules, '  '),
    },
  },

  kube.clusterRole(name) {
    rules: [
      {
        apiGroups: [
          '',
        ],
        resources: [
          'nodes',
          'nodes/proxy',
          'services',
          'endpoints',
          'pods',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
      {
        apiGroups: [
          'extensions',
        ],
        resources: [
          'ingresses',
        ],
        verbs: [
          'get',
          'list',
          'watch',
        ],
      },
      {
        nonResourceURLs: [
          '/metrics',
        ],
        verbs: [
          'get',
        ],
      },
    ],
  },

  kube.clusterRoleBinding(name) {
    roleRef: {
      apiGroup: 'rbac.authorization.k8s.io',
      kind: 'ClusterRole',
      name: name,
    },
    subjects: [
      {
        kind: 'ServiceAccount',
        name: name,
        namespace: namespace,
      },
    ],
  },

  if service then
    kube.service(name, namespace) {
      spec: {
        ports: [
          {
            name: 'prometheus-access',
            port: 9090,
            protocol: 'TCP',
          },
        ],
        selector: {
          app: name,
        },
        type: 'ClusterIP',
      },
    },
];


{
  config:: config,
  promArgs:: promArgs,
  nodeExporter:: nodeExporter,
  deployment:: deployment,
}
