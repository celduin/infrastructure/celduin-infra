/**
 * Monitoring Libsonnet
 *
 * Wrapper functions around the monitoring components, just to simplify the API
 * more than anything.
 */

local routing = import '../ingress/routing.libsonnet';
local grafana = import 'grafana/grafana.libsonnet';
local jaeger = import 'jaeger/jaeger.libsonnet';
local logProcessor = import 'log-aggregation/logProcessor.libsonnet';
local loki = import 'log-aggregation/loki.libsonnet';
local prometheus = import 'prometheus/prometheus.libsonnet';

/**
 * Create a grafana deployment
 *
 * @param namespace The namespace for the deployment
 * @param frontendDomain The domain for the ingress, used for redirects
 * @param image The docker image to use
 * @param specificationMixin Mixin ot add to the grafana Deployment
 *
 * @return a fully configured grafana deployment
 */
local _grafanaDeployment(namespace, frontendDomain, image, specificationMixin) =
  local config = grafana.config('grafana', frontendDomain, anonymousAuth=true);
  local promDatasource = grafana.datasource('prometheus-datasource', 'http://prometheus.rex:9090/prometheus');
  local lokiDatasource = grafana.datasource('loki-datasource', 'http://loki:3100', 'proxy', 'loki');

  local bbDashboard = import 'grafana/dashboards/Buildbarn-dashboard.libsonnet';
  local bbThroughputDashboard = import 'grafana/dashboards/Buildbarn-Throughput-dashboard.libsonnet';
  local k8sDashboard = import 'grafana/dashboards/Kubernetes-dashboard.libsonnet';
  local CPUDashboard = import 'grafana/dashboards/CPU_Metrics.libsonnet';
  local DiskDashboard = import 'grafana/dashboards/Disk_Metrics.libsonnet';
  local MemoryDashboard = import 'grafana/dashboards/Memory_Metrics.libsonnet';
  local WorkerDashboard = import 'grafana/dashboards/buildexecutor.libsonnet';
  local SchedulerDashboard = import 'grafana/dashboards/scheduler.libsonnet';
  local TraefikDashboard = import 'grafana/dashboards/traefik.libsonnet';

  local dashboards = [
    db.dashboard(db.name, 'prometheus-datasource')
    for db in [
      { name: 'buildbarn-dashboard', dashboard: bbDashboard.dashboard },
      { name: 'buildbarn-throughput-dashboard', dashboard: bbThroughputDashboard.dashboard },
      { name: 'kubernetes-dashboard', dashboard: k8sDashboard.dashboard },
      { name: 'cpu-dashboard', dashboard: CPUDashboard.dashboard },
      { name: 'disk-dashboard', dashboard: DiskDashboard.dashboard },
      { name: 'memory-dashboard', dashboard: MemoryDashboard.dashboard },
      { name: 'worker-dashboard', dashboard: WorkerDashboard.dashboard },
      { name: 'scheduler-dashboard', dashboard: SchedulerDashboard.dashboard },
      { name: 'traefik-dashboard', dashboard: TraefikDashboard.dashboard },
    ]
  ];

  grafana.deployment('grafana',
                     image,
                     config,
                     namespace,
                     3000,
                     datasources=[promDatasource, lokiDatasource],
                     dashboards=dashboards,
                     service=true,
                     specificationMixin=specificationMixin,);

/**
 * Create a jaeger deployment
 *
 * @param namespace The namespace for the deployment
 *
 * @return a fully configured jaeger deployment
 */
local _jaegerDeployment(namespace) =
  jaeger.jaegerCollector('jaeger-collector', namespace) +
  jaeger.jaegerQuery('jaeger-query', namespace) +
  jaeger.jaegerConfigMap('jaeger-config', namespace) +
  jaeger.jaegerCassandra('jaeger-cassandra', namespace);

/**
 * Create a prometheus deployment, complete with node-exporter
 *
 * @param namespace The namespace for the deployment
 * @param frontendDomain The domain for the ingress, used for redirects
 * @param prometheusImage The prometheus docker image to use
 * @param nodeExporterImage The node-exporter docker image to use
 * @param specificationMixin Mixin to be added to the Prometheus Deployment
 *
 * @return a fully configured Prometheus deployment
 */
local _prometheusDeployment(namespace,
                            frontendDomain,
                            prometheusImage,
                            nodeExporterImage,
                            specificationMixin,) =
  local config = prometheus.config('prometheus');
  local args = prometheus.promArgs(frontendDomain);

  prometheus.deployment('prometheus',
                        prometheusImage,
                        config,
                        namespace,
                        9090,
                        prometheusArgs=args,
                        service=true,
                        specificationMixin=specificationMixin,) +
  prometheus.nodeExporter('node-exporter',
                          nodeExporterImage,
                          namespace,
                          service=true,
                          specificationMixin=specificationMixin,);


/**
 * Create a full logging stack deployment
 *
 * @param namespace The namespace for the deployment
 * @param lokiImage version of the loki container Image
 * @param promtailImage version of the promtail container Image
 * @param lokiReplicas number of replicas of Loki
 *
 * @return a fully configured logging stack
 */
local _loggingStack(namespace,
                    lokiImage,
                    promtailImage,
                    lokiBucket,
                    lokiReplicas=1) =
  local config = loki.config('loki', lokiBucket);
  loki.deployment('loki',
                  namespace,
                  lokiImage,
                  config,
                  replicas=lokiReplicas) +
  logProcessor.promtail('promtail',
                        namespace,
                        promtailImage);

/**
 * Create a full monitoring stack deployment
 *
 * @param namespace The namespace for the deployment
 * @param frontendDomain The domain for the ingress, used for redirects
 * @param grafanaImage The grafana docker image to use
 * @param prometheusImage The prometheus docker image to use
 * @param nodeExporterImage The node-exporter docker image to use
 * @param lokiImage Desired loki container Image
 * @param promtailImage Desired promtail container Image
 * @param serverSecretName Name of a Secret containing a TLS key pair for server authentication
 * @param lokiBucket Name of S3 bucket to back loki. Blank for local storage
 *
 * @return a fully configured monitoring stack
 */
local deployment(
  namespace,
  frontendDomain,
  statsDomain,
  grafanaImage,
  prometheusImage,
  nodeExporterImage,
  grafanaSpecificationMixin={},
  prometheusSpecificationMixin={},
  lokiImage,
  promtailImage,
  serverSecretName,
  lokiBucket
      ) =

  _grafanaDeployment(namespace,
                     frontendDomain,
                     grafanaImage,
                     grafanaSpecificationMixin,) +
  _prometheusDeployment(namespace,
                        frontendDomain,
                        prometheusImage,
                        nodeExporterImage,
                        prometheusSpecificationMixin,) +
  _loggingStack(namespace,
                lokiImage,
                promtailImage,
                lokiBucket,) +
  [
    routing.ingress(
      name='monitoring',
      namespace=namespace,
      entryPoints=[routing.entrypoints.secure.name],
      routes=[
        routing.route(match='Host(`%s`) && PathPrefix(`/prometheus`)' % statsDomain, services=[routing.service('prometheus', namespace, port=9090)]),
        routing.route(match='Host(`%s`)' % statsDomain, services=[routing.service('grafana', namespace, port=3000)]),
      ],
      tls=routing.tls(serverSecretName, routing.tlsOption('monitoring', namespace)),
    ),
  ];

{
  deployment:: deployment,
}
