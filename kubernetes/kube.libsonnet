/**
 * Kubernetes Libsonnet
 *
 * Abstractions for Kubernetes objects.
 *
 * Chiefly exists to simplify APIs and remove boilerplate.
 */

//###############################################################################
//                             Private Methods                                  #
//###############################################################################

/**
 * Template for a generic Kubernetes object
 *
 * @param apiVersion The API version for the component
 * @param kind The kind of kubernetes object this is
 * @param name The name of the object
 * @param namespace The namespace for the object
 * @param labels The labels for the object
 * @param annotations Annotations to add to the object
 *
 * @return a boilerplate kubernetes object.
 */
local _kubernetesObject(apiVersion, kind, name, namespace='', labels={}, annotations={}) = {
  apiVersion: apiVersion,
  kind: kind,
  metadata: {
    name: name,
    [if namespace != '' then 'namespace']: namespace,
    [if labels != {} then 'labels']: labels,
    [if annotations != {} then 'annotations']: annotations,
  },
};

//###############################################################################
//                         Kubernetes Meta Objects                              #
//###############################################################################

/**
 * Template for a Kubernetes namespace
 *
 * @param namespace The name of the namespace to create
 *
 * @return a boilerplate Namespace definition
 */
local namespace(namespace) = {
  apiVersion: 'v1',
  kind: 'Namespace',
  metadata: {
    name: namespace,
  },
};

/**
 * Template for a Kubernetes Secret
 *
 * @param name The name of the secret
 * @param namespace The Kubernetes Namespace for the secret
 *
 * @return a boilerplate Kubernetes Secret
 */
local secret(name, namespace) =
  _kubernetesObject(
    apiVersion='v1',
    kind='Secret',
    name=name,
    namespace=namespace,
  );

/**
 * Template for a CustomResourceDefinition
 *
 * @param name The name of the CRD
 *
 * @return a boilerplate CRD definition
 */
local customResourceDefinition(name) = {
  apiVersion: 'apiextensions.k8s.io/v1beta1',
  kind: 'CustomResourceDefinition',
  metadata: {
    name: name,
  },
};

//###############################################################################
//                           Volume Abstractions                                #
//###############################################################################

/**
 * Template for a Kubernetes ConfigMap configuration
 *
 * @param name The name of this configmap
 * @param namespace The namespace for the configmap
 * @param labels The labels for the configmap
 * @param annotations Optionally add annotations to the ConfigMap
 *
 * @return a boilerplate ConfigMap definition
 */
local configMap(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='ConfigMap',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for a generic Persistent Volume
 *
 * @param name The name of the volume
 * @param namespace The namespace for the volume
 * @param labels Optionally add labels to the object
 * @param annotations Optionally add annotations to the object
 *
 * @return a boilerplate persistent volume config
 */
local persistentVolume(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='PersistentVolume',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for a Persistent Volume Claim
 *
 * @param name The name for this claim
 * @param namespace The namespace for this claim
 * @param labels Optionally add labels to the object
 *
 * @return a persistent volume claim configuration
 */
local persistentVolumeClaim(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='PersistentVolumeClaim',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for a volume mount object
 *
 * @param name The name of the volume
 * @param mountPath The path to mount the volume on
 *
 * @return a volume mount config
 */
local volumeMount(name, mountPath) = {
  name: name,
  mountPath: mountPath,
};

/**
 * Template for a volumeDevice object
 *
 * @param name The name of the volume
 * @param devicePath The path for the device in the container
 *
 * @return a volume device config
 */
local volumeDevice(name, devicePath) = {
  name: name,
  devicePath: devicePath,
};

//###############################################################################
//                               Deployments                                    #
//###############################################################################

/**
 * Template for an container configuration
 *
 * @param name The name of container
 * @param image Desired image to be deployed
 *
 * @return a boilerplate container definition
 */
local container(name, image) = {
  name: name,
  image: image,
};

/**
 * Template for a Kubernetes Deployment configuration
 *
 * @param name The name of this deployment
 * @param namespace The namespace for the deployment
 * @param labels The labels for the deployment
 * @param annotations Optionally add annotations to the Deployment
 *
 * @return a boilerplate Deployment definition
 */
local deployment(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='apps/v1',
    kind='Deployment',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for a Kubernetes DaemonSet configuration
 *
 * @param name The name of this DaemonSet
 * @param namespace The namespace for the DaemonSet
 * @param labels Labels to be applied to the DaemonSet
 * @param annotations Optionally add annotations to the DaemonSet
 *
 * @return a boilerplate DaemonSet definition
 */
local daemonSet(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='apps/v1',
    kind='DaemonSet',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for an StatefulSet configuration
 *
 * @param name The name of statefulSet
 * @param namespace The namespace for the statefulSet
 * @param labels Optionally add labels to the StatefulSet
 * @param annotations Optionally add annotations to the StatefulSet
 *
 * @return a boilerplate statefulSet definition
 */
local statefulSet(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='apps/v1',
    kind='StatefulSet',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for a job configuration
 *
 * @param name The name of job
 * @param namespace The namespace for the job
 * @param labels Optionally add labels to the Job
 * @param annotations Optionally add annotations to the Job
 *
 * @return a boilerplate job definition
 */
local job(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='batch/v1',
    kind='Job',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

//###############################################################################
//                          Networking Abstractions                             #
//###############################################################################

/**
 * Template for a Kubernetes Service configuration
 *
 * @param name The name of this service
 * @param namespace The namespace for the service
 * @param labels The labels for the service
 * @param annotations Optionally add annotations to the Service
 *
 * @return a boilerplate Service definition
 */
local service(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='Service',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for a Kubernetes Ingress configuration
 *
 * @param name The name of this Ingress
 * @param namespace The Kubernetes Namespace for this Ingress
 * @param labels Optionally add labels to the Ingress
 * @param annotations Optionally add annotations to the Ingress
 *
 * @return a boilerplate Ingress definition
 */
local ingress(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='networking.k8s.io/v1beta1',
    kind='Ingress',
    name=name,
    namespace=name,
    labels=labels,
    annotations=annotations,
  );

//###############################################################################
//                           Cluster Authorisation                              #
//###############################################################################

/**
 * Template for a Kubernetes ServiceAccount configuration
 *
 * @param name The name of this service account
 * @param namespace The namespace for the service account
 * @param labels Optionally add labels to the ServiceAccount
 * @param annotations Optionally add annotations to the ServiceAccount
 *
 * @return a boilerplate ServiceAccount definition
 */
local serviceAccount(name, namespace, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='v1',
    kind='ServiceAccount',
    name=name,
    namespace=namespace,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for an RBAC ClusterRole configuration
 *
 * @param name The name of this Cluster Role
 * @param labels Optionally add labels to the ClusterRole
 * @param annotations Optionally add annotations to the ClusterRole
 *
 * @return a boilerplate ClusterRole definition
 */
local clusterRole(name, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='rbac.authorization.k8s.io/v1beta1',
    kind='ClusterRole',
    name=name,
    labels=labels,
    annotations=annotations,
  );

/**
 * Template for an RBAC ClusterRoleBinding configuration
 *
 * @param name The name of this Cluster Role Binding
 * @param labels Optionally add labels to the ClusterRoleBinding
 * @param annotations Optionally add annotations to the ClusterRoleBinding
 *
 * @return a boilerplate ClusterRoleBinding definition
 */
local clusterRoleBinding(name, labels={}, annotations={}) =
  _kubernetesObject(
    apiVersion='rbac.authorization.k8s.io/v1beta1',
    kind='ClusterRoleBinding',
    name=name,
    labels=labels,
    annotations=annotations,
  );

//###############################################################################
//                           Resource Allocation                                #
//###############################################################################

/**
 * Template for a nodeSelector object to use with a nodeAffinity object
 *
 * @param key Key value of the label to be matched on the Node
 * @param operator Type of Affinity, see Kubernetes documentation for more details
 * https: *kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity
 * @param values List of accpeted values for the Node's label
 *
 * @return a configured NodeSelector object
 */
local nodeSelector(key, operator, values) = {
  key: key,
  operator: operator,
  values: values,
};

/**
 * Template for a nodeAffinity object to use as part of Affinity
 *
 * @param required Boolean value if  set to `true` it must be an exact match
 *        otherwise Kubernetes will try it best
 * @param selectors Array of nodeSelector objects
 *
 * @return a configured NodeAffinity
 */
local nodeAffinity(required, selectors) = {
  nodeAffinity: {
    [if required then 'requiredDuringSchedulingIgnoredDuringExecution'
    else 'preferredDuringSchedulingIgnoredDuringExecution']: {
      nodeSelectorTerms: [
        {
          matchExpressions: selectors,
        },
      ],
    },
  },
};

/**
 * Template for Affinity object
 *
 * @param nodeAffinities nodeAffinity object
 * @param podAffinities podAffinity object
 *
 * @return a configured Affinity object for use with NodeSelector
 */
local affinity(nodeAffinity={}, podAffinity={}) = {
  affinity: nodeAffinity + podAffinity,
};

{
  clusterRole:: clusterRole,
  clusterRoleBinding:: clusterRoleBinding,
  customResourceDefinition:: customResourceDefinition,
  configMap:: configMap,
  deployment:: deployment,
  daemonSet:: daemonSet,
  namespace:: namespace,
  secret:: secret,
  service:: service,
  serviceAccount:: serviceAccount,
  container:: container,
  statefulSet:: statefulSet,
  job:: job,
  persistentVolume:: persistentVolume,
  persistentVolumeClaim:: persistentVolumeClaim,
  volumeMount:: volumeMount,
  nodeSelector:: nodeSelector,
  nodeAffinity:: nodeAffinity,
  affinity:: affinity,
  volumeDevice:: volumeDevice,
}
