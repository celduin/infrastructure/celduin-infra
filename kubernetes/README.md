# Kubernetes Deployment

This folder contains a Kubernetes deployment of a remote cache and execution
service compatible with [REAPI] clients, in particular [Bazel], [BuildStream]
and [RECC].

This is essentially a bb-storage and a bst-artifact-server hiding behind
traefik, which does some gRPC routing. To use this simply point your client
(e.g. BuildStream) at the traefik endpoint (you shouldn't need to specify a port)
and run a build.

The remote cache is known to be compatible with:

- BuildStream 1.x
- BuildStream 2.x development snapshots (at least up to 1.93.4)
- Bazel >= 3.1.0

The remote execution service is known to be compatible with:

- BuildStream 2.x development snapshots (at least up to 1.93.4)
- Bazel >= 3.1.0

## Components

A full deployment of should include:

- [Argo CD]
- [cert-manager]
- [Traefik] ingress
- A remote execution stack
- A monitoring stack

More detail on the remote execution stack can be found [here](./rex/README.md),
and more detail on the monitoring stack can be found
[here](./monitoring/README.md).

## Libsonnet Library Structure

This folder allows you to deploy remote cache server to a kubernetes cluster,
all the possible cluster environments have been stored under `clusters/` folder
and the details of the deployment can be seen in the `deployment.jsonnet` file
of each setup (`clusters/${ENVIRONMENT}/deployment.jsonnet`).

In order to create the cluster deployments, we have created a set of libraries
where all the needed components for the cluster are defined. This libraries are
stored within the directories here, organised by their purpose.

- `argo-cd` - Templates used for integration with [Argo CD]
- `clusters` - Top-level definitions for entire deployments
- `ingress` - Abstractions and templates for ingress resources, including
  [cert-manager] and [traefik] ingress controller
- `monitoring` - Templates and abstractions used for the deployment of a
  monitoring stack
- `rex` - Templates and abstractions used for the deployment of the remote
  cache/execution services

[//]: # (EXTERNAL LINKS BLOCK)

[REAPI]: https://github.com/bazelbuild/remote-apis
[BuildStream]: https://buildstream.build
[Bazel]: https://bazel.build
[RECC]: https://gitlab.com/bloomberg/RECC
[Argo CD]: https://argoproj.github.io/argo-cd/
[cert-manager]: https://cert-manager.io
[Traefik]: https://docs.traefik.io
