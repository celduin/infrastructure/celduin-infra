# Testing the BuildStream cache

This is a minimal test project to test the staging cluster

## Testing with BuildStream

To spin up a container using the current directory as working directory use this command

```bash
docker run --rm -ti -v "$(pwd)":/test --privileged --net host buildstream/buildstream:dev
```

You will need to decrypt the certificates to the staging cluster. To do so:
1. Install ansible

```bash
pip3 install --user ansible
export PATH="$HOME/.local/bin:$PATH"
ansible-vault --version
```
2. Use ansible-vault to decrypt certificates:

```bash
find staging-certs/ -type f | xargs ansible-vault decrypt
```

Please contact @coldtom or @christopherphang for the password to decrypt.

Select the desired environment's Buildstream configuration:

```bash
ln -sf "project.conf.${ENVIRONMENT}" project.conf
```

Finally, simply invoking

```bash
bst build foo.bst
```
should build and push a test element. To push explicitly, use

```bash
bst artifact push foo.bst
```
after building.

To make sure you can pull from the cache, simply

```bash
bst artifact delete foo.bst
bst artifact pull foo.bst
```

Obviously, you will need to build and push to the cache first.
