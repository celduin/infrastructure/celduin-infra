# Testing Bazel

## Locally

### Prerequisites

+ Docker
+ Local Kubernetes Cluster - see [celduin-infra README](../../README.md#deploying-locally)

Make your local cluster accessible form your local system

```bash
kubectl -n rex port-forward svc/traefik-ingress 8080:443
```

### Building abseil-hello

You can use Bazel's client in a Docker container to build abseil-hello with

```bash
docker run -it --net host -v $(pwd)/hosts:/etc/hosts \
-v $(pwd)/WORKSPACE:/WORKSPACE \
-v $(pwd)/dev-certs:/dev-certs \
-v $(pwd)/bazelrc:/bazelrc \
l.gcr.io/google/rbe-ubuntu16-04@sha256:6ad1d0883742bfd30eba81e292c135b95067a6706f3587498374a083b7073cb9 \
sh -c \
  "wget -O bazel-installer.sh https://github.com/bazelbuild/bazel/releases/download/3.1.0/bazel-3.1.0-installer-linux-x86_64.sh && \
  chmod +x bazel-installer.sh && \
  ./bazel-installer.sh && \
  /usr/local/bin/bazel --bazelrc=/bazelrc build \
    --remote_executor=push.cache.com:8080 \
    --tls_certificate=dev-certs/server.crt \
    --tls_client_certificate=dev-certs/client-push.crt \
    --tls_client_key=dev-certs/client-push.key \
    --verbose_failures @abseil-hello//:hello_main"
```
