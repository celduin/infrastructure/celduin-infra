# Celduin Infra Documentation

This folder contains more in depth documentation for celduin-infra. Some simple documentation may be found in the [README](../README.md), and documentation on development with this repository can be found in our [CONTRIBUTING](../CONTRIBUTING.md) guide.

## Contents

### Using

- [Bazel Clients](./using-bazel-clients.md)
- [BuildStream Clients](./using-buildstream-clients.md)

### Operating

- [Deploying from Scratch](./deploy-from-scratch.md)
- [Release Process](./release-process.md)
- [Deployment Mechanism](./deployment-process.md)

### Design

- [Cloud Architecture](./cloud-architecture-design.md)
- [Deployment Architecture](./deployment-architecture-design.md)
- [IAM Policy](./IAM-rules.md)
- [Deployment Environments](./deployment-environments.md)

### Component Documentation

- [Grafana](./Grafana.md)
- [Loki](./Loki.md)
- [Jaeger](./grpc-tracing.md)
- [Prometheus](./prometheus.md)
- [Ingress](./routing-strategy.md)

### Contributing

- [Workflow](./workflow.md)

## Spikes

We have a few spikes in the `spikes` directory. These represent spikes of
research effort into tooling and should give some insight into the decisions
made.
