# A How To Guide for deploying celduin-infra from Scratch

This document aims to be a step-by-step guide to deploying a full Remote
Execution cluster from a clean AWS account.

**NOTE**: These commands are ugly at the moment as the script we use for
deploying terraform doesn't accept the `--tla-*` arguments used to customise the
infra to other users.

## Prerequisites

- An AWS account
- The AWS CLI
  - This can be installed from PyPI - `pip3 install --user awscli`
- Terraform
  - Your distribution _may_ supply this, if not then you may simply download
    the binary from [hashicorp](https://www.terraform.io/downloads.html) and
    add it to your `$PATH`
- aws-iam-authenticator
  - This can be installed by following the
    [instructions](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)
- Jsonnet
  - This may be available from your distribution, but if not you can install
    the binary release from the [jsonnet github](https://github.com/google/jsonnet/releases)
- Kubectl
  - This may be available from your distribution, but if not you can install by
    following the [instructions](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

## AWS Set up

Firstly you'll need to configure your AWS CLI to use your account, which can be
done through `aws configure` and following the instructions there. This will
allow terraform to talk to AWS.

Now you have the administration part out of the way, you can begin deploying the
infrastructure. Like all things, you first need a foundation - in this case a
DNS zone so you can access your services and an S3 bucket in order to store the
state of the deployment. As this bucket also stores the state of itself and the
DNS zone, this requires some bootstrapping.

Deploying the global terraform is an out of band operation, and requires some
fiddling. First you need to deploy the bucket (and DNS zone) using

```bash
mkdir -p tf
jsonnet terraform/global.tf.jsonnet \
  --tla-code bootstrap=true \
  --tla-str dnsZone=[your.domain.here] \
  --tla-str backendBucketName=[your_bucket_name_here] \
  --tla-str awsRegion=[desired-region-here] \
  > tf/main.tf.json

cd tf
terraform init
terraform apply
```

Once this has run, you need to push this state up to the bucket, which can be
achieved by

```bash
jsonnet terraform/global.tf.jsonnet \
  --tla-str dnsZone=[your.domain.here] \
  --tla-str backendBucketName=[your_bucket_name_here] \
  --tla-str awsRegion=[desired-region-here] \
  > tf/main.tf.json

cd tf
terraform init
terraform apply
```

This should push up your terraform.tfstate to the bucket. You're now ready to
deploy a remote execution cluster!

## Deploying Remote Execution Infrastructure

Inside the `terraform/` folder, you will find several `*.tf.jsonnet` files.
These are the top-level definitions for our deployments. On an infrastructure
level, these are essentially identical, only the `prod` environment does some
funkiness with the DNS subdomain used (switching from `prod` to `cache`). Choose
which of these you wish to replicate (I'll use staging here) and run

```bash
mkdir -p tf
jsonnet terraform/staging.tf.jsonnet \
  --tla-str dnsZone=[your.domain.here] \
  --tla-str backendBucketName=[your_bucket_name_here] \
  --tla-str awsRegion=[desired-region-here] \
  > tf/main.tf.json

cd tf
terraform init
terraform apply
terraform output -json > ../kubernetes/clusters/staging/_tfOutput.jsonnet
```

This will provision all the infrastructure required to run one of our clusters.

## Deploying Remote Execution Services

Deploying services also requires a little bootstrapping, as we use [Argo CD] to
manage deployments. To bootstrap up argo CD simply

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v1.4.2/manifests/install.yaml
```

We also use [cert-manager] to manage our server TLS certificates. This also
requires manual bootstrapping. To deploy cert-manager, simply run:

```bash
kubectl apply -f kubernetes/ingress/cert-manager-legacy.yaml
```

When deploying cert-manager, any `Issuers` must be in a "ready" state before
deploying any `Certificate` resources. First deploy any Issuers:

```bash
kubectl apply -f tmp/*-Ingress.json
```

Wait for these to show up as `READY` in the output from `kubectl get issuer -n rex`

Decrypt the SSL certificates, (if you're just wanting a play around, you can
copy the `dev` certificates from the corresponding place in
`kubernetes/clusters/dev`) using `ansible-vault`:

```bash
ansible-vault decrypt kubernetes/clusters/staging/config/certs.jsonnet
```

Deploying the client SSL certificates is currently an out-of-band operation, and
is done separately to the rest of the cluster. Deploy the certificates by:

```bash
mkdir -p crt
jsonnet -m crt kubernetes/clusters/staging/config/certs.jsonnet
kubectl create namespace rex
kubectl apply -f crt/
```

You can now deploy the services using

```bash
mkdir -p tmp
jsonnet -m tmp kubernetes/clusters/staging/deployment.jsonnet
kubectl apply -f tmp/rex-Namespace.json -f tmp/
```

This will give you a functional Remote Execution Cluster!

[//]: # (EXTERNAL LINKS)
[Argo CD]: https://argoproj.github.io/argo-cd/
[cert-manager]: https://cert-manager.io/
