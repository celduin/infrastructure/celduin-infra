# Prometheus

Prometheus is an open-source systems monitoring and alerting toolkit. We have templates
and abstractions for prometheus in `kubernetes/monitoring/prometheus/prometheus.libsonnet`.
To see how these are used to deploy prometheus, see `kubernetes/monitoring/monitoring.libsonnet`,
which defines our entire monitoring stack.

To make sure this is working execute:

`kubectl port-forward -n rex service/prometheus 9090:9090`

The open your browser and go to `localhost:9090`. In there introduce
`promhttp_metric_handler_requests_total`and press execute. You should see some
information labelled like `prometheus` and `kubernetes-pods`.

Our Prometheus deployment is composed by three elements:

* **The prometheus deployment.**
  The prometheus deployment is exposing an endpoint at http://localhost:9090/prometheus.
  The http prefix is necessary to talk to prometheus, even inside the cluster.
  This is necessary to make prometheus rewrite URLs correctly.
* **The prometheus rbac:** The rbac or role based access credentials defines a
  ClusterRole (which specifies a set of permissions over a cluster), a
  serviceAccount (which creates an identity for a process that runs in a pod)
  and a ClusterRoleBinding that ties the ServiceAccount to the ClusterRole. The
  ServiceAccount needs to be referenced in the Prometheus deployment, it will
  create a secret token to identify the identity which needs to be mounted to
  the Prometheus pod. This is automatically handled by k8s if the ServiceAccount
  definition and in the reference to the serviceAccount in the Prometheus
  deployment is specified `automountServiceAccountToken: true`.
* **The Prometheus configuration file:**  The Prometheus configuration file
  defines the targets to gather the metrics from and how it will be done. There
  are two mayor blocks on it, one to gather metrics from Prometheus itself and
  one more to gather metrics from the pods in the cluster. For the latest,
  Prometheus have been configured to search for annotations in the k8s deployments
  specifying if metrics need to be scraped (`prometheus_io_scrape: 'true'`),
  which container port should the metrics be scraped from (`prometheus_io_port: '7981'`)
  and from which path should the metrics be scraped (`prometheus_io_path:`).

  Here a example on how to include this in your kubernetes pods.

```yaml
# Deployment/pod definition
  metadata:
   labels:
     <labels>
   annotations:
     prometheus_io_scrape: 'true'
     prometheus_io_port: '7981'
spec:
   <spec-definitions>
   ports:
     - name: prometheus
       port: 7981
       protocol: TCP
# Service definition
spec:
  ports:
  - name: prometheus
    port: 7981
    protocol: TCP
```

## Accessing Prometheus

We currently have a prometheus frontend for [prod] and [staging].

[//]: # (EXTERNAL LINKS BLOCK)
[prod]: https://stats.public.aws.celduin.co.uk/prometheus
[staging]: https://stats.staging.aws.celduin.co.uk/prometheus
