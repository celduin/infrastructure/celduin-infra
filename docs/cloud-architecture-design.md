# Celduin Cloud Infrastructure Design

This document provides a clear definition of the project requirements affecting the Cloud Infrastructure design. This is completed by a description of the current design in place.  
This document aims to be an accurate representation of the state of this project's Cloud Infrastructure therefore it will be evolving.

The intention of this document is:  

- Help newcomers to understand the current configuration and the reasoning behind it
- Have a space to track the evolution of the Cloud Infrastructure and the requirements influencing it

## Index

1. [Project Requirements](#project-requirements)  
2. [Current Cloud Architecture](#current-cloud-architecture)  

## Project Requirements

The main objective of this project is to have a scalable/robust/replicable/customisable environment to run different clients ([Buildstream], [Bazel], [RECC]) with a Remote Execution server ([Buildbarn]). Finally the solution needs to be able to exist on-Prem.  

As this project intends to provide an example use case for using Remote Execution at scale in different scenarios each solution will be self-contained and self-sufficient; meaning that the infrastructure will be repeated for different Cloud Providers.  
Currently the Cloud Providers considered are:  

- [AWS] - Phase1  
- [Digital Ocean] - Experimental support
- [GCP] - Future (possibly Phase2)
- [Azure] - TBD

## Current Cloud Architecture

### Current State

- Cloud Providers
  - [AWS]
- [Kubernetes]
  - Managed by EKS
- Automated Cloud Provisioning
  - [Terraform]
- Monitoring
  - System Monitoring
    - [Prometheus]
  - Log Aggregation
    - [Promtail]
    - [Loki]
  - Data Visualisation
    - [Grafana]
  - gRPC Tracing
    - [Jaeger]
- BuildStream Artifact Store
  - On Disk storage (mounted [EBS] volume)
- BuildBarn CAS and Action Cache
  - On Disk storage ([EBS] volume)
- BuildBarn Remote Execution Stack
- Certificate Management
  - [cert-manager]
- Deployment tooling
  - [Argo CD]
- Templating
  - [Jsonnet]

### Initial Plan

Objective:  

- Core elements deployed and running.  
- Focus on AWS no customisation possible.  
- Dashboard for [Buildbarn] and system metrics.  

The following elements are considered as core:  

- Cloud Provider
  - [AWS]
- [Kubernetes]
- Automated Cloud provisioning
  - [Terraform]
- Monitoring  
  - System monitoring -> [Prometheus]  
  - Logging aggregator  
    - [Fluent-bit]
    - [Elasticsearch] as database  
  - Data visualisation -> [Grafana]  
- Artifacts storage  
  - Object Store - [S3] Bukects  
  - Disk Storage
  - In-memory Storage - [ElasticCache]

### Future Plans

As a later phase investigation work will be done around:  

- Customisation
  - [Kustomize] - Already used in RE API Testing
  - [Jsonnet]
  - [Helm]
- Monitoring  
  - [Loki] - Possible replacement for [Fluent-bit]
  - Alerting - with integration to communication tool (IRC, Slack, email)
- gRPC Tracing  
  - [Jaeger]
- Continuous Integration
  - [Zuul] -> Depends on CRASH work  
- Ostree server support

### Other Tools

- Monitoring tools  
  - [logz.io]  
    This is a managed service for logging aggregation based on the [ELK stack] and [Grafana]. It is very close to our intended solution and therefore we will not be using, but could be a potential solution for users who do not wish to manage their logging aggregation system

  - [Splunk]  
    This is another popular managed service for log aggregation. We have favored [Fluent-bit] and [Grafana] for the time being, but [Splunk] remians a valid option for log aggregation

[//]: # (EXTERNAL LINKS BLOCK)

[//]: # (REMOTE EXECUTION)
[Buildstream]: https://www.buildstream.build/
[Buildbarn]: https://github.com/buildbarn
[Bazel]: https://bazel.build/
[RECC]: https://gitlab.com/bloomberg/recc

[//]: # (KUBERNETES TOOLING)
[Kubernetes]: https://kubernetes.io/
[Kustomize]: https://kustomize.io/
[Jsonnet]: https://jsonnet.org/
[Helm]: https://helm.sh/
[Prometheus]: https://prometheus.io/docs/introduction/overview/
[Fluent-bit]: https://fluentbit.io/
[Loki]: https://github.com/grafana/loki
[Promtail]: https://github.com/grafana/loki/tree/v1.5.0/docs/clients/promtail
[Grafana]: https://grafana.com/
[Jaeger]: https://www.jaegertracing.io/
[Elasticsearch]: https://www.elastic.co/elasticsearch
[ELK stack]: https://www.elastic.co/what-is/elk-stack
[logz.io]: https://logz.io/
[Splunk]: https://www.splunk.com/

[//]: # (INFRASTRUCTURE AUTOMATISATION)
[Terraform]: https://www.terraform.io/
[GitLab]: https://docs.gitlab.com/ee/README.html
[GitLab Container Registry]: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
[Zuul]: https://zuul-ci.org/docs/zuul/discussion/concepts.html
[Argo CD]: https://argoproj.github.io/argo-cd/

[//]: # (CLOUD PROVIDERS)

  [//]: # (AWS LINKS)
  [AWS]: https://aws.amazon.com/
  [S3]: https://aws.amazon.com/s3/
  [EBS]: https://aws.amazon.com/ebs/
  [ElasticCache]: https://aws.amazon.com/elasticache/

[GCP]: https://cloud.google.com/
[Azure]: https://azure.microsoft.com/
[Digital Ocean]: https://www.digitalocean.com/
