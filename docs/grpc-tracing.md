# gRPC Traces

gRPC traces allow us to gather insights in the network traffic between the different application in the cluster. This information can help identify errors in the network or bottle-necks for example.

This project uses [Jaeger] to collect and access traces.


## Jaeger 

### Recommended Setup


```mermaid
  graph TB;
    
    subgraph "Jaeger"
    u1[UI] --> j1
    j1["jaeger-query"] --> c1
    j2["jaeger-collector"] --> c1
    c1["cassandra"]
    end

    subgraph "Application"
    j3[jaeger-agent] --> j2
    j3-- sidecar ---a1
    a1[application]
    end
```

A `jaeger-agent` is added to the pod of each applications producing traces.  
Each `jaeger-agent` then send the traces to the `jaeger-collector` which will queue the them and apply the sampling policies before storing it in a [Cassandra] database.  
Finally the traces can be accessed via a web interface created by `jaeger-query`.


### Collecting gRPC Traces 

#### Jaeger Infrastructure

To use this configuration in a cluster you first need to create all the different elemnts mentioned above.  
First you neded to create the `jaeger-configuration` as it will be required by the other elements:

```
kubectl apply -f kubernetes/monitoring/jaeger/jaeger-configuration.yml
```

Next is creating the [Cassandra] database and wait for the end of the bootstraping job:

```
kubectl apply -f kubernetes/monitoring/jaeger/jaeger-cassandra.yml
kubectl get pods -w
```

> You can tell when the bootstraping job is done when you see something similar too:  
> `jaeger-cassandra-schema-job-5l9kv   0/1     Completed   0          12m`

Once the job is and you have you [Cassandra] database running you can create the last element `jaeger-collector`:

```
kubectl apply -f kubernetes/monitoring/jaeger/jaeger-collector.yml
```


#### Jaeger Sidecar

Now you have all the infrastructure required to collect gRPC traces in your cluster. The last thing is to make sure that the application expected to produce traces have a sidecar configured.

To get a sidecar to running in a pod add the following element to the containers list of your pod definition:


```yaml
- image: jaegertracing/jaeger-agent:1.17
  name: jaeger-agent
  ports:
    - containerPort: 5775
      protocol: UDP
    - containerPort: 6831
      protocol: UDP
    - containerPort: 6832
      protocol: UDP
    - containerPort: 5778
      protocol: TCP
  args: ["--reporter.grpc.host-port=jaeger-collector:14267"]
```

Once the sidecar is running make sure that you application sends gRPC Traces to `localhost:<port>` where `port` can be any port exposed in the jaeger-agent image.

#### Viewing Traces

In order to view the gRPC traces collected you need to add `jaeger-query` to your cluster using the following commands:

```
kubectl apply -f kubernetes/monitoring/jaeger/jaeger-query.yml
```

Accessing the pod will depend on where the cluster is running

1. Locally  
   Run this command to make it accessible at `localhost:16686`:  
   ```
   kubectl port-forward jaeger-query 16686
   ```
2. Remotely  
   The `jaeger-query` Service links to the port `31668`; to get the ip address of the cluster use:  
   ```
   kubectl get services
   ```
   Finally you can reach at `<EXTERNAL-IP>:31668`.

#### Production Traces

We have jaeger tracing deployed on our [production cache][prod-jaeger], as well as our [staging cluster][staging-jaeger].

**Note** You cannot access the jaeger frontend on a *dev* cluster through traefik. This is due to jaeger trying to load its static resources via a http request to the hostname of the original request. Inside the cluster this won't work by default. We should fix this in the general case using rewrites.


[//]: # (EXTERNAL LINKS)

[Jaeger]: https://www.jaegertracing.io
[Cassandra]: https://cassandra.apache.org/
[prod-jaeger]: https://stats.cache.aws.celduin.co.uk/
