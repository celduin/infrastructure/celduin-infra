# Celduin Infrastructure Release Process

Currently we exist in a state of limbo, between fully automated and kind of
automated. This makes our release process somewhat complex. This document aims
to describe what to do.

## Criteria for release

End to end testing is currently done on our staging cluster. Staging must display the following characteristics.

- An end to end build of freedesktop-sdk and/or libreML must have occurred, with no failures in push and pull steps.
  - This should occur from a cold cache.
- An end to end build of abseil must have occurred, with no failures in push and pull steps.
  - This should occur from a cold cache.
- All web endpoints are accessible
- There are no observable errors on monitoring stacks. Places to look at include (but are not restricted to):
  - gRPC return codes
  - Any observable I/O, CPU, memory pressure
  - Suspicious log messages.

Unfortunately, much of this is manual right now. Issues such as #123 and #124 aim to make much of this process automatable.

Note that much of this could also be done in development environments. Please aim to have this as part of your checklist,
before merging to master.

## Process

### 1. Announce downtime

Announce the server downtime. This should be done in the `#celduin` channel on
freenode. Ideally announce the expected downtime if you can figure it out.

### 2. Assess whether manual deployment is necessary

Due to certain limitations with the deployment process (for now), it may be
necessary to deploy the release manually. A non-exhaustive list of reasons for
such an occurrence are:

- The instance type of an AWS nodegroup has changed
  - See #112
- Major changes to terraform
  - Currently the pipeline is ordered such that it gathers terraform output
  before actually running the terraform. This results in the nasty side effect
  that changes in terraform that change output need to be run by hand. At the
  time of writing this will only be domain changes thankfully.
- Attempting to leave it to the CI yields a failure for some other reason.

If you do not need to run a manual deployment, then you should proceed to step
5.

### 3. Run the terraform (If Required)

Terraform may need to be run manually, for possible reasons see above.
To run the terraform use:

```bash
./scripts/terraform-jsonnet -e public -c apply
```

Some caveats to be aware of:

- S3 buckets must be manually emptied (via the CLI or web UI) before deletion
  - Note that deploying a cluster creates no S3 buckets currently. The
  `celduin-tf-state` bucket is deployed as a global operation and is out of
  band.
- Terraform doesn't seem to be resizing instances in place, see #112
  - As a result, you'll need to destroy the cluster first, with

   ```bash
   ./scripts/terraform-jsonnet -e public -c destroy
   ```

### 4. Bootstrap the cluster (If Required)

Once your terraform has applied, you will need to relaunch the services on the
cluster too - in particular Argo CD is required for a pipeline to work, and (at
the time of writing) TLS certificates will need to be added too.

To bootstrap up a cluster, use the following commands:

```bash
# Provision Argo CD
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v1.4.2/manifests/install.yaml

# Provision cert-manager
kubectl apply -f kubernetes/ingress/cert-manager-legacy.yaml

# Decrypt and provision the certificates
ansible-vault decrypt kubernetes/clusters/public/config/certs.jsonnet
mkdir -p target
jsonnet -m target kubernetes/clusters/public/config/certs.jsonnet
kubectl create namespace rex

# Create Issuers
kubectl apply -f target/*-Issuer.json

# Wait until Issuers are ready...

# When they're ready, create the rest of the deployment
kubectl apply -f target
```

Now that Argo CD and the certificate secrets are deployed, Argo CD should be
able to do the rest of the release process if a pipeline is run, which will be
triggered by the next step.

### 5. Tag the repository

Tag the repository with a new release tag. We use Semantic Versioning, so it's a
good idea to be familiar with the [semver specification]. What exactly is or is
not a breaking change is up for debate (#129), so use best judgement. As a rule
of thumb, if no artifacts are lost it's a bug fix, if artifacts are lost but no
API (that is, user-facing endpoints) changes then it's a minor release, if the
API changes then it's a major release.

Note however that we are currently in v0.x.y releases, so we use x as the major
point.

For tagging, you can either use the git cli or use the gitlab web UI. Of the
two, the web UI is probably simpler. When using the gitlab web UI, create a
lightweight tag and add release notes for metadata.

### 6. Restart any deployments that need restarting

Currently updating a configmap will not automatically redeploy a dependent
deployment. As a result, we need to restart any deployments that have had
configmaps change under them. The best way to do this is to run

```bash
kubectl rollout restart -n rex deployment/foo
```

This may cause Argo CD to consider these deployments out of sync. Ideally we
should fix #59.

#### Artifact Storage

An additional concern is the artifact cache. Thanks to #37, it is a major issue
if the artifact cache points to blobs that do not exist in CAS. As a result, any
change to the storage backend which causes the loss of blobs will require a
clean out of the artifact cache.

Here kubernetes' StatefulSet abstraction becomes our enemy, as we can't simply
restart the deployment. Rather, we must enter the container and manually clear
out the artifacts. To do this use

```bash
kubectl exec -ti -n rex statefulset/storage-artifact -- bash
```

to shell into the artifact server, then

```bash
rm -rf /artifacts/cas/refs/heads/* /artifacts/artifacts/refs/*
```

The reason this has two locations is because BuildStream changed the server's
layout between 1.4 and master, `cas/refs/heads` is used for 1.4.x,
`artifacts/refs` is used for master.

### 7. Ensure that our test projects work with the newly deployed server

This should be handled by the CI now, thanks to stellar work from @jonathanmaw.
Simply wait for the CI to finish (with all green ticks) and relax. If you
really want to test it yourself, you can follow the instructions in
`test/buildstream` to test for BuildStream compatiblity.

### 8. Announce that the servers are back

Shout from the mountain tops that you have deployed to production, and it is (as
far as you know) working correctly. Again, #celduin on freenode is the go-to
mountain top for this.

### 9. Have a brew

Have a brew, you deserve it. In this time, you may also hope for there to be no
regressions or complications arising from the deployment.

[//]: # (EXTERNAL LINKS BLOCK)
[semver specification]: https://semver.org/
