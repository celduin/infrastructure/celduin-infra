# Project Workflow

This document records the current workflow we follow as part of the work for Celduin Infra. This is by no mean **sacred scriptures**, but a collection of guidelines owned by everyone.  
These guidelines aims to help us structure our work and improve the communication and cohesion of the team.

This workflow intention is to **help us** and **should not block us**. Maintaining Live services running remains a **top priority**.


# High Level

The main source of inspiration for this workflow is [Kanban]:

+ Each piece of work is represented by an Issue

+ Everyone can create a new Issue

+ An Issue must be be Triaged before it is done

+ The current work organised in a board composed of 2 columns:
  - *To Do*: List all the Issues ready to be picked up
  - *Doing*: List all the Issues being worked on

+ Milestones represent a cut-off point where a big piece of work is completed


# Workflow

## Isues

### Creating Issue

Everyone can create a new Issue.

An Issue should represent a single piece of work (Adding a new element, fixing some bug, updating something already existing, investigation, etc.), all the information required to do the work should be encapsulated in the Issue (List of tasks to perform, diagram of the expected result, expected behaviour versus current behaviour, etc.).

Ideally an Issue should be a small enough so it can be done by one person over a short period of time. However if a piece of work spans accross several smaller tasks it is possible to have one Issue for the overall work related to several smaller Issues.


### Issue Triage

After creating an Issue the Issue should be reviewed by at least one person, this cannot be done by the Issue creator.  
Triaging an Issue is to ensure that it is ready to be worked on and that we have enough understanding of it to complete it.

This is the *recommended* process to follow when doing Triage:

1. Issue Fitness

   + Do I understand the objective of this Issue

   + Is there already an Issue covering the same work

   + Do I have all the information I need or know where to get it to do the work (even if I don't fully understand it yet)

   + Is there a clear way to identify completion

2. Issue Identification

   + Make sure it has some Labels to help identifing the work to be done

   + Assign it to a Milestone

3. Feedback
   Raise any points or comments you gathered while performing the first two steps

4. Good to go
   Add to the `To Do` column of the Board


## Board

The [Board] should be composed of all the Issues ready to be worked on, Issues should be added to the [Board] only after Triage.  
It is composed of 3 columns:

1. To Do

2. Doing

3. Closed

### To Do

This column lists all the Issues ready to be worked on but not assigned yet. This should be the first place to look if you don't know what to do next.  


### Doing

Here you can see what Issues are currently being worked on, ideally there should only be one Issue per person in this column.

### Closed

This lists all the Issues once completed.


## Milestones

Each Milestone should represent the completion of an entire chunk of work, for example getting the CRASH team to use Buildbarn Remote Cache could be a milestone.

Milestones will help tracking progress of the project on longer pieces of work which will require some substantial amount of work. These should should not cover technical solutions and remain fairly high-level, providing planning for longer term goals.



[//]: # (EXTERNAL LINKS)

[Kanban]: https://en.wikipedia.org/wiki/Kanban_(development)
[Board]: https://gitlab.com/celduin/infrastructure/celduin-infra/-/boards/1584262
