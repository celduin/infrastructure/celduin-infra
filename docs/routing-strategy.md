# Routing Strategy

This document intends to be a quick overview of how we route requests to the
relevant services inside the cluster. This is necessary already with just the
remote caching services implemented, so once remote-execution gets involved
we'll definitely need it. This aims to clarify our traefik configuration, and
explain any other routing in the deployment.

## Ingress

All ingress to the cluster first goes through an AWS NLB pointing at the Traefik
service in the cluster. Most routing and load balancing is handled by traefik.

## Top level hostnames

We have 3 outward facing subdomains, one for the pull endpoint of the cache, one
for the push endpoint of the cache and one for the monitoring stack.

### The "Pull" Cache

This block has the most complicated routing configuration, as we want to
expressly disallow certain gRPC calls - namely pushing for the Artifact service,
the CAS BatchUploadBlobs and the ByteStream Write. This is configured by
routing the forbidden calls to the Forbidden bb-storage, which will just return
a gRPC `PERMISSION_DENIED` error. The remaining calls are sent by their prefixes
to the correct location, which is safe as only `buildstream` calls should be
sent to the artifact cache.

In the CRASH cache we have client SSL authentication enabled here, so only
authorised people can pull from the cache. As this is more complex, we also do
this in staging. The public deployment is intended to have public pulling, so we
don't verify clients.

### The "Push" Cache

This block is essentially the same, but without the complicated per-call
routing. We want every call to make it through to the cache. This has client
authentication for every deployment, as obviously we don't want anybody to be
able to push anything to any of our caches.

### The stats endpoint

This block is just a reverse proxy to each of the monitoring components. There
is no client TLS authentication.

## Other traefik-level quirks

The sharp-eyed reader may have noticed there are a few other routing configurations.

* HTTP -> HTTPS redirection
* Prometheus metric endpoint

## Beyond Traefik

The BuildBarn deployment also does some routing. At the moment, this is minimal
and only there as a placeholder until we get an execution service going. The
intention will be for all calls to go through the `grpc-demultiplex` bb-storage,
before going on to where they need to go. In practice, they just get thrown at
one of the bb-storage's pointing at the CAS for the moment.
