# Grafana

This document covers the basics of [Grafana] configuration and setup. It should help you in understanding the current configuration and help you get started using it in the Celduin infrastructure through some examples.  
If you are looking for information more centered to Grafana itself you can refer to [Grafana official documentation][grafana-documentation].

## Deploying Grafana

[Grafana] is included as part of the monitoring stack. To add a standalone instance look to [monitoring.libsonnet](../kubernetes/monitoring/monitoring.libsonnet) for an example of using the API.

## Accessing Grafana Web UI

### Locally

Once [Grafana] is running in the cluster you can make it available at `localhost:3000` with the following command:

```bash
kubectl port-forward -n rex svc/grafana 3000
```

Alternatively, you can access through the ingress by using the following commands:

```bash
# Nasty way to ensure stats.cache.com resolves
echo 127.0.0.1 stats.cache.com >> /etc/hosts

# Port forward to the ingress
kubectl port-forward -n rex svc/traefik-ingress 8080:443
```

Grafana may be accessed at https://stats.cache.com:8080.

### AWS

Our [grafana instance][prod-grafana] is available at https://stats.public.aws.celduin.co.uk.
We also have grafana for [staging].

## Adding Dashboards and Datasources

Currently we add dashboards and datasources to grafana through our libsonnet structure - we have abstractions to produce data sources, and dashboards are specified in `kubernetes/monitoring/grafana/dashboards`. To see how these are used, see [`kubernetes/monitoring/monitoring.libsonnet`](../kubernetes/monitoring/monitoring.libsonnet).

The dashboards are stored with a small wrapper around the raw JSON from grafana - this allows us to parametise the datasource used. When adding a new dashboard make sure that it follows the API used, that is:

```jsonnet
local dashboard(name, datasource) = {
  name:: name,
  data:: {
    // JSON from grafana here, with datasources parametised to `datasource`
  }
}

{
  dashboard:: dashboard,
}
```

This may be subject to change as currently this only allows dashboards with a single datasource.

When hacking on dashboards, it is recommended to use Grafana's UI rather than fiddling with the jsonnet files. To update the dashboard, simply copy the JSON into the `data` field of the dashboard and parametise the datasources.

In future, we are looking to template dashboards, perhaps using [grafonnet].

[//]: # (EXTERNAL LINKS)

[Grafana]: https://grafana.com/
[grafana-documentation]: https://grafana.com/docs/grafana/latest/
[prod-grafana]: https://stats.public.aws.celduin.co.uk
[staging]: https://stats.staging.aws.celduin.co.uk
[grafonnet]
