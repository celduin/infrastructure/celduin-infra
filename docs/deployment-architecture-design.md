# Remote Execution Deployment Design Document

This document aims to outline the design of a Remote Execution Server
deployment, which will be defined by the Kubernetes in this repository.

The intention of this document is to:

* Help newcomers understand what is deployed and why
* Track changes in the deployment and lessons learned

The intention of this document is NOT to:

* Outline precisely the requirements for an enterprise deployment of a
  remote-execution service
* Anticipate the implementation difficulties with any third party software
  requirements a deployment may have

## Index

  1. [Deployment Architecture](#deployment-architecture)
  2. [Components](#components)

## Deployment Architecture

We hope to deploy a [BuildBarn] instance that is robust, scalable and compatible
with [BuildStream], [Bazel] and [RECC]. This deployment will be based roughly
on those found at [bb-deployments]. Our application architecture will look
something like this, if all goes well:

```mermaid
graph LR;
  ingress[Traefik]:::loadBalancer;

  subgraph Remote Execution Stack
    %% Nodes
    bb-forbidden[bb-storage Forbidden]:::service;
    bst-artifact-server[BuildStream Artifact Server]:::service;

    bb-storage-cas[bb-storage CAS and Action Cache]:::service;
    bb-storage-router[bb-storage gRPC demultiplexer]:::service;
    bb-scheduler[bb-scheduler]:::service;
    subgraph Worker
      bb-worker[bb-worker]:::service;
      bb-runner[bb-runner]:::service;
    end

    artifacts[(Artifact Store)]:::storage;
    objects[(On Disk Store)]:::storage;

    %% Edges
    bb-storage-router --> bb-scheduler;
    bb-storage-router --> bb-storage-cas --> objects;
    bb-worker --> bb-runner;
    bb-worker --> bb-scheduler;
    bb-worker --> bb-storage-cas;
    bb-runner --> bb-worker;
  end

  subgraph Monitoring Stack
    %% Nodes
    grafana[Grafana]:::service;

    loki[Loki]:::service;
    promtail[Promtail]:::service;

    prometheus[Prometheus]:::service;
    node-exporter[Node Exporter]:::service;

    cassandra[Cassandra Database]:::service;
    jaeger-collector[Jaeger Collector]:::service;
    jaeger-query[Jaeger Query]:::service;

    %% Edges
    grafana --> loki --> promtail;
    grafana --> prometheus --> node-exporter;
    jaeger-query --> cassandra --> jaeger-collector;
  end

  %% Edges
  ingress -- build.bazel* --> bb-storage-router;
  ingress -- google.bytestream* --> bb-storage-router;
  ingress -- Auth Failed --> bb-forbidden;
  ingress -- buildstream* --> bst-artifact-server --> artifacts;

  ingress --> grafana;
  ingress --> prometheus;
  ingress --> jaeger-query;

  %% Styling
  classDef loadBalancer fill:#591,stroke:#333;
  classDef service fill:#f75,stroke:#333;
  classDef storage fill:#57f,stroke:#333;
```

This should be the necessary architecture to achieve our goal for a robust
BuildBarn deployment.

We will use Kubernetes for the deployment, which gives us the advantage of
scalability, and meaning we don't have to be too fussy with cloud provider
configuration - Kubernetes just handles a lot of things. Ideally all of the
components defined in Kubernetes will be stateless and ephemeral. At present
there are two single points of failure in the cluster - the BuildStream
artifact storage and the bb-scheduler. `bst-artifact-server` must exist
as a stateful singleton by necessity of design - it's a standalone application -
but work is underway to convert this to a more scalable service using the Remote
Asset API. bb-scheduler also must exist as a stateful singleton at the moment,
and as the job queue is stored in memory is a single point of failure.

## Components

This section details the components, what they are for, and how they are
configured.

### Traefik Ingress

We use [Traefik] primarily to route the [gRPC] traffic for remote execution to the
relevant places. This is particularly useful for remote caching, where we need
to only allow certain clients to push to the cache. It should also serve as a
load balancer to the separate pods for the other services.

Traefik is the location of TLS termination, beyond here traffic is internal and
unencrypted. We don't decrypt on the infra load balancer level as we can't
guarantee that this will be available in all scenarios, and more importantly
doing so may mangle gRPC traffic.

Traefik should also serve as the authentication and authorisation point, although
what form this will take is still TBD. Currently we have only a BuildStream
Artifact Server, which has push access restricted by TLS client authentication.

For information on how we route traffic using [Traefik], see our
[Routing Strategy doc](./routing-strategy.md).

In earlier deployments we used [Nginx] for ingress rather than Traefik. We
switched in order to leverage Traefik's superior [gRPC] and Kubernetes support,
and to simplify configuration as Nginx was becoming unruly.

### Remote Execution Stack

#### bb-storage Forbidden

This is a bb-storage using the `error` blobstore backend to return a gRPC
`PERMISSION_DENIED` error. Calls that attempt to push to the pull cache are
routed here. It may be possible to omit this element and instead return a
gRPC error from the ingress.

#### bst-artifact-server

This is a service used to store the artifact references for BuildStream
artifacts. Currently this is compatible with BuildStream 1.4 and the master
branch, but this may change soon. The artifact server is used to translate
cache keys into CAS digests (and some extra information for master).

Unfortunately this currently requires on disk storage, and is a single point
of failure for our deployment. We would like for this to be in an in-memory
store, like Redis, but this likely requires us writing our own service.

#### bb-storage gRPC demultiplexer

This is essentially a router used to send information to either the scheduler
or the CAS/Action Cache.

#### bb-storage CAS and ActionCache

The CAS is a Content Addressable Storage, which stores blobs named after a hash
of their content. This is used to cache the results of builds, whether they be
from Bazel, BuildStream or RECC. As the blobs are addressed by their content,
things are deduplicated to a file level, making this storage efficient. The
ActionCache is similar to the BuildStream Artifact Cache, but for Bazel
Actions. An action is a single step of the compilation process, e.g. a single
gcc invocation. The ActionCache maps actions to blob digests.

As bb-storage abstracts the blobstore used, the exact places we send blobs are
easily changed. Currently, we have a sharded on-disk solution to the storage,
where each pod is attached to an EBS volume used as a shard of storage. This
gives us a reasonably robust storage solution, in that we don't lose all data if
a shard is corrupted.

Our initial plan was to send large blobs to S3 and small blobs to Redis (on
Elasticache) for the first (AWS) phase. Unfortunately this proved fragile due to
Redis being temperamental (using it for medium-term data storage is far from
its intended use case) and S3 access times. The S3 access times were _such_ a
slow down that it was causing the nodes to OOM, as they couldn't push the blobs
fast enough. In the future, it may be worth evaluating Redis as a fast cache for
small blobs, and S3 as a long term data store.

#### bb-scheduler

The bb-scheduler is the service that actually handles remote execution requests
and assigns jobs to workers.

#### bb-worker

We intend to have a large number of bb-worker/bb-runner pairs, which actually
do the jobs. bb-workers communicate with the scheduler to get allocated jobs to
perform, and then tell the bb-runner what job to execute.

#### bb-runner

The bb-runner is where compile actions are actually done. This is often used to
define the toolchain used for compilation, but we would prefer to do this using
bazel toolchains. BuildStream may also experience difficulties here as it uses
a sandbox to stage only the dependencies it has as a unique toolchain.
Hopefully [BuildBox] will solve this for us.

### Monitoring Stack

#### Prometheus

[Prometheus] is an open-source systems monitoring and alerting toolkit. It works
by adding some labels to the Kubernetes objects you want to gather metrics from,
Prometheus will search the labelled objects in the cluster and scrape the metrics
once it finds them, storing them in a time series database.

To make this possible, the application running in those k8s objects must have
had implemented the Prometheus API on itself. Below there is a list of the
components we are currently monitoring and the information they can provide us:

* BuildBarn:
  * grpc calls statistics.
  * blobstore statistics.
* Traefik:
  * Connections statistics.
  * Http request.
  * If traefik is up.
* Prometheus itself:
  * Http requests

For more detail read our [Prometheus document](./prometheus.md).

Prometheus has a complementary plugin called [Node Exporter]. Node exporter
exports metrics from the host machine. Our intention is to use it to obtain
metrics from the nodes our infrastructure is running in. [Node exporter]
allows you to gather system levels metrics, such as:

* CPU statistics.
* Disk statistics.
* Filesystem statistics.
* Load average
* IPVS status
* Memory statistics.
* Network statistics.
* Scheduler statistics.

#### Grafana

[Grafana] provides data visualisation, allowing an operator or user to view the
status of services in the cluster. Grafana gathers no data itself, and instead
relies on tools like Prometheus to provide the data. More information can be
found in the [Grafana doc](./grafana.md) in this repo.

#### Loki

[Loki] aggregates logs from all pods in the cluster. This logging data can then
be lined up with data from prometheus and visualised by extracting more
pertinent information.

Loki itself acts as a database for the logs, it does not scrape the logs itself.
In order to do the scraping, we deploy [Promtail], which is configured in a
similar manner to [Prometheus].

For more information, read our [Loki doc](./loki.md). It may also be informative
to read the [Log Aggregation Spike](./spikes/log-aggregation.md)

#### Jaeger

[Jaeger] is distributed tracing system, collects gRPC traces in the cluster.
gRPC traces allow us to gather insights in the network traffic between the
different application in the cluster. For more detail, read our
[gRPC Tracing doc](./grpc-tracing.md).

At present we don't enable Jaeger, due to difficulties with rolling it out in
deployments. The cause is that we need to run a job to standup the Cassandra
databases before deploying the rest of the Jaeger stack. We haven't developed
tooling in Argo CD to perform this yet.

Jaeger Tracing in BuildBarn is also currently quite limited, and needs work to
be improved.

[//]: # (EXTERNAL LINKS BLOCK)

[//]: # (REMOTE EXECUTION)
[Buildstream]: https://www.buildstream.build/
[Buildbarn]: https://github.com/buildbarn
[Bazel]: https://bazel.build/
[RECC]: https://gitlab.com/bloomberg/recc
[bb-deployments]: https://github.com/buildbarn/bb-deployments
[BuildBox]: https://gitlab.com/buildgrid/buildbox/
[gRPC]: https://grpc.io

[//]: # (WEBSERVER)
[Nginx]: https://nginx.com
[Traefik]: https://containo.us/traefik/

[//]: # (MONITORING)
[Prometheus]: https://prometheus.io
[Grafana]: https://grafana.com
[Loki]: https://github.com/grafana/loki
[Jaeger]: https://www.jaegertracing.io
[Promtail]: https://github.com/grafana/loki/tree/v1.5.0/docs/clients/promtail
[Node Exporter]: https://github.com/prometheus/node_exporter
