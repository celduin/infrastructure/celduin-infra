# Loki

## Viewing Logs

The logging data is scraped by [Promtail], sent by this to [Loki] and made
accessible by [Grafana]. [Promtail] will scrape logs from various targets and
label them, after this, the logs will be searchable by the user in [Grafana].
To have a quick view of the logs:

1. In [Grafana] open the `Explore` tab on the left hand side.
2. Once in the `Explore` tab, select `loki-datasource` as the datasource in the
   top left side of the window.
3. In the `Log labels` tab write `{}`, put the cursor inside the brackets and
   press the down arrow in your keyboard. This will show you all the possible
   labels you can use to search for logs in [Loki]. For this example select
   `pod_name`.
4. After selecting a label, [Grafana] will show you all the possible values for
   that specific label, in this case `pod_name`. For this example select any of
   the `traefik-ingress-controller` instances.
5. Now should be visible the logs from the selected label, value pair. In this
   `traefik-ingress-controller` instance pod.

## Expanding the logging possibilities

If the currently provided labels are not enough for you, feel free to add more
to the collection by configuring `promtailConfig` in [logProcessor.libsonnet].

You can achieve this by:

* Creating new `relabel_configs` sections in `kubernetes-pods` job. This will
  relabel the labels obtained by scrapping the Kubernetes API and make them
  accessible for you. Have a look to [promtail kubernetes_sd_config configuration]
  for information about how to do this.
* Add a new job to discover new targets and obtain new logs. Have a look at
  [promtail configuration]

## TODO

Obtaining permanent logging information dashboards will be our next step on
improving the logging visualization. This way, the `Explore` tab will not be needed
any longer.

[//]: # (EXTERNAL LINKS BLOCK)

[Promtail]: https://github.com/grafana/loki/tree/master/docs/clients/promtail
[Loki]: https://github.com/grafana/loki
[Grafana]: https://github.com/grafana/grafana
[logProcessor.libsonnet]: kubernetes/monitoring/log-aggregation/logProcessor.libsonnet
[promtail kubernetes_sd_config configuration]: https://github.com/grafana/loki/blob/master/docs/clients/promtail/configuration.md#kubernetes_sd_config
[promtail configuration]: https://github.com/grafana/loki/blob/master/docs/clients/promtail/configuration.md
