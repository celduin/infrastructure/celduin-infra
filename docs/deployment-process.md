# Deployment Process

In this document you will find explanations on the process followed to deploy Kubernetes resources to the different environments.

## Context

This step is one of the last steps of the automated Infrastructure, at this step we have a set of Kubernetes resources defined that we want to deploy to a given cluster.  
At the current time we will make the following assumptions:

+ The Kubernetes definitions are valid.
+ The environment specified is the intended one (no check to prevent passing the wrong yaml files or pointing at the wrong environment).
+ The Cluster already exists and can support the definitions being passed.
+ Argo-CD is already running and configured properly.

## Overview

The deployment process is a little unintuitive, certainly from an outside perspective. The process goes thusly:

1. A pipeline is set off in this repository.
2. The pipeline deploys any terraform changes.
3. The pipeline generates JSON files for Kubernetes configuration
4. Generated JSON files are pushed to https://gitlab.com/celduin/infrastructure/infrastructure-declarations
5. Argo CD (deployed in the cluster) notices these definitions have changed, and updates the cluster

The following diagram may also prove instructive:

```mermaid
graph TD;
  %% Nodes
  developer[Developer];
  celduin-infra[Celduin Infra CI Pipeline];
  infra-declarations[infrastructure-declarations repository];
  argo[Argo CD];
  services[Cluster Services];

  %% Edges
  developer -- Merge to master/tag repo --> celduin-infra;
  celduin-infra -- Generate and push --> infra-declarations;
  infra-declarations -- Monitored by Argo --> argo;
  argo -- Deploy definitions to --> services;
```

Let's run through each step in detail.

## Deployment Pipelines

Not every pipeline in the repository causes a deployment to each environment. Our current policy is as follows:

+ Pipelines on master cause deployment to staging
+ Pipelines on tags cause deployment to production

This policy ensures that we have one cluster which is up to date with the repository, on which we can test for regressions. This way, tagging the repository for a release is less hair-raising.

## Deploying Terraform

Terraform changes are currently not handled very well by pipelines, but some minimal changes can be handled by the CI. This is done in pipeline using our [`terraform-jsonnet` script](../scripts/terraform-jsonnet). Larger terraform changes may cause Argo CD to be removed from the cluster (e.g. removing the node), in which case the cluster will need to be manually bootstrapped.

## Generating JSON files

The pipeline will then gather information from terraform and generate the actual kubernetes definitions which will be used. The top level definitions for the cluster can be found in `kubernetes/clusters/$ENVIRONMENT/deployment.jsonnet`, with the templating it's built upon contained under the `kubernetes/` umbrella.

## Push the Generated JSON

The pipeline will then push the generated JSON to the [infrastructure-declarations](https://gitlab.com/celduin/infrastructure/infrastructure-declarations) repository, on a branch corresponding to the environment.

## Argo CD deploys the Update

Argo CD is configured to watch the branch corresponding to the environment it is deployed on in the infrastructure-declarations repository. When changes appear, it will automatically deploy them to the cluster.

## Secrets Exception

Currently we do not have a solution to let Argo-CD handle Kubernetes Secrets. Until a solution is implemented we will have to manually manage thoses resources.
Only the certficates are Secrets and need to be handled manually.
