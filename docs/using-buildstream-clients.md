# BuildStream Client Guide

This repository provides a Kubernetes definitions for a Remote Cache and Remote Execution service that are compatible with BuildStream. Remote Execution support is experimental and targets BuildStream development snapshots on the lead up to BuildStream 2.0. BuildStream 1.93.4 is known to work with this deployment as of v0.7.0.

## BuildStream Configuration

BuildStream must be configured correctly to use the services. The following configuration changes can be made either to a Project's `project.conf` file or a user's `buildstream.conf`.

### Remote Cache Access

There are two modes of access to a remote cache - Pull and Push. The definitions in this repository are flexible on the specification of access control to either endpoint - each is configurable to use TLS client authorisation or not. For example, the `public` environment offers a pull endpoint without client authentication, but authorisation is required to push to the cache. We do not recommend allowing all users to push to a cache, especially if the cache is publically available.

Configuring BuildStream to use a cache server can be done using the following YAML snippet:

```yaml
artifacts:
# Pointing to an endpoint without Client Authentication
- url: https://public.aws.celduin.co.uk

# Configuration for an endpoint with Client Authentication
- url: https://push.public.aws.celduin.co.uk
  client-cert: /path/to/client-certificate.pem
  client-key: /path/to/client-key.pem
  push: true  # Defaults to false, tells BuildStream to try to push to the remote
```

Note, the client certificate/key pair - if used - must be PEM encoded. A good heuristic to check for this is to make sure that the certificate begins with `----BEGIN CERTIFICATE-----`, and the key begins with `-----BEGIN PRIVATE KEY-----`. The key may have some modifier (e.g. `RSA`, `EC`) before `PRIVATE`, but these keys should still be PEM encoded.

Some deployments (e.g. `dev`) provide self-signed certificates for the server to use. In this case, the server will not be trusted by systems without this certificate in their trust root. If a server is untrusted by a system, BuildStream allows users to specify a TLS certificate for the server with `server-cert: /path/to/server-certificate.pem`. **NOTE**: If a server is not trusted by your system, make sure that you _know_ that you trust the server before using its certificate like this.

### Remote Execution Access

Remote Execution services are available only behind the `push` endpoint. This is because remote execution necessarily requires write access to the Content Addressable Storage (CAS), so it's equivalent to pushing to the cache directly. If the `push` endpoint requires TLS client authorisation then certificates will need to be specified again. If certificates are required, then they will need to be PEM encoded, as for remote cache.

Configuring BuildStream to use a remote execution service can be done using the following YAML snippet:

```yaml
 remote-execution:
  execution-service:
    url: https://push.public.aws.celduin.co.uk
    instance-name: remote-execution
    client-cert: /path/to/client-certificate.pem
    client-key: /path/to/client-key.pem
  storage-service:
    url: https://push.public.aws.celduin.co.uk
    instance-name: remote-execution
    client-cert: /path/to/client-certificate.pem
    client-key: /path/to/client-key.pem
  action-cache-service:
    url: https://push.public.aws.celduin.co.uk
    instance-name: remote-execution
    client-cert: /path/to/client-certificate.pem
    client-key: /path/to/client-key.pem
```

Note, the `instance-name` parameter must be `remote-execution` to work with clusters specified in this repository.

Remote execution for BuildStream is not well supported by infrastructure in this repository, nor well tested. Currently we know that freedesktop-sdk's bootstrap can be built using remote execution, but it can be temperamental. Use with caution.

## Known Issues

### Remote Cache

* bst 1.93.4: Sometimes BuildStream will fail to push artifacts to the cache, but report that the artifact is already cached. Client issue fixed in master.
* bst 1.93.3: BuildStream will intermittently fail to push to CAS. Client issue fixed in 1.93.4.

### Remote Execution

* Job timeout is currently quite small - 3600s at most. This will not be enough to build a lot of components.
