# Certificate Management

This document aims to outline how we manage TLS certificates for the cluster, for both server and client authentication.

## Client Authentication Certificates

For the present, we use self-signed TLS certificates for client authentication. This has the advantage of being simple to generate new certificates, but unfortunately has implications for security. Using a self-signed certificate means that in order to be trusted by the server, you need either the same certificate or one signed by it. If this gets compromised, anyone can access the cluster, and revoking it means all clients need new certificates too.

To generate a new client certificate, use the following openssl command:

```bash
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=client" -out client.crt -keyout client.key
```

This should then be passed into the relevant cluster as a secret, see `dev` for an example of the API.

## Server Authentication Certificates

The certificates we use for TLS server authentication are managed by [cert-manager]. This allows us as operators to remove the conscious need to think about certificate management (at least for these certificates).

### Let's Encrypt and ACME

For specifics, we configure [cert-manager] to use [Let's Encrypt] as an Issuer, the upstream Certificate Authority (CA) from which it obtains signed certificates. [Let's Encrypt] is an [ACME] server, which is a protocol designed to allow automatic management of certificates. In order to do this reliably, it defines a couple of challenges which are used to validate that a Certificate Signing Request (CSR) is actually coming from the owner of the domain.

There are two challenges widely used - HTTP01 and DNS01. HTTP01 is based on HTTP, and requires configuration to allow traffic to reach whatever server the domain is pointed at. DNS01 challenges are based on control of the domain. cert-manager has abstractions for both of these challenges.

We use DNS01 to provision our certificates. This is because it's simpler on a Kubernetes level, and HTTP01 challenges in cert-manager are based on Kubernetes `Ingress` resources, which we don't use in favour of a Custom Resource Definition (CRD) that integrates better with our ingress controller.

### cert-manager Configuration

In order to answer the DNS01 challenges, cert-manager needs access to our DNS hosting. There were three potential avenues for allowing this access:

1) Create an IAM User, and pass the credentials to cert-manager
2) Create an IAM Role, and pass the ARN to cert-manager. Cert-manager will switch to this role before continuing with the challenge.
3) Add the policy directly to the EC2 instance's IAM role.

We use the third option - (1) meant that we have to manager the credentials as a `Secret`, which isn't ideal. (2) and (3) were a close call, but in the end (3) was favoured, as (2) requires the creation of two IAM policies - one to allow Route53 access, and another to allow the EC2 instance to `AssumeRole`. As a result, (2) is actually _more_ permissive than (3), because it potentially allows the assumption of more roles. In any case (3) was simpler, as (2) requires passing the ARN for the role to be assumed over into Kubernetes.

Now we have an issuer - an upstream to obtain certificates from - we need to create a resource to represent the actual certificate that should be kept up to date. In cert-manager this is the appropriately named `Certificate` resource. A `Certificate` contains information about the requested certificate - the domains it's valid for, the duration before expiry, the duration before automatic renewal and pretty much anything else that can be configured about an X.509 certificate.

### Deploying cert-manager

In order to be used, cert-manager must be deployed into the cluster separately. In an ideal world this would be done using the upstream manifests, but we needed to modify this. As a result we have vendored the manifest in `kubernetes/ingress/cert-manager-legacy.yaml`. To deploy, simply run:

```bash
kubectl apply -f kubernetes/ingress/cert-manager-legacy.yaml
```

When deploying the actual certificates, bear in mind that the `Issuer` must be _ready_ before creating the `Certificate`. It's probably best to do this separately when bootstrapping a new cluster. This can be achieved by running:

```bash
# Generate JSON
mkdir -p target
jsonnet -m target kubernetes/clusters/dev/deployment.jsonnet

# Deploy cert-manager
kubectl apply -f kubernetes/ingress/cert-manager-legacy.yaml

# Deploy Issuer
kubectl apply -f target/rex-Namespace.json -f target/letsencrypt-Issuer.json
```

Then wait until `kubectl get issuer -n rex` shows that the `Issuer` is `READY`, and run `kubectl apply -f target` to finish the deployment.

[//]: # (EXTERNAL LINKS)
[cert-manager]: https://cert-manager.io/
[Let's Encrypt]: https://letsencrypt.org/
[ACME]: https://tools.ietf.org/html/rfc8555
