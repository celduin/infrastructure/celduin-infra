# Deployment Environments

We have several different deployments of our infrastructure, the purposes of
which are hopefully adequately defined in this document. Broadly speaking,
there are three basic kinds: Production, Staging and Development.

Each environment has a unique `clusterId`, which is used throughout as an
identifier. This only really comes into play in the terraform jsonnet,
however it is used to pass terraform outputs into kubernetes, and is used in
the subdomain of the deployed cluster. Due to the current scripting, we require
a cluster to have the following "structure":

* Terraform defined in `terraform/$clusterId.tf.jsonnet`
* Kubernetes defined in `kubernetes/clusters/$clusterId`

The top level Kubernetes should be defined in `kubernetes/clusters/$clusterId/deployment.jsonnet`
for consistency.

## Production

The following environments are considered "Production". Any changes to these
clusters should be done only on releases of celduin-infra, and ideally be
performed by the CI only.

### Public

This server is publically available to pull artifacts from, but requires a TLS
client certificate to push artifacts to. The `clusterID` is `public`. This is
available at https://public.aws.celduin.co.uk.

## Staging

Obviously, we'd rather not just deploy a change without testing it first, which
means that we need an environment to test changes before we merge. This is
where `staging` comes in. This is intended to be an environment that changes
can be applied to in order to check that things won't break in production. At
the moment it has TLS client authentication to both push and pull, as this is
more complicated than either alternative. This is available at https://staging.aws.celduin.co.uk.

## Development

As the other environments have a terraform backend, as well as requiring Redis
and S3, none of them are suitable to test changes either quickly on a
developer's machine. As a result we define a `dev` deployment, which is
suitable for use on a developer's machine or to quickly provision a small
cluster in the cloud (for some value of "quickly"). This is very different to
the production environments, and uses different backends for the object store.

## Adding new environments

To add a new environment, let's say `foo`, then you need to add the following
files:

* `terraform/foo.tf.jsonnet`
* `kubernetes/clusters/foo/deployment.jsonnet`

For ease of creation, it's probably easier to copy the staging config and
modify as necessary.
