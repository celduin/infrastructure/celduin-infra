# Log Aggregation Spike

## Introduction

The aim of this document is to reflect the discussion that took place during the
implementation of the logging aggregation. During that process three solutions
were proposed as a way to handle the logging information:

* [ElasticSearch]
* [InfluxDB]
* [Loki]

In the next sections each of them characteristics will be described:

## [ElasticSearch]

ElasticSearch is a search server used for full text search, structured search,
analytics, and all three in combination.

## Characteristics

* Written in Java.
* Apache 2 License.
* Distributed.
* Scalable.
* Uses [Lucene] for internally for all of its indexing and searching.
* Every data field is indexed and searchable.
* It is schema-less, and can store data from various log sources.
* Can handle all types of data, including textual, numerical, geospatial, structured, and unstructured.
* It can automatically guess data types.
* It can store very large datasets.
* It can efficiently search across a very large dataset.
* It scales horizontally, and can tolerate node failures.

## Elastic search concepts and architecture

* An **[ElasticSearch] node** is a single instance of elastic search.
* An **Index** is a collection of documents, is where the data is stored. An
  Index is stored across multiple nodes to make data highly available.
* **Shard**: Indexes are usually split into elements known as shards that are
  distributed across multiple nodes.
* **Replica**: By default, Elasticsearch creates five primary shards and one replica for each index.
* An **[ElasticSearch] cluster** is a group of one or more ElasticSearch nodes
that are connected together. The nodes are:
  * Master-eligible node: Performs tasks like create/delete and index, track the
 cluster nodes, determine the location of the allocated shards.
  * Data Node: Contains data that contains indexed documents, handles operations
  such as CRUD, search and aggregation.
  * Ingest Node: Process a document in pipeline mode before indexing the document.
  * Coordinating-only Node: If the three nodes described above are disabled, this
node will acts as a coordination node that performs routing requests.

The deployment would be basically composed by a:

* **Client**: Provide the API endpoint and can be used for queries.
* **Master**: These nodes provide coordination.
* **Data**: Store the data and inverted index.

## Pro/cons

### Pros

* Many projects used it, so there should be lots of information to help.
* Architected for scale.
* Distributed storage and processing
* Nodes handling: Every time a node is added or removed the master re-shards the cluster and re-balances how shards are organized on nodes
* Full-text search: it performs searches based on language and returns those documents that match the search condition.
* Parallel processing: Elastic Search prefers to process data on several nodes.
* Built-in parallelization: user doesn't need to lift his/her finger to structure the queries' paths among shards.
* Self-organizing behaviour: Never exists a single point failure

### Cons

* Can be finicky with the underlying infrastructure
  * Once you scale up, capacity planning is essential to get it to work at the same speed.
  * It requires the right kind of hardware to perform at peak capacity.
  * If you have too many small servers it could result in too much overhead to manage the system.
  * If you have just a few powerful servers, failover could be an issue.
* Needs to be fine-tuned to get the most out of it.
* No geographic distribution.  ElasticSearch doesn't recommend distributing data across multiple locations globally.
* Language constraint: To handle the requests and responses, Elasticsearch doesn't have multi-language support.
* Well-organized: To run the queries correctly, you need to take care of a hierarchy of indexes, IDs, and types. Besides this, you also need to ensure the status of all nodes must be 'green' and not 'yellow'
* SSD's requirement: Elasticsearch needs a group of servers having 64GB of RAM to work efficiently.

## [Influxdb]

### Characteristics

* [MIT License]
* Written in Go.
* InfluxQL supports regular expressions, arithmetic expressions, and time series-specific functions.
* InfluxDB automatically compacts data to minimize storage space.
* Possibility to downsample the data; keeping high-precision raw data for a limited time and storing the lower-precision, summarized data for much longer or until the end of time

### Storage Engine

The InfluxDB storage engine is meant to ensure that:

* Data is safely written to disk.
* Queried data is returned complete and correct.
* Data is accurate (first) and performant (second).

#### Behaviour

1. Data is write using [line protocol] sent via HTTP POST request to /write endpoint.
2. Then data points are are compressed and written to the "[Write Ahead Log]". WAL ensures data is durable in case of an unexpected failure.
3. Data points are written to cache and make them immediately queryable.
4. Memory cache is periodically written to disk in form of "[Time-Structured Merge Tree]". This groups field values by [series keys], and then orders them by time. The compressed data is formatted in columns and stores only stores deltas between values in series.

#### Tims Series Index

The TSI stores series keys grouped by measurement, tag, and field. This allows the database to answer two questions :

* What measurements, tags, fields exist?
* Given a measurement, tags, and fields, what series keys exist?

### Pro/cons

#### Pros

* Time Slot aggregated data (buckets).
* [Retention Policies]:  knob to adjust retention of data up or down, sets how long you want to keep data tagged with a specific policy.
* [Into Clause]: writes query results to a specified measurement, (back to the database).
* Data are [sharded].

#### Cons

* By default a new shard is created each week, and data are kept forever (infinite retention), write times eventually suffering a lot. Needs proper configuration.
* Shard configuration is dependant on retention periods.  When data are discarded because of the retention date expiring, the entire shard is lost. Therefore, if shards are too large (let’s say one month), and you have a retention period of one month, it will mean that two months of data have to be kept on the disk.
The open source version doesn't come with support for distributed deployments (clustering), you need to upgrade to [InfluxCloud] or enterprise edition.
* No support for [histogram function].
* Backup retention policies are too limited. Taken from [InfluxDB: The Good, the Bad, and the Ugly]:

> The other gripe we have is that we’d like to use the Cloud offering, but it’s backup retention policies are too limited. We don’t want to keep all the data in the live InfluxDB database as it impairs performance (see discussion above on shards and retention policies), and forces us to buy the more expensive live clusters with increased disk space. Instead we want to keep backups of archived data in something like S3, which allows us to do one-off analysis of old data for R&D purposes. Again, we ended up implementing our own solution that does exactly that.
---

### Observations

InfluxDB is a component of the TICK Stack, a set of open source projects designed to handle massive amounts of time-stamped information to support metrics analysis. InfluxDB 2.0 (still in development) includes [InfluxDB] + [Chronograf] and [Kapacitor], which basically is added dashboard stream data processing.

## [Loki]

Loki is a set of components that compose a fully featured monitoring stack.

### Characteristics

* Apache 2 license.
* Time-series data source.
* Supported by [Graphana].
* Contents organized by labels instead of using an index.
Logs are stored in plain text tagged with a set of label names and values.

* Horizontally-scalable.
* Multi-tenant, each tenant has an ID.
* Log data is compressed and stored in chunks in stores such as S3 and GCS.
* Two working Modes:
  * Single process: All Loki microservices in same one process, for small scale.
  * Horizontally Scalable: Loki Microservices broken into separate processes.
* gRPC is used for between service communication (Ingester-Distributor).

### Architecture/Behaviour

![Logging Architecture](https://grafana.com/static/assets/img/blog/image1.png)
Image taken from [15].

![Loki Architecture](https://grafana.com/static/assets/img/blog/image4.png)
Image taken from [15].

Besides the elements shown in the images above, there are two more elements that
are part of Loki:

* **Hash ring**: A hash ring stored in Consul (microservices mode) or in memory
(monolithic mode) is used to achieve consistent hashing. All ingesters register
themselves into the hash ring with a set of tokens they own, then distributors use
the hashing in conjunction with a configurable replication factor to determine
which instances of the ingester service should receive a given stream.
* **Table Manager**: Loki supports storing indexes and chunks in table-based data
 storages. When such a storage type is used, multiple tables are created over the
time: each table contains the data for a specific time range. The Table Manager
takes care of creating a periodic table before its time period begins, and deleting
it once its data time range exceeds the retention period.

### Write Steps

1. Once the logs are collected they are sent to the **distributor**.
1. The **distributor** then hashes the labels and the logs and then looks up which **ingester** to send the entry to based on the hash value.

  The **ingester** is a component in charge of building and then later flushing the chunks. We have multiple ingesters, and the logs belonging to each stream should always end up in the same ingester for all the relevant entries to end up in the same chunk. We do this by building a ring of ingesters and using consistent hashing.

1. The **ingester** will receive the entries and start building chunks by gzipping the logs and appending them. Once the chunk “fills up”, we flush it to the database.

### Read Steps

1. Given a time-range and label selectors, the **querier** looks at the index to figure out which chunks match, and greps through them to give you the results.

### Storage Engine

Loki stores two sets of data:

* Chunks: Compressed streams of logs. The supported stores are:
  * Amazon DynamoDB
  * Google Bigtable
  * Apache Cassandra
  * Amazon S3
  * Google Cloud Storage

* Index: stores each stream's label set and links them to the individual chunks. The supported stores are:
  * Amazon DynamoDB
  * Google Bigtable
  * Apache Cassandra
  * BoltDB (doesn't work when clustering Loki)

### Scalability

* Chunks and index are easily scalable because they are in object stores.
* Distributors and queriers are stateless components than can horizontally scale.

## Pro/cons

### Pros

* When deployed alongside Prometheus with Promtail same labels will be scraped.
  Having logs and metrics with the same levels enables users to switch between
  metrics and logs, helping with root cause analysis.

### Cons

  Undefined yet.

## Sources

* [0] [ElasticSearch]
* [1] [ElasticSearch Server]
* [2] [Elasticsearch: The Definitive Guide]
* [3] [Advanced ElasticSearch 7.0]
* [4] [Kubernetes Logging and Monitoring: The Elasticsearch, Fluentd, and Kibana (EFK) Stack – Part 2: Elasticsearch Configuration]
* [5] [Scaling Elasticsearch – The Good, The Bad and The Ugly]
* [6] [What is ElasticSearch? Pros, Cons and Features List]
* [7] [InfluxDB: The Good, the Bad, and the Ugly]
* [8] [Influxdb v1.8 docs]
* [9] [Influxdb]
* [10] [MIT License]
* [11] [Loki]
* [12] [Loki Documentation]
* [13] [Loki Storage]
* [14] [Loki Architecture]
* [15] [Loki: Prometheus-inspired, open source logging for cloud natives]
* [16] [Loki compared to other log systems]
* [17] [Loki Design Document]

[//]: (ElasticSearch Links)

[ElasticSearch]: https://www.elastic.co/
[ElasticSearch Server]: https://books.google.co.uk/books?id=PEFK3MuwBsIC&printsec=frontcover#v=onepage&q&f=false
[Elasticsearch: The Definitive Guide]: https://www.elastic.co/guide/en/elasticsearch/guide/current/index.html
[Lucene]: https://lucene.apache.org/
[Advanced ElasticSearch 7.0]: https://books.google.es/books?id=fyisDwAAQBAJ&printsec=frontcover#v=onepage&q&f=false
[Kubernetes Logging and Monitoring: The Elasticsearch, Fluentd, and Kibana (EFK) Stack – Part 2: Elasticsearch Configuration]: https://platform9.com/blog/kubernetes-logging-and-monitoring-the-elasticsearch-fluentd-and-kibana-efk-stack-part-2-elasticsearch-configuration/
[Scaling Elasticsearch – The Good, The Bad and The Ugly]: https://logdna.com/scaling-elasticsearch-the-good-the-bad-and-the-ugly/
[What is ElasticSearch? Pros, Cons and Features List]: https://www.courseya.com/blog/what-is-elasticsearch-pros-cons-and-features-list/

[//]: (InfluxDb Links)

[InfluxDB]: https://www.influxdata.com/time-series-platform/
[Chronograf]: https://www.influxdata.com/time-series-platform/chronograf/
[Kapacitor]: https://www.influxdata.com/time-series-platform/kapacitor/
[Write Ahead Log]: https://v2.docs.influxdata.com/v2.0/reference/internals/storage-engine/#write-ahead-log-wal
[Time-Structured Merge Tree]: https://v2.docs.influxdata.com/v2.0/reference/internals/storage-engine/#time-structured-merge-tree-tsm
[series key]: https://v2.docs.influxdata.com/v2.0/reference/glossary/#series-key
[Retention Policies]: https://docs.influxdata.com/influxdb/v1.3/query_language/database_management/#create-retention-policies-with-create-retention-policy
[Into Clause]: https://docs.influxdata.com/influxdb/v1.3/query_language/data_exploration/#the-into-clause
[sharded]: https://en.wikipedia.org/wiki/Shard_%28database_architecture%29
[InfluxCloud]: https://www.influxdata.com/products/editions/
[line protocol]: https://v2.docs.influxdata.com/v2.0/reference/syntax/line-protocol/
[histogram function]: https://github.com/influxdata/influxdb/issues/5930
[InfluxDB: The Good, the Bad, and the Ugly]: https://bluefox.io/influxdb-good-bad-ugly
[Influxdb v1.8 docs]: https://docs.influxdata.com/influxdb/v1.8/
[MIT License]: https://github.com/influxdata/influxdb/blob/master/LICENSE

[//]: (Loki Links)

[Loki]: https://grafana.com/oss/loki/
[Loki: Prometheus-inspired, open source logging for cloud natives]: https://grafana.com/blog/2018/12/12/loki-prometheus-inspired-open-source-logging-for-cloud-natives/
[Graphana]: https://grafana.com/
[Loki Documentation]: https://github.com/grafana/loki/blob/master/docs/README.md
[Loki Architecture]: https://github.com/grafana/loki/blob/master/docs/architecture.md
[Loki compared to other log systems]: https://github.com/grafana/loki/blob/master/docs/overview/comparisons.md
[Loki Storage]: https://github.com/grafana/loki/blob/master/docs/operations/storage/README.md
[Loki Design Document]: https://docs.google.com/document/d/11tjK_lvp1-SVsFZjgOTr1vV3-q6vBAsZYIQ5ZeYBkyM/edit
