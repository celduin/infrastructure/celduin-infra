# Bazel Client Guide

This repository provides Kubernetes definitions for a Remote Cache and Remote Execution service that are compatible with Bazel. Due to requiring TLS client authorisation in (most) deployments, Bazel 3.1.0 is the minimum version usable, and the most widely tested. Bazel 3.1.0 is known to work with this deployment as of v0.7.0.

## Bazel Configuration

Bazel must be configured to use the services. The following configuration changes can be made either to a user's `bazelrc`, a project's `bazelrc` or passed to the command line. Given the mass of CLI options which bazel can accrue in a single call, we recommend using a bazelrc file.

### Remote Cache Access

There are two modes of access to a remote cache - Pull and Push. The definitions in this repository are flexible on the specification of access control to either endpoint - each is configurable to use TLS client authorisation or not. For example, the `public` environment offers a pull endpoint without client authentication, but authorisation is required to push to the cache. We do not recommend allowing all users to push to a cache, especially if the cache is publically available.

Configuring Bazel to use a remote cache server can be done using the following bazel flags:

```txt
build --remote_cache=public.aws.celduin.co.uk
# If TLS client authentication is required
build --tls_client_certificate=/path/to/client-certificate.pem
build --tls_client_key=/path/to/client-key.pem
# If the server's certificate is not trusted
build --tls_certificate=/path/to/certificate.pem
```

### Remote Execution Access

Remote Execution services are available only behind the `push` endpoint. This is because remote execution necessarily requires write access to the Content Addressable Storage (CAS), so it's equivalent to pushing to the cache directly. If the `push` endpoint requires TLS client authorisation then certificates will need to be specified as above.

Configuring Bazel to use a remote cache server can be done using the following bazel flags:

```txt
build --remote_executor=public.aws.celduin.co.uk
build --remote_instance_name=remote-execution

# Platform properties for the worker to match
build --remote_default_exec_properties=OSFamily=Linux
build --remote_default_exec_properties=container-image=docker://marketplace.gcr.io/google/rbe-ubuntu16-04@sha256:6ad1d0883742bfd30eba81e292c135b95067a6706f3587498374a083b7073cb9

# If TLS client authentication is required
build --tls_client_certificate=/path/to/client-certificate.pem
build --tls_client_key=/path/to/client-key.pem
# If the server's certificate is not trusted
build --tls_certificate=/path/to/certificate.pem
```

Platform properties should match those required by the project, as well as those on the worker you need to use. This configuration is specified against the ubuntu-16.04 runners from v0.7.0, which are based upon Google's RBE service.
