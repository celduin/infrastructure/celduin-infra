# Celduin-Infra

For the end to end kubernetes deployment of [Remote Execution API] (REAPI) servers and supporting services.

[Buildbarn] is used as the REAPI server implementation.

For in depth documentation, including guides on deploying and internal architecture, see [our documentation](./docs).

## Deploying locally

To deploy celduin-infra to your local machine, you can use our `dev` deployments.

We recommend the use of [kind] to run a kubernetes cluster locally.

You will also require:

- [Jsonnet]
  - This may be available from your distribution, but if not you can install
    the binary release from the [github releases page](https://github.com/google/jsonnet/releases)
- [Kubectl]
  - Follow the [installation instructions](https://kubernetes.io/docs/tasks/tools/install-kubectl/) to install.

To deploy:

```bash
kind create cluster --config kind.yaml
mkdir -p target
jsonnet -m target kubernetes/clusters/dev/config/certs.jsonnet
jsonnet -m target kubernetes/clusters/dev/deployment.jsonnet
# If the following node names don't work do `kubectl get nodes` to find
# the correct names
kubectl label node kind-worker type=worker
kubectl label node kind-worker2 type=default

kubectl apply -f target/*-Namespace.json
kubectl apply -f target
```

Any 'no matches for kind "AppProject" in version "argoproj.io/v1alpha1"' errors can be safely ignored.

## Deploying to AWS

To deploy to a fresh AWS account, follow the instructions in [this document](./docs/deploy-from-scratch.md).

## Monitoring

Cluster activity can be monitored through grafana dashboards and prometheus backends. We also deploy Loki for log aggregation.

## Contact us

For more information, please contact us at #celduin on freenode!

[//]: # (External Links Block)

[kind]: https://kind.sigs.k8s.io/
[Remote Execution API]: https://github.com/bazelbuild/remote-apis
[Buildbarn]: https://github.com/buildbarn
[Jsonnet]: https://jsonnet.org
[Kubectl]: https://kubernetes.io/docs/reference/kubectl/overview/
[BuildStream]: https://buildstream.build
[Bazel]: https://bazel.build
